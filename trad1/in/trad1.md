Le vrai est ce quil peut; le faux est ce
qu'l veut*
Madame de Duras'

* “Lo que puedeé es lo verdadero; lo que quiere es lo falso”.
* En D, el epigrafe es otro:
La fundación de nuestras bellas artes y la fijación de sus distintos tipos
y usos se remontan a una época que se distingue marcadamente de la
nuestra y a hombres cuyo poder sobre las cosas y las circunstancias era
insignificante en comparación con el nuestro. Pero el sorprendente crecimiento de nuestros medios y la adaptabilidad y precisión que han alcan-

[35]

J6

WALTER BENJAMIN

zado nos aseguran para un futuro próximo profundas transformaciones en
la antigua industria de lo bello. En todas las artes hay una parte física que
ya no puede ser vista y tratada como antes; que no puede sustraerse a das
empresas del conocimiento y de la fuerza modernos. Ni la materia ni el
espacio ní el tiempo son desde hace veinte años lo que habían sido siempre. Debemos esperar innovaciones tan grandes que transformen el conjunto de las técnicas de las artes y afecten así la invención misma y alcancen
tal vez finalmente a transformar de manera asombrosa la noción misma
del arte.
. El original francés dice:
Nos Beaux-Arts ont été institués, et leurs types comme leur usage fixés,
dans un temps bien distinct du nóte, par des hommes dont le pouvoir
d'action sur les choses était insignifrant aupres de celui que nous possédons. Mais létonnant accroissement de nos moyens, la souplesse et la
précision qu'ils alteignent, les idées et les habitudes qw'ils introduisent nous
assurent de changements prochains et tres profonds dans lantigue industrie du Beau. Il y a dans tous les arts une partie physique quí ne peut plus
étre regardée ni traitée comme naguere, qui ne peut pas étre soustraite aux
entreprises de la connaissance et de la puissance modemes. Ni la matiere,
ni lespace, ni le temps ne sont depuis vingt ans ce qu'ils étaient depuis
toujours. I faut s'attendre que de si grandes nouveautés transforment toute
Ja technique des arts, agissent par la sur Vinvention elle-méme, aillent peut-

étre jusqu'á modifier merveilleusement la notion méme de Part.
(Paul Valéry, Pieces sur lart (*La conquéte
vres, tomo I1, La Pleiade, París, 1960, p. 1284).

de lubiquité”), en Oerr-

I*

Prólogo

Cuando Marx emprendió el análisis del modo de producción
capitalista éste estaba en sus comienzos. Marx dispuso de tal
manera sus investigaciones, que éstas adquirieron un valor de
prognosis. Descendió hasta las condiciones fundamentales de
la producción capitalista y las expuso de tal manera que de ellas

se podía derivar lo que habría de esperarse más adelante del

capitalismo. Se derivaba que del mismo se podía esperar no
sólo una explotación cada vez más aguda de los proletarios
sino también, finalmente, la preparación de las condiciones
que hacen posible su propia abolición.
El revolucionamiento de la supraestructura avanza mucho
más lentamente que el de la infraestructura, ha requerido más

de medio siglo para hacer vigente en todos los ámbitos cul-

turales la transformación de las condiciones de producción.

La figura que adoptó ese revolucionamiento es algo que apenas hoy en día puede registrarse. Sería conveniente someter
los datos de este registro a ciertas exigencias de prognosis.
Estas exigencias responden menos a unas posibles tesis acerca del arte del proletariado después de la toma del poder, o
del arte de la sociedad sin clases, que a otras tesis, igualmente
posibles, acerca de las tendencias del desarrollo del arte bajo
las actuales condiciones de producción. La dialéctica de éstas
no es menos perceptible en la supraestructira que en la
* En C no aparecef En D figura como “Prefacio”.
T Los subtítulos de las tesis provienen del índice de la primera versión (A).

[37]

38

WALTER BENJAMIN

economía. Sería errado, por lo tanto, menospreciar el valor

que tales tesis puedan tener en la lucha actual. Son tesis que

hacen de lado un buen número de conceptos heredados —como

“creatividad” y “genialidad”, “valor imperecedero” y “miste-

rio”— cuyo empleo acrítico (y difícil de controlar en este momen-

to) lleva a la elaboración del material empírico en un sentido

fascista. Los conceptos nuevos que se introducen a continuación en la teoría del arte se diferencian de los usuales por el

hecho de que son completamente inutilizables para los fines
del fascismo. Son en cambio útiles para formular exigencias
revolucionarias en la política del arte.

H*

Reproductibilidad técnica
En principio, la obra de arte ha sido siempre reproducible.
Lo que había sido hecho por seres humanos podía siempre
ser re-hecho o imitado por otros seres humanos. Hubo, en
efecto, imitaciones, y las practicaron lo mismo discípulos para
ejercitarse en el arte, maestros para propagar sus obras y también terceros con ambiciones de lucro. Comparada con la
imitación, la reproducción técnica de la obra de arte es algo
nuevo que se ha impuesto intermitentemente a lo largo de la
historia, con largos intervalos pero con intensidad creciente.
Con el grabado en madera, la gráfica se volvió por primera
vez reproducible técnicamente; lo fue por largo tiempo antes
de que la escritura llegara a serlo también gracias a la imprenta. Son conocidas las enormes transformaciones que la imprenta, la reproducción técnica de la escritura, ha suscitado
en la literatura. Y ellas son tan sólo un caso especial, sin duda
particularmente importante, de un fenómeno que se considera aquí a escala histórica universal. Al grabado en madera
se suman, en la edad media, el grabado en cobre y el aguafuerte, así como, a comienzos del siglo xTx, la litografía.
Con la litografía, la técnica de la reproducción alcanza un
nivel completamente nuevo. El procedimiento mucho más
conciso que diferencia a la transposición del dibujo sobre una
plancha de piedra respecto del tallado del mismo en un bloque
de madera, o de su quemadura sobre una plancha de cobre,

* En C y en D lleva el número L; en A, el 1.

[39]

40

WALTER BENJAMIN

dio a la gráfica por primera vez la posibilidad de que sus productos fueran llevados al mercado no sólo en escala masiva
(como antes), sino en creaciones que se renovaban día a día.
Gracias a la litografía, la gráfica fue capaz de acompañar a la
vida cotidiana, ofreciéndole ilustraciones de sí misma. Comen-

zó a mantener el mismo paso que la imprenta. Pero ya en
estos comienzos, pocos decenios después de la invención de
la litografía, sería superada por la fotografía. Con esta, la mano
fue descargada de las principales obligaciones artísticas dentro del proceso de reproducción de imágenes, obligaciones
que recayeron entonces exclusivamente en el ojo. Puesto que
el ojo capta más rápido de lo que la mano dibuja, el proceso
de reproducción de imágenes se aceleró tanto, que fue ca-

paz de mantener el paso con el habla.* Si en la litografía se

encontraba ya virtualmente la revista ilustrada, así, en la fo-

tografía, el cine sonoro. La reproducción técnica del sonido
fue emprendida a finales del siglo pasado.* Hacía mil novecien-

tos la reproducción técnica había alcanzado un estándar tal,

que le permitía no sólo convertir en objetos suyos a la totalidad de las obras de arte heredadas y someter su acción a las
más profundas transformaciones, sino conquistar para sí mis* En D se intercala aquí esta frase:
“El editor sincroniza la sucesión de
habla el intérprete”.
* En D se intercala aquí el siguiente
gentes hacían previsible una situación
siguiente frase: “Así como el agua, el gas

imágenes con la velocidad a la que
pasaje: “Estos dos esfuerzos converque Paul Valéry caracteriza con la
y la corriente eléctrica vienen ahora

desde lejos a servimos en nuestras casas, obedeciendo a un movimiento de
nuestra mano, así llegaremos a disponer de imágenes y sucesiones sonoras
que se presentarán respondiendo a un movimiento nuestro, casi a una señal,

y que desaparecerán de la misma manera” (Paul Valéry, Pieces sur Part[La
conquete de lubiquité], Patís sl (134) p. 105.)

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

ma un
es más
en que
la obra
arte en

41

lugar propio entre los procedimientos artísticos. Nada
sugerente para el estudio de este estándar que el modo
sus dos manifestaciones distintas —la reproducción de
de arte y el arte cinematográfico— retroactúan sobre el
su figura heredada.*

* En A, esta última frase dice:

“Nada es más sugerente para el estudio de este estándar que el modo

en que se interpenetran estas dos diferentes funciones: la reproducción de
la obra de artey el arte cinematográfico”.

m*

Autenticidad

Incluso en la más perfecta de las reproducciones una cosa
queda fuera de ella: el aquí y ahora de la obra de arte, su

existencia única en el lugar donde se encuentra. La historia a
la que una obra de arte ha estado sometida a lo largo de su
permanencia es algo que atañe exclusivamente a ésta, su existencia única. Dentro de esta historia se encuentran lo mismo
las transformaciones que ha sufrido en su estructura física a lo
largo del tiempo que las cambiantes condiciones de propiedad
en las que haya podido estar.* La huella de las primeras sólo
puede ser reconocida después de un análisis químico o físico
al que una reproducción no puede ser sometida; la huella de
las segundas es el objeto de una tradición cuya reconstrucción debe partir del lugar en que se encuentra el original.
El concepto de la autenticidad del original está constituido
por su aquí y ahora; sobre éstos descansa a su vez la idea de
una tradición que habría conducido a ese objeto como idéntico a sí mismo hasta el día de hoy.* Todo el ámbito de la
* En C, I.
* En D entra aquí la siguiente nota (2):
“Por supuesto que la historia de una pieza de arte incluye más cosas: en
la historia de la Mona Lisa, por ejemplo, el tipo y el número de copias que
fueron hechas de ella en los siglos Xv11, XVTII y XIX”.
* En D se encuentra aquí esta frase: “ Análisis químicos de la pátina de
un bronce pueden ser útiles para determinar su autenticidad; de manera
similar, la comprobación de que cierto manuscrito de la edad media proviene de un archivo del siglo xV puede ser útil en la determinación de su
autenticidad.”

[+2]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

43

autenticidad escapa a la reproductibilidad técnica —y por supuesto no sólo a ésta—* Mientras lo auténtico mantiene su
plena autoridad frente a la reproducción manual, a la que por
lo regular se califica de falsificación, no puede hacerlo en cambio frente a la reproducción técnica. La razón de esto es doble.
En primer lugar, la reproducción técnica resulta ser más independiente del original que la reproducción manual. Ella puede,

por ejemplo, resaltar en la fotografía aspectos del original que
son asequibles a la lente, con su capacidad de elegir arbitrariamente un punto de vista, y que no lo son al ojo humano;
puede igualmente, con la ayuda de ciertos procedimientos
como la ampliación o el uso del retardador, atrapar imágenes
que escapan completamente a la visión natural. Esto por una
parte. Puede además, por otra, poner la réplica del original
en ubicaciones que son inalcanzables para el original. Hace,

sobre todo, que ella esté en posibilidad de acercarse al receptor, sea en forma de una fotografía o de una reproducción de

sonido grabada en disco. La catedral abandona su sitio para
ser recibida en el estudio de un amante del arte; la obra coral
que fue ejecutada en una sala o a cielo abierto puede ser
escuchada en una habitación.
* En D entra aquí la siguiente nota (3):
“Precisamente porque la autenticidad no es reproducible, la invasión
intensiva de ciertos procedimientos técnicos de reproducción dio un motivo para diferenciar y jerarquizar la autenticidad. Una importante función del comercio con el arte fue la de desarrollar tales diferenciaciones.
Éste tenía un interés evidente en mantener separadas las diferentes impresiones de un grabado en madera. Con la invención del grabado en
madera puede decirse que la cualidad de lo auténtico fue atacada en su
raíz, antes incluso de que desarrollara su florecimiento

tardio. En el

momento en que fue elaborada la imagen medieval de la madonna ésta
no era “auténtica' todavía; se fue volviendo tal con el correr de los siglos
siguientes y llegó a su plenitud en el siglo x1X”.

44

WALTER BENJAMIN

Por lo demás, aunque estas nuevas condiciones pueden
dejar intacta la consistencia de la obra de arte, desvalorizan

de todos modos su aquí y ahora. Es algo que no afecta solamente a la obra de arte; puede afectar también, por ejemplo,

a un paisaje que en una película pasa de largo ante el especta-

dor; sin embargo, se trata de un proceso que toca en el objeto

de arte un núcleo sensitivo tan susceptible como no lo hay en
ningún objeto natural. Ese núcleo es su autenticidad. La autenticidad de una cosa es la quintaesencia de todo lo que en
ella, a partir de su origen, puede ser transmitido como tradición,
desde su permanencia material hasta su carácter de testimonio histórico. Cuando se trata de la reproducción, donde la
primera se ha retirado del alcance de los receptores, también
el segundo —el carácter de testimonio histórico— se tambalea,
puesto que se basa en la primera. Sólo él, sin duda; pero lo
que se tambalea con él es la autoridad de la cosa, su carga de
tradición.*
Se puede resumir estos rasgos en el concepto de aura, y
decir: lo que se marchita de la obra de arte en la época de su
reproductibilidad técnica es su aura. Es un proceso sintomático; su importancia apunta mas allá del ámbito del arte. Za
técnica de reproducción, se puede formular en general, separa a lo reproducido del ámbito de la tradición. Al multiplicar
sus reproducciones, pone, en lugar de su aparición única, su
aparición masiva. Y al permitir que la reproducción se apro* En D entra aquí la siguiente nota (4):
“La más precaria de las puestas en escena provincianas del Fausto tiene
esta ventaja sobre una película del mismo tema: está idealmente en com-

petencia con la puesta en escena original en Weimar. Además, cualquier

contenido tradicional que uno puede traer a la memoria ante el escenario
se vuelve inutilizable en la pantalla: que en el Mefisto se esconde Johann
Heinrich Merck, un amigo de la juventud de Goethe, y cosas parecidas”.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

45

xime al receptor en su situación singular actualiza lo reproducido. Estos dos procesos conducen a un enorme trastorno del
contenido de la tradición —un trastorno de la tradición que es

la otra cara de la crisis y renovación contemporáneas de la
humanidad—. Son procesos que están en conexión estrecha
con los movimientos

de masas

de nuestros

días. Su agente

más poderoso es el cine. Incluso en su figura más positiva, y
precisamente en ella, su significación social no es pensable sin
su lado destructivo, catártico: la liquidación del valor tradicional de la herencia cultural. Este fenómeno se percibe de la
manera más directa en las grandes películas históricas. Es un

fenómeno que integra cada vez más posiciones dentro de su
ámbito. Cuando Abel Gance, en 1927, exclamó entusiasmado:
“Shakespeare, Rembrandt, Beethoven harán películas [...] Todas las leyendas, todas las mitologías y todos los mitos, todos

los fundadores de religión, incluso todas las religiones [...] esperan su resurrección en la pantalla, y los héroes se apiñan
ante los portones”,' lo que hacía, tal vez sin proponérselo, era
invitar a una liquidación omniabarcante.

IV*

La destrucción del aura

Dentro de largos períodos históricos, junto con el modo de
existencia de los colectivos humanos, se transforma también

la manera de su percepción sensorial. El modo en que se
organiza la percepción humana —el medio en que ella tiene
lugar— está condicionado no sólo de manera natural, sino también histórica. La época de la invasión de los bárbaros, en la

que surgió la industria cultural de la Roma tardía y se ilustró

el Génesis de Viena, no sólo tenía un arte diferente del de la

antigledad sino también una percepción diferente. Riegl y
Wickhoff, los académicos de la escuela de Viena que se opusieron al peso de la herencia clásica bajo el que había estado
enterrado aquel arte, fueron los primeros en llegar a la idea
de sacar del mismo conclusiones acerca de la organización de
la percepción en el tiempo en que tuvo vigencia. Aunque
fueron descubrimientos de gran importancia, tuvieron sin
embargo el límite que les impusieron sus autores, quienes se
contentaron con señalar la rúbrica formal que fue propia de
la percepción en la época de la Roma tardía. No intentaron
—y tal vez no podían esperar hacerlo— mostrar los trastornos
sociales que encontraban expresión en estas transformaciones
de la percepción. En la época actual, las condiciones para
una comprensión de ese tipo son más favorables. Ahora es
posible no sólo comprender las transformaciones del medium
de la percepción de las que somos contemporáneos como
* En C y en D

lleva el número II.

[+6]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBIIDAD TÉCNICA

47

una decadencia del aura, sino también mostrar sus condiciones
sociales.

¿Qué es propiamente el aura? Un entretejido muy especial

de espacio y tiempo: aparecimiento único de una lejanía, por
más cercana que pueda estar.*

Reposando en una tarde de verano, seguir la linea mon-

tañosa en el horizonte o la extensión de la rama que echa su

sombra sobre aquel que reposa, eso quiere decir respirar el
aura de estas montañas, de esta rama. Con ayuda de esta
descripción resulta fácil entender el condicionamiento social
de la actual decadencia del aura. Se basa en dos condiciones
que están conectadas, lo mismo la una que la otra, con el

surgimiento de las masas y la intensidad creciente de sus movimientos. Esto es: “Acercarse las cosas” ? es una demanda
tan apasionada de las masas contemporáneas como la que
está en su tendencia a ir por encima de la unicidad de cada

suceso mediante la recepción de la reproducción del mismo.
Día a día se hace vigente, de manera cada vez más irresistible,
la necesidad de apoderarse del objeto en su más próxima
cercanía, pero en imagen, y más aún en copia, en reproduc* En D en lugar de esta frase se encuentra la siguiente:
“Es conveniente ilustrar el concepto de aura propuesto más arriba para
objetos históricos con el concepto de un aura propia de objetos naburales.
A ésta última la definimos como el aparecimiento único...”
* En D se añade aqui: “espacial y humanamente”, y se incluye la siguiente nota (6):
“Hacer que algo se acerque humanamente a las masas puede significar
la expulsión fuera del campo visual de la función social que ese algo tiene.
Nada garantiza que un pintor de retratos actual, cuando pinta a un cirujano famoso a la mesa del desayuno, rodeado de los suyos, atine a mostrar
su función social con mayor precisión que un pintor del siglo XVI1 como
Rembrandt, por ejemplo, en su Anatomía, cuyos médicos pinta él repre-

sentativamente para el público”.

48

WALTER BENJAMIN

ción. Y es indudable la diferencia que hay entre la reproducción, tal como

está disponible en revistas ilustradas y

noticieros, y la imagen. Unicidad y durabilidad se encuentran
en ésta tan estrechamente conectadas entre sí como fugacidad
y repetibilidad en aquélla. La extracción del objeto fuera de
su cobertura, la demolición del aura, es la rúbrica de una

percepción cuyo “sentido para lo homogéneo en el mundo”
ha crecido tanto, que la vuelve capaz, gracias a la reproducción, de encontrar lo homogéneo incluso en aquello que es
único. Así es como se manifiesta en el campo de lo visible
aquello que en el campo de la teoría se presenta como un
incremento en la importancia de la estadística. La orientación
de la realidad hacia las masas y de las masas hacia ella es un
proceso de alcances ilimitados lo mismo para el pensar que
para el mirar.

V*

Ritual y política

El carácter único de la obra de arte es lo mismo que su imbricación en el conjunto de relaciones de la tradición. Y esta
tradición, por cierto, es ella misma algo plenamente vivo, ex-

traordinariamente cambiante. Una antigua estatua de Venus,
por ejemplo, se encontraba entre los griegos, que hacían de
ella un objeto de culto, en un conjunto de relaciones tradicionales diferente del que prevalecía entre los clérigos medievales, que veían en ella un ídolo maligno. Algo, sin embargo,
se les ofrecía a ambos de la misma manera: su unicidad, es

decir, su aura. El modo originario de inserción de la obra de
arte en el sistema de la tradición encontró su expresión en el
culto. Las obras de arte más antiguas surgieron, como sabemos, al servicio de un ritual que primero fue mágico y después
religioso. Ahora bien, es de importancia decisiva el hecho de

que esta existencia aurática de la obra de arte no llega nunca
a separarse del todo de su función ritual.? En otras palabras:
El valor único e insustituible de la obra de arte “auténtica”
* En C y en D, IV.
* En D entra aquí la siguiente nota (7):

“La definición del aura como el “aparecimiento único de una lejanía,

por más cercana que pueda estar” no representa otra cosa que la formu-

lación del valor de culto de la obra de arte puesta en categorías de la

percepción espacio-temporal. Lejanía es lo contrario de cercanía. Lo esencialmente lejano es lo inacercable. De hecho, la inacercabilidad es una
cualidad principal de la imagen de culto. Ella permanece por naturaleza

lejana, por cercana que pueda estar”. La cercanía que se pueda sacar de

su materia no rompe la lejanía que conserva desde su aparecimiento”.

[49]

50

WALTER BENJAMIN

tiene siempre su fundamento en el ritual* Éste puede estar
todo lo mediado que se quiera pero es reconocible como un
ritual secularizado incluso en las formas más profanas del servicio a la belleza.* El servicio profano a la belleza que se generó en el Renacimiento ha estado vigente tres siglos; pasado
este período, la primera conmoción grave que ha llegado a
afectarlo permite reconocer claramente esos fundamentos. En
efecto, cuando el surgimiento de la fotografía —el primer
método de reproducción verdaderamente revolucionario (simultáneo a la irupción del socialismo)— mueve al arte a detectar la cercanía de la crisis, que pasados otros cien años se

ha vuelto imposible ignorar, éste reacciona con la doctrina de
Vart pour f'art, que es una teología del arte. De ésta derivó
entonces directamente una teología negativa, representada por
la idea de un arte puro, que rechaza no sólo cualquier función

social del mismo, sino incluso toda determinación que proven-

ga de un asunto objetivo. (En la poesía fue Mallarmé el primero en alcanzar esta posición.)
* En A: “tiene un fundamento teológico”. D añade: “en el que tuvo su

valor de uso originario y primero”.

* En D entra aquí la siguiente nota:
“En la medida en que el valor de culto de la imagen se seculariza, las
ideas acerca del substrato de su unicidad se vuelven cada vez más imprecisas. Cada vez más la unicidad del aparecimiento que actúa desde la
imagen de culto va siendo desplazada en la imaginación del receptor por
la unicidad empírica del hacedor de la imagen o de su rendimiento como
artista plástico; pero nunca sin que algo quede: el concepto de autenticidad
no ceja nunca en su tendencia a rebasar al de la adjudicación de autenticidad. (Esto se muestra de la manera más clara en el coleccionista, que

retiene siempre algo de servidor del fetiche y que, al ser propietario de la
obra de arte, participa de la fuerza ceremonial de la misma.) No obstante
esto, la función del concepto de autenticidad en la crítica de arte permanece univoca: con la secularización del arte, la autenticidad entra en el

lugar del valor de culto”.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

— 51

A una reflexión que tiene que ver con la obra de arte en la
época de su reproductibilidad técnica le es indispensable reconocer el valor que les corresponde a estas interconexiones.
Pues ellas preparan un conocimiento que aquí es decisivo:
por primera vez en la historia del mundo la reproductibilidad
técnica de la obra de arte libera a ésta de su existencia parasi-

taria dentro del ritual. La obra de arte reproducida se vuelve
en medida creciente la reproducción de una obra de arte compuesta en tomo a su reproductibilidad.* De la placa fotográfica es posible hacer un sinnúmero de impresiones; no tiene
sentido preguntar cuál de ellas es la impresión auténtica. Pero

si el criterio de autenticidad llega a fallar ante la producción
artística, es que la función social del arte en su conjunto se ha

trastornado. En lugar de su fundamentación en el ritual, debe
aparecer su fundamentación en otra praxis, a saber: su fundamentación en la política.

Valor de culto y valor de exhibición

Sería posible exponer la historia del arte como una disputa
entre dos polaridades dentro de la propia obra de arte, y
distinguir la historia de su desenvolvimiento como una sucesión de desplazamientos del predominio de un polo a otro de
la obra de arte. Estos dos polos son su valor ritual y su valor
de exhibición.** La producción artística comienza con imá* En Cy en D, V.
! En D entra aquí la siguiente nota (11):
“La transición del primer tipo de recepción artística al segundo determina el decurso histórico de la recepción artística en general. No obstante,
en principio, en cada obra de arte singular se puede observar una oscilación entre ambos tipos contrapuestos de recepción. Así, por ejemplo,
en la Madonna Sixtina. Es conocido, después de la investigación de Hubert Grimme, que la Madonna Sixtina fue pintada originalmente para fines
de exhibición. Grimme recibió el impulso para sus investigaciones de la
pregunta: ¿qué hace ahí la tira de madera sobre la que están sentados los
dos ángeles, en la parte delantera de la imagen? ¿Cómo pudo Rafael,
continuó preguntándose Grimme, dotar al cielo de un par de porteros? La
investigación dio como resultado que la Madonna Sixtina había sido encargada para la capilla ardiente del Papa Sixto. Las capillas ardientes de
los papas se colocaban en una capilla lateral de la Catedral de San Pedro.
La imagen de Rafael había sido colocada, durante la ceremonia solemne,
sobre el féretro, en una especie de nicho al fondo de la capilla. Lo que
Rafael representa en esta imagen es cómo la Madona, viniendo del fondo
del nicho delimitado por los porteros verdes, se aproxima al féretro papal.

El extraordinario valor de exhibición de la imagen de Rafael encontró así
su realización en los funerales del Papa Sixto. Un tiempo después la imagen pasaría al altar mayor del templo en el monasterio de los monjes
negros de Piacenza. La razón del exilio está en el ritual romano. El ritual
romano prohibe llevar al altar mayor imágenes que hayan sido expuestas

12

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

— 53

genes que están al servicio de la magia. Lo importante de
estas imágenes está en el hecho de que existan, y no en que
sean vistas. El búfalo que el hombre de la Edad de Piedra
dibuja sobre las paredes de su cueva es un instrumento mági-

co que sólo casualmente se exhibe a la vista de los otros; lo
importante es, a lo mucho, que lo vean los espiritus. El valor
ritual prácticamente exige que la obra de arte sea mantenida

en lo oculto: ciertas estatuas de dioses sólo son accesibles para

los sacerdotes en la cella, ciertas imágenes de la Virgen per-

manecen ocultas por un velo durante gran parte del año; cier-

tas esculturas de las catedrales góticas no son visibles para el

espectador al nivel del suelo. Con la emancipación que saca a
los diferentes procedimientos del arte fuera del seno del ri-

tual, aumentan para sus productos las oportunidades de ser
exhibidos. Un retrato en busto, que puede ser enviado de un

lugar o a otro, tiene más probabilidades de ser exhibido que
la estatua de un dios, que tiene su lugar fijo en el interior del
templo. Lo mismo sucede con un cuadro si lo comparamos
con el mosaico o el fresco que lo precedieron. Y aunque por
sí misma una misa no estaba menos destinada a la exhibición

que una sinfonía, de todos modos la sinfonía apareció en un
momento en que su capacidad de ser exhibida prometía ser
mayor que la de una misa.
Con los diferentes métodos de reproducción técnica de la

obra de arte, su capacidad de ser exhibida ha crecido de

en festividades fúnebres. Esta prescripción devaluaba, dentro de ciertos

limites, la obra de Rafael. A fin de alcanzar de todos modos un precio
adecuado por ella, la curia decidió entregar, junto a la imagen, su aceptación tácita para que ocupara el altar mayor. Para evitar el escándalo se
entregó la imagen a esa hermandad de la lejana ciudad provincial”.

54

WALTER BENJAMIN

manera tan gigantesca, que el desplazamiento cuantitativo entre
sus dos polos la lleva a una transformación parecida a la de
los tiempos prehistóricos, que invierte cualitativamente su
consistencia. En efecto, así como en los tiempos prehistóricos
la obra de arte fue ante todo un instrumento de la magia en
virtud del peso absoluto que recaía en su valor ritual —un
instrumento que sólo más tarde fue reconocido en cierta medida como obra de arte—, así ahora, cuando el peso absoluto
recae en su valor de exhibición, la obra de arte se ha converti-

do en una creación dotada de funciones completamente nuevas, entre las cuales destaca la que nos es conocida: la función
artística —la misma que más tarde será reconocida tal vez como
accesoria—.* De lo que no hay duda es de que el cine es
actualmente el hecho que más se presta para llegar a esta
conclusión. También es indudable que el alcance histórico de
este cambio de función del arte —que se presenta de la manera más adelantada en el cine— permite que se lo confronte
no sólo formal sino también materialmente con la época originaria del arte.

* En A: “como rudimentaria”. En D entra aquí la siguiente nota (12):
“En otro plano, Brecht hace consideraciones análogas: 'Si no hay cóomo
mantener el concepto de obra de arte para aquella cosa que aparece cuando una obra de arte es convertida en mercancía, debemos

entonces, de

manera cuidadosa y prudente, pero con valentía, abandonar dicho concepto. Ello, si no queremos eliminar la función misma de esa cosa. Es una

fase por la que ella debe pasar, y sin segunda intención. No se trata de un
desviarse del camino correcto que no tenga consecuencias; lo que acontezca aquí con ella la cambiará fundamentalmente; borrará a tal grado su
pasado, que si el viejo concepto fuera retomado más tarde —y lo será, ¿por
qué no?— ya no desatará ningún recuerdo de aquello a lo que hizo referencia una vez”” [Bertolt Brecht, “El proceso de tres centavos”, en Versuche 14, Suhrkamp, 1959, p. 295|.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

55

Al servicio de la magia, el arte de los tiempos prehistóricos
conserva ciertas propiedades que tienen utilidad práctica.*
Que la tienen, probablemente, en la ejecución de operaciones
mágicas (tallar la figura de un ancestro es en sí mismo un procedimiento mágico), en la prefiguración de las mismas (la figura del ancestro modela una posición ritual) o como objeto
de una contemplación mágica (mirar la figura del ancestro
fortalece la capacidad sobrenatural del que la mira). Los objetos con este tipo de propiedades ofrecían imágenes del ser
humano y su entorno, y lo hacían en obediencia a los requerimientos de una sociedad cuya técnica sólo existe si está confundida con el ritual.* Se trata por supuesto de una técnica
atrasada si se la compara con la de las máquinas. Pero esto no
es lo importante para una consideración dialéctica; a ésta le
interesa la diferencia tendencial entre aquella técnica y la nuestra, diferencia que consiste en que mientras la primera invo-

lucra lo más posible al ser humano, la segunda lo hace lo
menos posible. En cierto modo, el acto culminante de la pri-

mera técnica es el sacrificio humano; el de la segunda está en
la línea de los aviones teledirigidos, que no requieren de tripu-

* En C, el texto que va de esta frase hasta el final de esta Tesis lleva el
número V1.
T En el manuscrito y también, con ciertos cambios, en A se encuentra

aquí un texto divergente:
“Esta sociedad representaba el polo opuesto de la actual, cuya técnica
es la más liberada. Esta técnica liberada se enfrenta, sin embargo, como
una segunda naturaleza a la sociedad actual, y como una naturaleza no
menos elemental que aquella que tenía ante sí la sociedad originaria. Ante
esta segunda naturaleza, el hombre —que la inventó y a la que ha dejado
de dominar hace mucho, o a la que aún no domina todavía— necesita
realizar un proceso de aprendizaje como el que realizó ante la primera. Y
es nuevamente, el arte el que se pone a su servicio, especialmente el cine”.

50

WALTER BENJAMIN

lación alguna. Lo que guía a la primera técnica es el “de una
vez por todas” (y en ella se juega o bien un error iremediable
o bien un sacrificio sustitutivo eternamente válido). Lo que
guía a la segunda es, en cambio, el “una vez no es ninguna” (y
tiene que ver con el experimento y su incansable capacidad
de variar los datos de sus intentos). El origen de la segunda
técnica hay que buscarlo allí donde, por primera vez y con
una astucia inconsciente, el ser himano empezó a tomar distancia frente a la naturaleza. En otras palabras, hay que bus-

carlo en el juego.
Seriedad y juego, rigor y desentendimiento aparecen en-

trelazados entre sí en toda obra de arte, aunque en propor-

ciones sumamente cambiantes. Con ello está dicho
arte se conecta lo mismo con la segunda técnica que
primera. De todos modos es preciso observar aquí
muy discutible caracterizar la finalidad de la segunda

que el
con la
que es
técnica

como el “dominio sobre la naturaleza”; sólo la caracteriza si

se la considera desde el punto de vista de la primera técnica.
La intención de la primera sí era realmente el dominio de la
naturaleza; la intención de la segunda es más bien la interacción concertada entre la naturaleza y la humanidad. La función social decisiva del arte actual es el ejercitamiento en esta
interacción concertada. Esto vale en especial para el cine. El
cine sirve para ejercitar al ser humano en aquellas percepciones
y reacciones que están condicíionadas por el trato con un sistema de aparatos cuya inportancia en su vida crece día a día*
Al mismo tiempo, el trato con este sistema de aparatos le
* En el manuscrito se encuentra aquí un texto divergente:
“Contribuir a que el inmenso sistema técnico de aparatos de nuestro

tiempo, que para el individuo es una segunda naturaleza, se convierta en

una primera naturaleza para el colectivo: esa es la tarea histórica del cine”.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

87

enseña que la servidumbre al servicio del mismo sólo será
sustituida por la liberación mediante el mismo cuando la constitución de lo humano se haya adaptado a las nuevas fuerzas
productivas inauguradas por la segunda técnica*

Fotografia
Con la fotografía, el valor de exhibición comienza a vencer
en toda la línea al valor ritual. Pero éste no cede sin ofrecer
resistencia. Ocupa una última trinchera, el rostro humano.

No es de ninguna manera casual que el retrato sea la principal ocupación de la fotografia en sus comienzos. El valor de
culto de la imagen tiene su último refugio en el culto al recuerdo de los seres amados, lejanos o fallecidos. En las primeras fotografías, el aura nos hace una última seña desde la
expresión fugaz de un rostro humano. En ello consiste su belleza melancólica, la cual no tiene comparación. Y allí donde el
ser humano se retira de la fotografía, el valor de exhibición se

enfrenta por primera vez con ventaja al valor de culto. Al
atrapar las calles de París en vistas que las muestran deshabi-

tadas, Atget supo encontrar el escenario de este proceso; en

esto reside su importancia incomparable. Con toda razón se
ha dicho de él que captaba esas calles como si cada una fuese
un “lugar de los hechos”. El lugar de los hechos está deshabitado; si se lo fotografía es en busca de indicios. Con Atget,
las tomas fotográficas comienzan a ser piezas probatorias en
el proceso histórico. En ello consiste su oculta significación
política. Exigen por primera vez que su recepción se haga con
un sentido determinado. La contemplación carente de compromiso no es ya la adecuada para ellas. Inquietan al observador, que siente que debe encontrar una determinada vía de

acceso a ellas. Los periódicos ilustrados comienzan a ofrecer* En D, VI.

[8]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBIIDAD TÉCNICA

59

le esa vía. Correctas o erradas, da igual. En ellos, los pies de

foto se vuelven por primera vez indispensables. Y es claro

que su carácter es completamente diferente del que tiene el

título de una pintura. Las directivas que recibe de la leyenda
al pie quien observa las imágenes de los periódicos ilustrados
se vuelven poco después, en el cine, aun más precisas y exigentes; allí la captación de cada imagen singular aparece prescrita por la secuencia de todas las precedentes.

VIT*

Valor etemo

Los griegos sólo conocieron dos procedimientos de reproducción técnica de obras de arte: el vaciado y el acuñamiento.
Bronces, terracotas y monedas eran las únicas obras de arte
que ellos podían producir en masa. Todas las demás eran únicas e imposibles de reproducir técnicamente. Por esta razón
debían ser hechas para la eternidad. Fue el estado de su técniica lo que llevó a los griegos a producir valores eternos en el
arte. Esta es la razón del lugar excepcional que ocupan en la
historia del arte; lugar respecto del cual quienes vinieron
* En D esta tesis está sustituida por la siguiente:

“El desempeño artístico del actor de teatro es presentado finalmente al

público por él mismo en persona; el desempeño artístico del intérprete de
cine, en cambio, mediante un sistema de aparatos. Esto último tiene dos
consecuencias. Del sistema de aparatos que pone ante el público el desernpeño artístico del intérprete de cine no se espera que respete la totalidad
de ese desempeño. Bajo la guía del camarógrafo, este sistema toma constantemente una posición frente a ese desempeño. La sucesión de tomas de
posición que compone el editor con el material que se le entrega conforma
la película definitiva. Abarca un cierto número de aspectos del movimiento que deben reconocerse como generados por la cámara, para no hablar
de enfoques especiales como las tomas panorámicas, por ejemplo. De esta
manera, el desempeño del intérprete es sometido a una serie de tests ópticos. Esta es la primera consecuencia del hecho de que el desempeño del
intérprete de cine sea presentado mediante el sistema de aparatos. La segunda consecuencia se basa en el hecho de que el intérprete de cine, al no
ser él mismo quien presenta al público su propio desempeño, pierde la
posibilidad, reservada para el actor de teatro, de adaptar su desempeño al
público durante la actuación. El público toma así la actitud de un experto
calificador que no es afectado por ningún contacto personal con el intér-

[60)

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

6l

después pudieron ubicar el suyo. Nuestro lugar —de ello no
cabe duda— se encuentra en el polo opuesto al de los griegos.
Nunca antes las obras de arte pudieron ser reproducidas técnicamente en una medida tan grande y con un alcance tan
amplio como pueden serlo hoy. Por primera vez, con el cine,
tenemos una forma cuyo carácter artístico se encuentra deter-

minado completamente por su reproductibilidad. Sería ocioso confrontar detalladamente esta forma con el arte griego.
Pero en un cierto punto resulta sugerente hacerlo. Con el cine
se ha vuelto decisiva una cualidad de la obra de arte que para
los' griegos hubiera sido la última o la menos esencial en ella:
su capacidad de ser mejorada. Una película terminada es todo
menos una creación lograda de un solo golpe; está montada a
partir de muchas imágenes y secuencias de imágenes, entre

las cuales el editor tiene la posibilidad de elegir —imágenes

que, por lo demás, pudieron ser corregidas a voluntad desde

prete. El público sólo se identifica con el intérprete al identificarse con el
aparato. Adopta la actitud de éste: califica. Es una actitud ante la cual no
pueden ser expuestos los valores de culto”.
Esta tesis incluye la siguiente nota (17):
“El cine... da (0 podría dar) indicaciones, conclusiones utilizables sobre
el detalle de las actividades humanas... Queda fuera toda motivación a
partir del carácter; la vida interior de las personas no entrega nunca la
causa principal y pocas veces es el resultado principal de la acción” (Brecht,
loc. cit, p. 268). La ampliación del campo de lo que puede someterse a un
test, algo logrado por el sistema de aparatos en el caso del intérprete de
cine, se corresponde con la extraordinaria ampliación del campo de lo

que puede someterse a un ¿est que afecta ahora al individuo a causa de las
condiciones económicas. Es así como crece cada vez más la importancia
de las pruebas de aptitud profesional. En éstas lo que importa son determinados cortes en el desempeño del individuo. La toma cinematográfica y la
prueba de aptitudes profesionales tienen lugar ante un gremio de especialistas. El director de escena ocupa en el estudio cinematográfico el mismo
lugar que el director de examen en la prueba de aptitudes”.

62

WALTER BENJAMIN

la secuencia de tomas hasta su resultado definitivo—. Para producir su Opinion publique, que tiene 3 mil metros, Chaplin

hizo filmar 125 mil. £l cine es, así, la obra de arte con mayor

capacidad de ser mejorada. Y esta capacidad suya de ser mejorada está en conexión con su renuncia radical a perseguir
un valor eterno. Lo mismo puede verse desde la contraprueba: para los griegos, cuyo arte estaba dirigido a la producción
de valores eternos, el arte que estaba en lo más alto era el que
es menos susceptible de mejoras, es decir, la plástica, cuyas
creaciones son literalmente de una pieza. En la época de la
obra de arte producida por montaje, la decadencia de la plástica es inevitable.

]X*

Fotograña y cine como arte

La disputa en que se empeñaron la pintura y la fotografia en
el transcurso del siglo XTX acerca del valor artístico de sus
respectivos productos da ahora la impresión de una disputa
erraday confusa. Ello no habla en contra de su importancia;
por el contrario, podría más bien subrayarla. En efecto, esta

disputa fue la expresión de una transformación de alcance
histórico universal de la que ninguno de los contrincantes
estaba conciente. La época de la reproductibilidad técnica

del arte separó a éste de su fundamento ritual; al hacerlo, la

apariencia de su autonomía se apagó para siempre. Pero el
cambio de función del arte que venía con ello estuvo fuera

del campo de visión de ese siglo. Fue algo que también al
siglo XX, que ha vivido el desarrollo del cine, se le escapó
por un buen tiempo.
Antes de la llegada del cine, mucha agudeza fue empleada inútilmente en decidir la cuestión de si la fotografía era

un arte o no —sin haberse planteado la pregunta previa acerca de sí el carácter global del arte no se había transformado

a causa del descubrimiento de la fotografía—; después, los

teóricos del cine retomarían pronto, de modo igualmente

apresurado, la misma cuestión. Pero las dificultades que la

fotografía había planteado ala estética tradicional resultaron
un juego de niños en comparación con las que el cine le tenía
reservadas. De ahí la ciega violencia que caracteriza los
* En D, VIL

[63]

64

WALTER BENJAMIN

inicios de la teoría del cine. Así, por ejemplo, Abel Gance
compara al cine con los jeroglíficos: “Así, pues, hemos venido a dar nuevamente, como resultado de un retormo suma-

mente extraño a lo que ya fue una vez, en el plano de
expresión de los egipcios [...] El lenguaje en imágenes no
ha madurado todavía a plenitud porque nuestros ojos no se
encuentran todavía a su nivel. Todavía no existe el suficiente respeto, no se rinde el suficiente cu/to a lo que se
expresa en él”* O Severin-Mars, quien escribe: “A qué
arte le fue conferido soñar de una manera que resulta más
poética mientras más realista es! Mirado desde este punto
de vista, el cine representaría un medio de expresión in-

comparable; en su atmósfera sólo deberían moverse personajes del más noble estilo de pensamiento, en los instantes
más perfectos y misteriosos del camino de su vida”.** Es
muy ilustrativo observar la manera en que el intento de
catalogar al cine como “arte” obliga a estos teóricos a introducir en él elementos rituales, mientras que lo interpretan

con una incomparable falta de respeto. Cuando se publican estas especulaciones existen ya obras como L'opinion
publique y La ruée vers l'or. Lo que no impide que Abel
Gance introduzca su comparación con los jeroglifos y que
Severin-Mars hable del cine como se podría hablar de los
cuadros de un Fra Angelico. Es indicativo que todavía hoy
autores especialmente reaccionarios! busquen la signifi* En D

se intercala lo siguiente, con su correspondiente nota (15):

“Alexandre Amoux, por su parte, termina una fantasía sobre el cine

mudo con la pregunta: ' ¿No deberían todas las atrevidas descripciones de
las que nos hemos servido aquí desembocar en una definición de lo que es
el rezo?” (Alexandre Amoux,

* En C: “conservadores”.

Cinéma, París, 1929, p. 28.)

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

— 65

cación del cine en la misma dirección; si no directamente
en lo sacro, sí en lo sobrenatural. Con motivo de la filmación hecha por Reinhardt del Sueño de una noche de

verano, Werfel constata que aquello que hasta aquí le ha

impedido al cine prosperar en el reino del arte ha sido indudablemente la copia estéril del mundo
calles,

decorados,

estaciones

de

trenes,

exterior, con sus
restaurantes,

au-

tomóviles y playas. “El cine no ha captado todavía su verdadero

sentido,

sus posibilidades

reales

[...] Éstas con-

sisten en su capacidad única de expresar, con medios
naturales y con una capacidad persuasiva incomparable,
lo mágico, lo maravilloso, lo sobrenatural.””

X*

El cine y el desempeño calificable

Uno es el tipo de reproducción que la fotografía dedica a una
obra pictórica y otro diferente el que dedica a un hecho ficti-

cio en un estudio cinematográfico. En el primer caso, lo repro-

ducido es una obra de arte; no así su producción. Porque el

desempeño del camarógrafo con su lente no crea una obra de
arte, como no lo hacé tampoco un director de orquesta frente
a una orquesta sinfónica; lo que crea, en el mejor de los casos,
es un desempeño artístico. Otra cosa sucede con una tomá en
el estudio cinematográfico. Aquí lo reproducido ya no es una
obra de arte, y la reproducción tampoco, igual que en el primer
caso. La obra de arte surge aquí sólo a partir del montaje. Un
montaje en el cual cada componente singular es la reproducción de un suceso que no es en sí mismo una obra de arte ni
da lugar a una obra de arte en la fotografía. Si no son obras de
arte, ¿qué son entonces estos sucesos reproducidos en el cine?
La resp_uésta debe partir del peculiar desempeño artístico
del intérprete cinematográfico. Éste se diferencia del actor de

escenario por el hecho de que su desempeño artístico, en la
forma original que sirve de base a la reproducción, no se
_desarrolla ante un público cualquiera, sino ante un gremio de
especialistas que son directores de producción, directores
de escena, camarógrafos, ingenieros de sonido, ingenieros

de iluminación, etcétera, y que como tales están en posición de
intervenir en cualquier momento en su desempeño artístico.
* En D, VII.

[66]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

í67

Se trata de una característica que es de suma importancia en

términos sociales. La intervención de un gremio de expertos

es propia del desempeño deportivo y, en un sentido más
amplio, de todo rendimiento sometido a prueba. Y el proceso
de la producción cinematográfica está determinado completamente por una intervención de este tipo. Como es sabido,

muchas escenas se filman en distintas variantes. Un grito de
ayuda, por ejemplo, puede ser filmado en diferentes versiones. De entre éstas, el cutter hace su elección; al hacerlo,

establece de entre ellas la
sentado en un estudio de
suceso real correspondiente
de un disco en un estadio,

versión récord. Un suceso reprefilmación se diferencia así de un
como se diferencia el lanzamiento
durante una competencia depor-

tiva, de un lanzamiento del mismo, en el mismo lugar y a la
misma

distancia,

si sucediera

como

el acto

de

matar

a un

hombre. El primero sería un desempeño sometido a pruebas, el segundo no.
Ahora bien, el desempeño sometido a prueba que cumple el intérprete de cine es por supuesto un desempeño completamente único. ¿En qué consiste? Consiste en la superación de una cierta barrera; aquella que mantiene dentro
de unos límites estrechos al valor social de los rendimientos
sometidos a prueba. No ya al de los desempeños deportiVos, sino al de los que se efectúan en una prueba mecanizada. La prueba que conoce el deportista es sólo, por decirlo
así, una prueba natural; se mide frente a tareas que le propone la naturaleza y no ante las de un conjunto de aparatos
—a no ser en casos excepcionales, como el de Numni, de
quien se dice que corría contra el reloj—. Entre tanto, espe-

cialmente desde que se encuentra normado por la banda

mecánica, el proceso de trabajo da lugar todos los días a

08

WALTER BENJAMIN

innumerables exámenes ante un sistema de pruebas
mecanizado. Estos exámenes tienen lugar secretamente: el
que no los pasa es desconectado del proceso de trabajo.
Pero también tienen lugar abiertamente: en los institutos de
prueba de aptitudes profesionales. En ambos casos se topa
uno con los límites mencionados anteriormente.
Se trata, en efecto, de exámenes que, a diferencia de los de-

portivos, no son todo lo exhibibles que sería de desear. Y este es
precisamente el lugar en el que interviene el cine. Al hacer que
la capacidad misma de exhibirse, propia de todo desempeño,
se convierta en una prueba, el cíne hace que el desempeño
sometido a prueba se torne exhibible. El interprete de cine no
actúa ante un público sino ante un sistema de aparatos. El
director de filmación se encuentra exactamente en el mismo
lugar en el que está el director del examen en un examen de
aptitudes. Actuar bajo la luz de los reflectores y satisfacer al
mismo tiempo las exigencias del micrófono es una prueba de
desempeño de primer orden. Representar esta prueba de desempeño significa mantener la humanidad ante el sistema
de aparatos. El interés en este desempeño es inmenso puesto

que es ante un sistema de aparatos ante el cual la mayor parte
de los habitantes de la ciudad, en oficinas y en fábricas, de-

ben deshacerse de su humanidad mientras dura su jomada
de trabajo. Son las mismas masas que, en la noche, llenan las
salas de cine para tener la vivencia de cómo el intérprete de
cine toma venganza por ellos no sólo al afirmar su humanidad
(o lo que se les presenta así) ante el sistema de aparatos, sino
al poner esa humanidad al servicio de su propio triunto.

XI*

El intérprete cinematográfico
Al cine le importa poco que el intérprete represente a otro
ante el público; lo que le importa es que se represente a sí
mismo ante el sistema de aparatos. Pirandello ha sido uno de

los primeros en percibir esta modificación del intérprete cuando
su trabajo es un desempeño sometido a prueba. Poco perjudica el hecho de que las observaciones que hace sobre el asun-

to en su novela Corre filmación se limiten a subrayar su aspecto negativo. Menos aún que se refieran al cine mudo, puesto
que el cine sonoro no ha cambiado nada fundamental en este

asunto. Lo decisivo sigue siendo que se actúa para un sisterna
de aparatos 0, en el caso del cine sonoro, para dos. “El intér-

prete de cine”, escribe Pirandello, “se siente como en el exi-

lio. Exiliado no solamente de la escena sino incluso de su

propia persona. Percibe con oscuro desconcierto el vacío inexplicable que aparece por el hecho de que su cuerpo se convierte en una ausencia,

se desvanece

y es privado

de su

realidad, de su vida, de su voz y de los ruidos que hace al

moverse, para convertirse en una imagen muda que tiembla
un instante sobre la pantalla para desaparecer inmediatamente
en el silencio [...] Es este pequeño sistema de aparatos el que
usará su sombra para actuar ante el publico; él, por su parte,

deberá contentarse con actuar para ese sistema”.* El mismo

hecho puede caracterizarse de la siguiente manera: por primera vez —y esto es obra del cine— el hombre llega a una
situación en la que debe ejercer una acción empleando en
* En D, XIX.

[69]

70

WALTER BENJAMIN

ella toda su persona pero renunciando al aura propia de ésta.
Porque el aura está atada a su aquí y ahora. No existe una
copia de ella. El aura que está alrededor de Macbeth sobre el
escenario no puede separarse, para el público, de la que está
alrededor del actor que lo representa en vivo. Lo peculiar de
la filt_ación en el estudio cinematográfico está en que ella
pone al sistema de aparatos en el lugar del público. Se anula

de esta manera el aura que está alrededor del intérprete, y

con ella al mismo tiempo la que está alrededor de lo interpretado.

No debe sorprender que sea precisamente un hombre de teatro
como Pirandello quien, al caracterizar al intérprete de cine,
toque involuntariamente el fundamento de la crisis que vemos afectar actualmente al teatro.* Nada hay, en efecto, como

el escenario teatral, que sea más decididamente opuesto a
una obra de arte tomada ya completamente por la reproducción técnica 0, más aún —como en el cine—, resultante de ella.

Así lo confirma cualquier consideración detenida. Observadores calificados han reconocido hace tiempo que, en la representación filmica “los mejores resultados se alcanzan casi
siempre mientras menor es la “actuación” que hay en ellos [...]
Amheim, en 1932, ve el último desarrollo de esto en el hecho

de que el actor es tratado como un elemento de utilería que
se selecciona según sus aptitudes y que [...] se introduce en el
lugar adecuado”.” Con esto se conecta de la manera más es-.
trecha lo siguiente: E actor que se desenvuelve en el escenarío se identifica con su papel. Al intérprete de cine, muy a
* En A, la frase anterior es otra:

“El arte contemporáneo puede contar con mayor efectividad mientras
más encauzado esté hacia la reproductibilidad. Es natural que, entre todas
las artes, sean las dramáticas las que más evidentemente estén afectadas
por la crisis”.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBIIDAD TÉCNICA

“7

menudo, esto le está prohibido. Su desempeño no es de ninguna manera unitario sino que está conjuntado a partir de
muchos desempeños singulares. Además de consideraciones
acerca del alquiler de los estudios, de la disponibilidad de
colegas, de decorados, etcétera, son necesidades elementales

de la maquinaria las que desintegran la actuación del intérprete en una serie de episodios montables. Se trata sobre todo
de la iluminación, cuya instalación obliga a que la representación de un suceso que aparece sobre la pantalla en una
sucesión unitaria y ágil se realice en una serie de tomas singulares, que en el estudio pueden llegar a estar separadas por
horas. Por no hablar de otros montajes más evidentes. El salto
desde una ventana, por ejemplo, puede ser filmado en el estudio como un salto desde una plataforma, mientras que la huída que viene a continuación puede serlo más tarde, incluso

semanas después, en una toma en exteriores. Por lo demás, es

fácil imaginar casos más exagerados. Se le puede pedir al
intérprete que se asuste después de escuchar unos golpes a la
puerta. Y puede ser que esta acción no resulte como se
deseaba. El director puede entonces, en otra ocasión, cuando
el actor se encuentre nuevamente en el estudio, ordenar que

suene un disparo a sus espaldas. El susto del actor en ese instante puede ser filmado y montado posteriormente en la película. Nada muestra de manera más contundente que el arte se ha
escapado del reino de la “apariencia bella” que era tenido has-

ta ahora como el único en donde podía prosperar.'*

* En A, el texto de esta tesis continúa:
“El proceder del director que provoca experimentalmente un susto
real del intérprete con el fin de filmar el susto de la persona representada
es perfectamente genuino en el cine. En la grabación filmica, ningún intérprete puede pretender una visión de conjunto del contexto en el que se

72

WALTER BENJAMIN

encuentra su desempeño. Pedir un desempeño sin conexión vivencial inmediata con una situación no arreglada para el efecto es algo común en
todo examen, lo mismo en el deporte que en el cine. En cierta ocasión,

Asta Nielsen la puso en evidencia de manera impresionante. Había una
pausa en los estudios. Se filmaba una película basada en £l idiofa, de
Dotoievsky. Asta Nielsen, que hacía el papel de Aglaia, conversaba con
un amigo. La escena que venia era una de las principales: Aglaia observa
de lejos al príncipe Mishkin mientra pasa de largo acompañado de Nastasia Filipovna, y las lágrimas le saltan a los ojos. Asta Nielsen, que durante
toda la conversación había rechazado los elogios de su amigo, reparó de
pronto en la intérprete de Nastasia que iba y venía en el fondo del estudio
consumiendo su desayuno. “"Mire usted, esto es lo que entiendo yo por una
representación en cine', le dijo a su amigo, mientras lo miraba con unos
ojos que, como lo pedía la escena próxima, se habían llenado de lágrimas
al notar a su colega, pero sin que los acompañara el menor gesto de su
rostro.

“Las exigencias técnicas que se le ponen al intérprete de cine son diferentes de las que corresponden a un actor de teatro. Casi nunca las estrellas
de cine son actores sobresalientes en el sentido teatral. Por lo general han
sido más bien actores de segunda o tercera categoría a los que el cine ha
abierto una gran carrera. En dirección contraria, son raros los mejores intérpretes de cine que han pasado del cine al escenario; el intento ha fracasado
en la mayoría de los casos. (Este hecho tiene que ver con la peculiar naturaleza del cine, para el que es menos importante que el intérprete represente
a otro ante el púbico a que se represente a sí mismo ante el sistema de
aparatos.) El actor de cine Úpico sólo se representa a sí mismo. Está en
contraposición al tipo de mimo. Este hecho limita su empleo sobre el escenario pero lo amplía extraordinariamente en los estudios de cine. La estrella
de cine llega a su público sobre todo porque, a partir de él, a cada individuo
parece abrírsele la posibilidad de “entrar en el cine”. La idea de ser repro-ducido en un sistema de aparatos ejerce sobre el hombre actual una in“mensa atracción. Anteriormente también, sin duda, cualquier muchacha
se ilusionaba con subir al escenario. Pero el sueño de entrar en el cine
tiene dos ventajas frente a esa ilusión: en primer lugar, es más realizable,
porque el consumo de intérpretes por el cine (donde cada intérprete sólo
se actúa a sí mismo) es mucho mayor que el del teatro; en segundo lugar,
es más atrevido, porque la idea de ver difundida masivamente la propia
presencia, la propia voz, hace palidecer al brillo del gran actor”.

Exhibición ante la masa

Al entrar el sistema de aparatos en representación del hom-

bre, la autoenajenación humana ha sido aprovechada de una

manera extremamente productiva. Es un aprovechamiento
que puede medirse en el hecho de que la extrañeza del actor
ante el aparato descrita por Pirandello es originalmente del
mismo tipo que la extrañeza' del hombre ante su aparición
en el espejo, aquella en la que los románticos gustaban detenerse. Sólo que ahora esta imagen especular se ha vuelto separable de él, transportable. ¿Y a dónde se la transporta? Ante
la masa."' El intérprete de cine no deja de estar consciente de
esto ni por un instante. Al estar ante el sistema de aparatos,

sabe que en Última instancia con quien tiene que vérselas es*
con la masa. Es esta masa la que habrá de supervisarlo. Y ella,
precisamente, no es visible, no está presente mientras él cum* En A forma parte de la tesis 11; en D lleva el número X.
! En A, esta frase se completa así: “del hombre romántico ante su imagen en el espejo —que, como se sabe, era un motivo preferido de Jean
Paul—.
* En D, este párrafo continúa así:
*..."con el público: el público de los consumidores que conforman el
mercado. Ese mercado en el que entra no sólo con su fuerza de trabajo
sino toda su corporeidad, con almay corazón, es para él, en el momento del
desempeño que le corresponde, tan poco aprehensible como lo es para
cualquier otro producto elaborado en una fábrica. ¿No tiene este hecho su
parte en el embarazo, en el nuevo tipo de pánico que, según Pirandello, se
abate sobre el intérprete ante el sistema de aparatos? A la contracción del
aura, el cine responde con una construcción artificial de la “personality
fuera de los estudios. El culto de las estrellas, promovido por el capital

[73]

74

WALTER BENJAMIN

ple el desempeño artístico que ella supervisará. La invisibilidad de la masa incrementa la autoridad de la supervisión.
No debe olvidarse, sin embargo, que la valoración política de
esta supervisión se hará esperar hasta que el cine haya sido
liberado de las cadenas de su explotación capitalista. Porque,
a causa del capital invertido en el cine, las oportunidades revo-

lucionarias de esta supervisión se encuentran convertidas en
contrarrevolucionarias. No es sólo que el culto a las estrellas
promovido por él conserve aquella magia de la personalidad
—misma que ya hace mucho no consiste en otra cosa que en
el brillo dudoso de su carácter mercantil—; también su com-

plemento, el culto del público, fomenta por su parte aquella
constitución corrupta de la masa que el fascismo intenta poner en lugar de la que proviene de su conciencia de clase.'“

invertido en el cine, conserva aquella magia de la personalidad que ya
hace mucho no consiste en otra cosa que en el brillo dudoso de su carácter
mercantil. Mientras sea el capital el que ponga la pauta, al cine de hoy no
se le podrá reconocer otro mérito revolucionario que el de haber impulsado una crítica revolucionaria de las ideas heredadas acerca del arte. No
negamos que, más allá de eso, en casos especiales el cine actual puede
impulsar una crítica revolucionaria de las condiciones sociales o incluso
del sistema de propiedad. Pero así como la investigación no pone el énfasis en ello, tampoco lo pone la producción cinematográfica de Europa
occidental”.

XIIT*

Derecho a ser filmado
Tal como sucede con la técnica del deporte, la técnica del

cine implica que todo el que presencia los desempeños exhibidos por ella lo hace en calidad de semiexperto. Para detectar esta vinculación basta con oír cómo discuten los repartidores de periódicos, apoyados en sus bicicletas, los resultados

de una carrera ciclista." En el caso del cine, el noticiero sema-

nal demuestra con toda claridad que cualquier persona puede
encontrase en la situación de ser filmada. Pero no sólo se trata
de esta posibilidad; todo hombre de hoy tiene derecho a ser
álmado. La mejor manera de precisar en qué consiste este derecho es echar una mirada sobre la situación histórica de la
literatura actual. Durante siglos, en la literatura las cosas se
daban de tal manera, que un reducido número de escritores
se encontraba frente a un número de lectores muchos miles
de veces mayor. Pero a finales del siglo pasado se presentó un
cambio. La expansión creciente de la prensa puso a disposición

* En D forma parte de la tesis X.
' En D, este pasaje continúa:
“No por nada los editores de periódicos organizan competencias ciclisticas entre sus repartidores. El interés que despiertan en ellos es grande
puesto que el vencedor tiene la oportunidad de ascender de repartidor a
corredor de carreras. Así, por ejemplo, el noticiero semanal da a cual-

quiera la oportunidad de ascender de simple peatón a extra de cine; de
manera parecida puede incluso verse incluido en una obra de arte —piénsese en Tres canciones por Lenin, de Wertov, o en Borinage, de Iven—”.

[75]

WALTER BENJAMIN
de los lectores órganos locales siempre nuevos de expresión

política, religiosa, científica y profesional. Fue así que una parte
creciente de los lectores pasó a contarse también —aunque

fuera sólo ocasionalmente— entre quienes escribían. Comenzó con el “buzón” que abrió para ellos la prensa diaria, y para
nuestros días la situación es tal, que no hay apenas un europeo que se encuentre en el proceso de trabajo que no pueda
encontrar, en principio, en algún lugar, la oportunidad de
publicar una experiencia laboral, un reclamo, un reportaje o
algo parecido. Con ello, la distinción entre autor y público se
encuentra a punto de perder su carácter fundamental; se con-

vierte en una distinción funcional que se reparte en cada caso
de una manera u otra. El lector está en todo momento listo
para convertirse en alguien que escribe. El haberse vuelto
experto, para bien o para mal, en un proceso de trabajo extremadamente especializado —aunque no lo fuera más que en
una operación menor— le ha valido una entrada a la autoría.
El trabajo mismo toma la palabra.* Y su exposición en palabras forma parte de la pericia que es requerida para su
ejecución. La competencia literaria no se basa ya en una educación especializada sino en una politécnica; se vuelve así un
bien común.* Todo esto puede ser trasladado sin más al cine,
* En D se añade: “en la Unión Soviética”.
* En D entra aquí la siguiente nota (21):
'
“Las técnicas correspondientes pierden su carácter de privilegio. Aldous Huxley escribe: “Los progresos técnicos han [...] conducido a la vulgaridad [...] La reproductibilidad técnica y la prensa rotativa han permitido
la multiplicación impredecible de escritos e imágenes. La educación escolar generalizada y los sueldos relativamente altos han creado un público
muy amplio que puede leer y hacerse de material de lechura y material
gráfico. Para proporcionar este material se ha establecido una industria
considerable. Las dotes artísticas son sin embargo algo sumamente raro;
de lo que se sigue [...] que en toda época y en todo lugar, la mayor parte

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTBBILIDAD TÉCNICA

— 77

donde desplazamientos que en el campo de la escritura requirieron siglos se han cumplido en un decenio. Porque en la
praxis del cine —sobre todo del ruso— este desplazamiento se
ha realizado ya parcialmente. Una parte de los intérpretes
que encontramos en el cine ruso no son intérpretes en el sentido en que nosotros lo entendemos, sino gente que se auto—
exhibe y en primer lugar, por cierto, en su proceso de trabajo.
de la producción artística ha sido de bajo valor. Hoy en día, el porcentaje de los descalificados para la producción artística global es mayor que
nunca antes [...] Estamos aquí ante un simple problema aritmético. A lo
largo del siglo pasado, la población de Europa occidental aumentó a
algo más del doble. El material de lectura y formación ha crecido sin
embargo, en un cálculo aproximado, en proporción de 1 a 20, tal vez a 50
o incluso a 100. Si una población de x millones tiene n talentos artísticos,
otra de 2x millones tendrá probablemente 2n talentos artísticos. Ahora
bien, la situación puede resumirse así: si hace 100 años se publicaba una
página impresa con material de lectura y formación, hoy en día se publican 20, cuando no 100 páginas del mismo tipo. Si, por otra parte, hace 100
años existía un talento artístico, hoy existen dos en su lugar. Concedo que
hoy, como resultado de la educación generalizada, un gran número de
talentos virtuales, que antes no podían desarrollarse, pueden volverse pro-

ductivos. Supongamos entonces [...] que 3 o incluso 4 talentos artísticos de
hoy corresponden al talento artístico de antes. De todos modos sigue siendo indudable que el consumo de materiales de lectura y formación ha
rebasado ampliamente la producción natural de escritores dotados y de
dibujantes dotados. Con el material audible no sucede otra cosa. La prosperidad, el gramófono y la radio han dado vida a un público cuyo consumo de material audible está fuera de toda proporción respecto del
crecimniento de la población y, en esa medida, del crecimiento normal de
número de músicos talentosos. Resulta, entonces, que en todas las artes,

hablando lo mismo en términos absolutos que relativos, la producción de
descalificados es mayor de lo que fue antes; y así tendrá que seguir mientras la gente mantenga, como ahora, un consumo desproporcionadamente
grande de materiales de lectura y audición (Aldous Huxley, Croisiere
d'hiver. Voyage en Amérique Centrale (1933) [Traducción de Jules Castier], París, 1935, pp. 273-275). Este modo de ver las cosas obviamente no

es progresista”.

78

WALTER BENJAMIN

En Europa occidental la explotación capitalista del cine prohíbe tener en cuenta el derecho legítimo que tiene el hombre
de hoy a ser objeto de una reproducción. Por lo demás, también la desocupación lo vuelve prohibitivo puesto que excluye de la producción a grandes masas que tendrían derecho a ser reproducidas ante todo en su proceso de trabajo. En
estas condiciones, la industria cinematográfica tiene interés
en acicatear la participación de las masas mediante representaciones ilusorias y especulaciones dudosas. Para lograr este
efecto ha puesto en movimiento un enorme aparato publicitario: ha puesto a su servicio la carrera y la vida amorosa de
las estrellas, ha organizado consultas populares, ha convoca-

do concursos de belleza.* Todo ello para falsificar, por la vía

de la corrupción, el interés originario y justificado de las ma-

sas en el cine: un interés en el autoconocimiento y así tam-

bién en el conocimiento de su clase. Lo que rige para el fascismo en general rige por ello también en particular para el capital
invertido en el cine; con él, una necesidad indispensable de

nuevas estructuraciones sociales está siendo e>.cplotada secretamente para beneficio de una minoría de propietarios. La
expropiación del capital invertido en el cine es por ello una
exigencia urgente del proletariado.

* En C, el final de esta tesis es otro:

“Explota así un elemento dialéctico en la formación de
aglomera a las masas espectadoras de las proyecciones es
aspiración del individuo aislado a ponerse en el lugar del
separarse de la masa. La industria cinematográfica juega

la masa: lo que
precisamente la
star, es decir, a
con este interés

completamente privado para corromper el justificado interés original de
las masas en el cine”.

XIV*

El pintor y el hombre de la cámara
La toma cinematográfica, en especial la del cine sonoro, ofrece una visión que antes hubiera resultado absolutamente in-

imaginable en todas partes. Presenta un suceso en referencia al cual no existe ya ningún punto de vista capaz de dejar
fuera del campo visual del espectador aquellos elementos que
no pertenecen al hecho escénico en cuanto tal: el aparato de
grabación, la maquinaria de iluminación, el equipo de asistentes, etcétera (a no ser que el enfoque de su pupila coincida

con el de la cámara). Este hecho, y éste más que cualquier
otro, convierte en superficiales e insignificantes las similitudes .
que tal vez puedan existir entre una escena en el estudio de
filmación y otra en el escenario. El teatro conoce, por princi-

pio, la posición desde la cual deja de ser reconocido fácilmente como ilusorio lo que ocurre sobre el escenario. Frente
a la escena grabada por el cine, este lugar no existe. Su consistencia ilusoria es una consistencia de segundo g1'ád0; es el

resultado de la edición. Es decir: en el estudio cinematográfico el sistema de aparatos ha penetrado tan profundamente en
la realidad, que el aspecto puro de ésta, libre de ese cuerpo
extraño que sería dicho sistema, es el resultado de un procedimiento especial, a saber: de la grabación mediante un aparato

fotográfico enfocado apropiadamente y de su montaje con otras
grabaciones del mismo tipo. La presencia de la realidad en
tanto que libre respecto del aparato se ha vuelto aquí su pre* En D, X1.

[79

80

WALTER BENJAMIN

sencia más artificial, y la visión de la realidad inmediata una
flor azul cultivada en el país de la técnica.
El mismo estado de cosas que se distingue de esta manera
frente al que prevalece en el teatro puede ser confrontado de
una manera incluso más notoria frente al de la pintura. En
este caso debemos plantearnos la siguiente pregunta: ¿qué
relación guarda el operador con el pintor? Para responderla,
permitámonos una construcción auxiliar basada en el concepto de operador que es usual en cirugía. El cirujano representa uno de los polos de un orden cuyo otro polo lo ocupa el
mago. La actitud del mago que cura al enfermo poniendo su
mano sobre él es diferente de la del cirujano que practica una
intervención en el mismo. El mago mantiene la distancia natural entre él mismo y el paciente 0, con más exactitud, la

reduce un poco gracias al toque de su mano y la incrementa
mucho gracias a su autoridad. El cirujano procede a la inversa: reduce mucho la distancia con el paciente —al penetrar en
su interior— y la incrementa sólo un poco en virtud del cuidado con que su mano se mueve entre los órganos. En una
palabra, a diferencia del mago (que está todavía en el médico

practicante), el cirujano renuncia en el instante decisivo a ponerse de hombre a hombre frente a su enfermo; en lugar de

ello se introduce operativamente en él. El mago y el cirujano
se comportan, respectivamente, como el pintor y el operador
de la cámara. El pintor observa en su trabajo una distancia
natural frente a lo dado; el operador de la cámara, en cambio,

penetra profundamente en el tejido mismo del hecho de estar
dado.” Las imágenes que ambos extraen son enormemente
* En D entra aquí la siguiente nota (22):
“El atrevimiento del camarógrafo es, de hecho, comparable al del operador quirúrgico. En una guía de habilidades específicamente gestuales de

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

81

distintas. La del pintor es una imagen total; la del operador de

la cámara es una imagen despedazada muchas veces, cuyas
partes se han juntado de acuerdo a una nueva legalidad. S?
para el hombre de hoy la más significativa de todas las representaciones de la realidad es la cinematográfica, ello se debe
a que ésta entrega el aspecto de la realidad como una rea-

lidad libre respecto del aparato —que él tiene derecho de ex-

igir en la obra de arte— precisamente sobre la base de su
compenetración más intensa con

ese aparato.

la técnica, Luc Durtain presenta aquellas “que en la cirugía son indispensables en ciertas intervenciones especialmente dificiles. Elijo como ejemplo un caso de la otorrinolaringología [...], me refiero a la conocida como
el procedimiento de perspectiva endonasal; remito asimismo a las habilidades acrobáticas que la cirugia de la laringe debe completar guiándose
por la imagen invertida en el espejo laríngeo; y podría mencionar también
la cirugía del oído, que recuerda el trabajo de precisión de un relojero.
Qué sucesión ascendente de la acrobacia muscular más sutil no se exige
del hombre que quiere reparar o salvar un cuerpo humano; piénsese sola-

mente en la operación de las cataratas en la que, por decirlo así, se entabla

un debate entre el acero y ciertas partes casi líquidas del tejido o las decisivas intervenciones intestinales (laparotomía)' (Luc Durtain, “La technique
et Thomme”, en Vendredi, 13 de marzo de 1936, número 19)>.

Recepción de la pintura

La reproductibilidad técnica de la obra de arte transforma
el comportamiento de las masas con el arte. Por ejemplo, de
ser el más atrasado a la vista de un Picasso, se convierte en el

más adelantado ante un Chaplin, por ejemplo. El comporta-

miento adelantado se caracteriza aquí por el hecho de que,
en él, el placer en la mirada y la vivencia entra en una combi-

nación inmediata y de interioridad con la actitud del dictaminador especializado. Tal combinación es un indicio social
importante. En efecto, mientras más disminuye la importancia social de un arte, más se separan en el público —como se
observa claramente en el caso de la pintura— la actitud de
disfrute y la actitud crítica. Lo convencional es disfrutado sin
ninguna crítica; lo verdaderamente nuevo es criticado con
repugnancia. No así en el cine." Y este factor es el decisivo: en
ninguna parte como en el cine las reacciones individuales,
cuya suma compone

la reacción masiva del público, se en-

cuentran condicionadas de entrada por su masificación inminente. Son reacciones que se supervisan al manifestarse. Y la
comparación con la pintura sirve una vez más. El cuadro tuvo
siempre una señalada preferencia a ser contemplado por uno
o por pocos. La contemplación simultánea de cuadros por
parte de un público numeroso, tal como aparece en el siglo

* En D, XI
* En D, en lugar de esta frase, dice: “En el cine coinciden la actitud

crítica y la actitud de disfrute por parte del público”.

[82]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

83

XIX, es un síntoma temprano de la crisis de la pintura, que de
ningún modo

fue desatada sólo por la fotografía, sino, con

relativa independencia de ésta, por las exigencias planteadas
por la obra de arte a la masa.
En efecto, el hecho es que la pintura no está en condición
de ofrecerse como objeto de una recepción colectiva simultánea, como fue el caso de la arquitectura desde siempre,

como lo fue una vez para la épica, como lo es hoy para el

cine. Y aunque de este hecho por sí mismo no se puedan
sacar conclusiones acerca de la función social de la pintura,
de todas maneras constituye un serio obstáculo allí donde la
pintura, dadas ciertas circunstancias especiales, y en cierto
modo contra su propia naturaleza, se pone ante las masas. En
las iglesias y monasterios de la edad media y en las cortes de
los principados, hasta fines del siglo XV la recepción colectiva de pinturas tuvo lugar de manera gradual y por mediación jerárquica, y no simultáneamente. Y si las cosas han cambiado es porque tal cambio pone de manifiesto el peculiar
conflicto en que se ha enredado la pintura a causa de la reproductibilidad técnica de la imagen. Porque, aunque se logró

llevarla ante las masas al presentarla en galerías y salones, de

todos modos no se encontró una vía por la que las masas
hubiesen podido organizarse y supervisarse a sí mismas en
una recepción de ese tipo.* Así se explica que el mismo público
que reacciona

de manera

progresista ante una película

grotesca se vuelva anticuado ante el surrealismo.

* En A se encuentra aquí esta frase: “Hubieran tenido que llegar al

escándalo para expresar manifiestamente su juicio”.
En D entra aquí la siguiente nota (23):
“Este modo de ver las cosas puede parecer torpe; pero como lo muestra el gran teórico Leonardo, modos torpes de ver las cosas pueden ser

XvI

El ratón Mickey

Entre las funciones sociales del arte, la más importante es la

de establecer un equilibrio entre el hombre y el sistema de

aparatos. El cine resuelve esta tarea no sólo con la manera en

que el hombre se representa ante el sistema de aparatos de

filmación, sino con la manera en que, con la ayuda de éste, se
hace una representación del mundo circundante.' Al hacer
útiles en su oportunidad. Leonardo compara la pintura con la música en
las siguientes palabras: “La pintura aventaja a la música y sobre ella señorea
porque no muere fulminada tras su creación, como la desventurada música [...] Cosa más noble es la que más permanece. Conque la música, que
no así ha nacido cuando ya se desvanece, es menos digna que la pintura,
que con vidrio dura eternamente' ( [Leonardo da Vinci, Frammenéti letterarii e filosofici| cit. por Fernand Baldensperger, “Le raffermissement des
techniques dans la littérabure occidentale de 1840', en Revue de Littérature Comparée, xVf, París, 1935, p. 79 [nota 1)”.
* En D, XII.
* En la tesis XII de D se encuentra aquí el siguiente pasaje, con su nota
correspondiente (24):
“Una mirada a la psicología del desempeño ilustra la capacidad de someter
a examen que tiene el sistema de aparatos. Una mirada al psicoanálisis ilustra esa
capacidad desde otro lado. En efecto, el cine ha enriquecido nuestro mundo de
lo significativo con métodos que pueden ilustrarse con los de la teoría freudiana.
Un lapsus en una conversación pasaba más o menos inadvertido hace cincuenta
años. Que abriera de pronto una perspectiva de profundidad en una conversación que parecía desenvolverse superficialmente, era algo excepcional.
Esto ha cambiado desde la Psicopatología de la vida cotidiana Se han aislado
y vuelto analizables cosas que antes nadaban confundidas en la amplia
corriente de lo percibido. El cine ha tenido como consecuencia una profundización parecida de la percepción en toda la amplitud del mundo de
lo significativo, primero en lo que respecta a lo visual y últimamente tam-

[84]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TEÉCNICA

85

ampliaciones del inventario de éste último, al subrayar detalles escondidos de utensilios que nos son familiares, al inves-

tigar ambientes banales bajo la conducción genial del lente,

el cine incrementa, por un lado, el reconocimiento de las ine-

vitabilidades que rigen nuestra existencia, pero llega, por otro,
a asegurarnos un campo de acción inmenso e insospechado.
Parecía que nuestras tabernas y avenidas, nuestras oficinas y
cuartos amueblados, nuestras estaciones y fábricas nos encerbién a lo sonoro. Se trata solamente del otro lado del hecho de que los
desempeños presentados por el cine son analizables de manera más exacta y bajo puntos de vista mucho más numerosos que los desempeños representados en pinturas o sobre el escenario teatral. Frente a la pintura, lo
que promueve en un desempeño filmado su mayor disposición a ser analizado es la presentación incomparablemente más precisa de la situación.
Frente al teatro, la mayor disposición al análisis que tiene el desempeño
filtnado se debe a que es mayor la capacidad de ser tomado aisladamente.
Este hecho —y esta es su principal importancia— trae consigo la tendencia
a promover la interpenetración del arte y la ciencia. En efecto, frente a un

comportamiento aislado y preparado liumpiamente —como un músculo en

un cuerpo— dentro de una determinada situación, resulta dificil decidir

qué es lo que atrae más fuertemente en él: su valor artístico o su utilidad
científica. Una de las funciones revolucionarias del cine será llevar a que
sean reconocidas como idénticas la utilización arfística y la científica de la
fotografía, mismas que antes se encontraban separadas. |Nota:| Si buscamos una analogía para esta situación, la pintura del Renacimiento nos
ofrece una muy sugerente. También allí nos encontramos con un arte cuyo
impulso incomparable y cuya significación no se basan en poca medida en
el hecho de que es capaz de integrar una serie de nuevas ciencias o al
menos de nuevos datos de la ciencia. Se sirve de la anatomía y la perspectiva, de la matemática, la meteorología y la teoría de los colores. 'Qué hay

más lejano de nosotros”, escribe Valéry, “que la extraña exigencia de Leonardo, para quien la pintura era la meta última y la más alta demostración del conocimiento humano, a tal punto, que ella, según él estaba

convencido, requería un saber omniabarcante.

Él mismo

no retrocedia

ante análisis teóricos cuya profundidad y precisión nos dejan ahora sin
palabras' (Paul Valéry, Pieces sur Vart, loc. cit, p. 191, “Autour de CorotY”.

80

WALTER BENJAMIN

raban sin esperanza; pero llegó el cine con su dinamita de las

décimas de segundo e hizo saltar por los aires este mundo

carcelario, de tal manera que ahora podemos emprender sin
trabas viajes de aventura en el amplio espacio de sus ruinas.

Con las ampliaciones se expande el espacio; con las tomas en
cámara lenta, el movimiento. Y así como con la ampliación
no se trata solamente de una simple precisión de algo que “de

todas maneras” sólo se ve borrosamente, sino que en ella se
muestran más bien conformaciones estructurales completamente nuevas de la materia, asi también la cámara lenta no
sólo muestra motivos dinámicos ya conocidos, sino que des-

cubre en éstos otros completamente desconocidos “que no

surten el efecto de movimientos rápidos que han sido retardados, sino de movimientos diferentes, peculiarmente resbaladizos, flotantes, sobrenaturales.”'3 De esta manera se vuelve
evidente que una es la naturaleza que se dirige al ojo y otra la

que se dirige a la cámara. Otra, sobre todo porque, en el
lugar del espacio trabajado concientemente por el hombre,
aparece otro, trabajado inconscientemente. Si bien no es cosa

rara interpretar, aunque sea burdamente,

el caminar de una

persona, nada se sabe de la actitud de ese alguien en la fracción de segundo en la que aprieta el paso. Si bien nos damos

cuenta en general de lo que hacemos cuando tomamos con la

mano un encendedor o una cuchara, apenas sabemos algo de

lo que se juega en realidad entre la piel y el metal; para no

hablar del modo en que ello varía con los diferentes estados
de ánimo en que nos encontramos. Es aquí donde interviene

la cámara con todos sus accesorios, sus soportes y andamios;

con su interrumpir y aislar el decurso, con su extenderlo y
atraparlo, con su magnificarlo y minimizarlo. Sólo gracias a
ella tenemos la experiencia de lo visual inconsciente, del mis-

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBLIDAD TÉCNICA

— 87

mo modo en que, gracias al psicoanálisis, la tenemos de lo
pulsional inconsciente.
Por lo demás, entre estos dos tipos de inconsciente existe la
más estrecha de las relaciones, puesto que los diversos as-

pectos que el aparato de filmación puede sacar de la realidad
se encuentran en su mayor parte sólo fuera del espectro normal de las percepciones sensoriales. Muchas de las deformaciones y estereotipos, de las mutaciones y catástrofes que
pueden afectar al mundo áptico en las películas lo afectan de
hecho en psicosis, en alucinaciones, en sueños. Y así, aquellos

modos de obrar de la cámara son otros tantos procedimientos
gracias a los cuales la percepción colectiva es capaz de apropiarse de los modos de percepción individuales del psicótico
o del soñador. El cine ha abierto una brecha en la antigua
verdad heracliteana: los que están despiertos tienen un mundo en común, los que sueñan tienen uno cada uno. Y lo ha
hecho, por cierto, mucho menos a través de representaciones

del mundo onírico que a través de creaciones de figuras del
sueño colectivo, como el ratón Mickey que hoy da la vuelta al

mundo.

Cuando uno se da cuenta de las peligrosas tensiones que la
tecnificación* y sus secuelas han generado en las grandes masas —tensiones que en estadios críticos adoptan un carácter
psicótico—, se llega al reconocimiento de que esta misma tecnificación ha creado la posibilidad de una vacuna psíquica con-

tra tales psicosis masivas mediante determinadas películas en

las que un desarrollo forzado de fantasías sádicas o alucinaciones masoquistas es capaz de impedir su natural maduración
peligrosa entre las masas. La carcajada colectiva representa
* En C se precisa aqui: “que la técnica racional ha engendrado

en el

seno de una economía capitalista que es irracional ya desde hace tiempo”.

83

WALTER BENJAMIN

un estallido anticipado y bienhechor de psicosis colectivas de
ese tipo. Las colosales cantidades de sucesos grotescos que se
consumen en el cine son un agudo indicio de los peligros que
amenazan a la humanidad a partir de las represiones que la
civilización trae consigo. Las grotescas películas americanas y
las películas de Disney producen una voladura terapéutica
del inconsciente."* Su antecesor fue el “excéntrico”. Él fue el
primero en sentirse en casa en los nuevos escenarios que surgieron gracias al cine, en estrenarlos. En este contexto, Chaplin
tiene su lugar como figura histórica

Dadaísmo

Desde siempre, una de las tareas más importantes del arte ha
sido la de generar una demanda a cuya satisfacción plena no le

ha llegado la hora todavía.'* La historia de toda forma artística

tiene épocas críticas en las que esta forma presiona en dirección
a efectos que sólo podrán alcanzarse, sin que sean forzados, sobre

un estándar técnico transformado, es decir, con una nueva forma

artística. Las extravagancias y crudezas que resultan de ello, sobre

todo en las llamadas épocas de decadencia, surgen en realidad
de la parte de esa forma que es la más rica en energía histórica.
En este tipo de barbaridades se ha regocijado últimamente el
dadaísmo. Apenas ahora es reconocible su impulso: el dadaísmo
intentó generar con los medios de la pintura (o de la literatura,

en su caso) los efectos que el público encuentra ahora en el cine.
Toda producción de una demanda que sea básicamente
nueva, innovadora, mandará su disparo más allá de la meta.

El dadaísmo hace esto a tal grado, que sacrifica los valores

comerciales, tan característicos del cine, en beneficio de intenciones más significativas —que

por supuesto no son cons-

cientes para él en la forma aquí descrita—. Los dadaístas da-

ban mucho menos peso a la utilidad mercantil de sus obras

de arte que a su inutilidad como objetos de recogimiento con-

templativo. Esta inutilidad la buscaron en buena medida mediante un envilecimiento radical de sus materiales. Sus poe-

mas son “ensaladas de palabras”, contienen giros obscenos y
cuanto sea imaginable de basura verbal. Así también sus pin* En D, XIV.

[89]

90

WALTER BENJAMIN

turas, sobre las que pegan botones y billetes de tranvía. Lo
que alcanzan con tales medios es una destrucción irreverente
del aura de sus engendros, a los que imprimen la marca de
una reproducción sirviéndose de los medios propios de la
producción. Ante un cuadro de Arp o un poema de August
Stramm es imposible darse un tiempo para el recogimiento y
la ponderación como ante un cuadro de Derain o un poema
de Rilke. Frente al recogimiento, que se volvió escuela de comportamiento asocial con la degeneración de la burguesía,
aparece la distracción como un tipo de comportamiento social.* En efecto, las manifestaciones dadaístas garantizaban
una distracción muy evidente por cuanto ponían a la obra de
arte en el centro de un escándalo. Esta tenía que cumplir
sobre todo una exigencia: suscitar la irritación pública.
Con los dadaístas, la obra de arte dejó de ser una visión

cautivadora o un conjunto de convincente sonidos y se con-

virtió en un proyectil que se impactaba en el espectador; alcan-

zó una cualidad táctil.? Favoreció de esta manera la demanda
por el cine, cuyo elemento de distracción es igualmente en

* En D entra aquí la siguiente nota (27):
“La figura teológica originaria de este recogimiento es la conciencia de
estar a solas con su Dios. En esta conciencia se fortaleció, en la gran época
de la burguesía, la libertad para sacudirse la tutela de la Iglesia. En los

tiempos de su decadencia, la misma conciencia debió ser el vehículo de la

tendencia escondida a apartar de los asuntos de la comunidad esas fuerzas
que el individuo moviliza en su trato con Dios”.
* En A, en lugar de esta frase, se encuentra el siguiente pasaje: “Con
ello estuvo a punto de recobrar para el presente la cualidad táctil que le es
indispensable en las grandes épocas de la historia del arte.
“Que todo lo que percibimos, lo que llega a nuestros sentidos, es algo
que choca con nosotros —esta fórmula de la percepción onírica, que incluye el aspecto táctil de la percepción artística— fue puesto en vigor de
nuevo por el dadaismo”.

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

—91

primera línea táctil; se basa, en efecto, en el cambio de esce-

narios y de enfoques que se introducen, golpe tras golpe, en

el espectador.'** El cine liberó al efecto de shock fisico de la
envoltura moral en la que el dadaismo lo mantenía todavía
empaquetado."

* En D se intercala aquí el siguiente pasaje, con su nota respectiva (28):
“Duhamel, que odia el cine y no ha comprendido nada de su importancia, aunque sí algo de su estructura, asienta este hecho en el siguiente
apunte: “Ya no puedo pensar lo que quiero pensar; las imágenes móviles
se han puesto en el lugar de mis pensamientos” (Georges Duhamel, Scenes
de la vie future, 2a. ed., París, 1930, p. 527”.

! En D, entra aquí la siguiente nota (30):
“Así como para el dadaísmo, también para el cubismo y el futurismo es
posible sacar del cine claves importantes. Ambos aparecen como intentos

defectuosos, por parte del arte, de dar cuenta a su manera de la pene-

tración de la realidad mediante el sistema de aparatos. Son escuelas que, a
diferencia del cine, no emprendieron su intento mediante la utilización del
sistema de aparatos en la representación artística de la realidad, sino mediante una especie de amalgama de representación de la realidad y representación del sistema de aparatos. En el caso del cubismo, el papel principal en
esta amalgama le corresponde a la premonición de la construcción de este
sistema de aparatos como construcción basada en la visión; en el caso del

futurismo, a la premonición de los efectos de ese sistema de aparatos, tal
como se hacen patentes en el correr apresurado de la película”.

Recepción táctil y recepción visual

La masa es en nuestros días la matriz de la que surge renacido todo comportamiento frente a las obras de arte que haya
sido usual hasta ahora. La cantidad ha dado un salto y se ha
vuelto calidad: las masas de participantes, ahora mucho más
amplias, han dado lugar a una transformación del modo mismo de participar. El observador no debe equivocarse por el
hecho de que este modo de participación adopte de entrada
una figura desprestigiada.” Oirá lamentos porque las masas
buscan diversión en la obra de arte, mientras que el amante
del arte se acerca a ésta con recogimiento. Para las masas, la
obra de arte sería una ocasión de entretenimiento; para el

amante del arte, ella es un objeto de su devoción. En este

* En D lleva el número xv.
* D intercala aquí el siguiente pasaje, con su nota correspondiente (31):
“No han faltado sin embargo quienes se atienen apasionadamente a
este aspecto superficial del asunto. Entre ellos, es Duhamel quien se ha
expresado de manera más radical. Lo que le molesta sobre todo en el cine
es el tipo de participación que despierta en las masas. Habla del cine como
'una manera de matar el tiempo propia de parias, una diversión para cria-

turas incultas, miserables, agobiadas por el trabajo, consumidas por sus

preocupaciones [...], un espectáculo que no exige ningún tipo de concentración, que no presupone una capacidad de pensar (...], que no enciende
ninguna luz en el corazón y que no despierta otra esperanza que la esperanza ridícula de algún día volverse un «síar» en Los Ángeles” (Georges

Duhamel,

doc. cit, p. 58). Como

se ve, se trata en el fondo del mismo

lamento porque las masas buscan entretenimiento mientras que el arte
reclama recogimiento. Se trata de un lugar común que resulta cuestionable como base para una investigación acerca del cine”.

[92]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

93

punto es necesario mirar las cosas más de cerca. Diversión y

recogimiento están en una contraposición que puede formularse de la siguiente manera: quien se recoge ante una obra

de arte se hunde en ella, entra en la obra como cuenta la

leyenda del pintor chino que contemplaba su obra terminada. La masa, en cambio, cuando se distrae, hace que la obra

de arte se hunda en ella, la baña con su oleaje, la envuelve en

.su marea. Esto sucede de la manera más evidente con los
edificios. La arquitectura ha sido desde siempre el prototipo
de una obra de arte cuya recepción tiene lugar en medio de la
distracción y por parte de un colectivo. Las leyes de su recepción son de lo más aleccionadoras.

Los edificios acompañan a la humanidad desde su historia
originaria. Muchas formas artísticas han surgido y han perecido. La tragedia surge con los griegos para extinguirse con
ellos y revivir después de siglos. La épica, cuyo origen está en
la júventud

de

los pueblos,

se apaga

en

Europa

con

el

Renacimiento. La pintura de cuadros es una creación del Medioevo y nada le garantiza una duración ininterrumpida. La
necesidad humana de habitación es por el contrario permanente. El arte de construir no ha estado nunca en reposo. Su

historia es más prolongada que la de cualquier otro arte y
recordar la manera en que realiza su acción es de importancia
para cualquier intento de* explicar la relación de las masas
con la obra de arte. La recepción de los edificios acontece de
una doble manera: por el uso y por la percepción de los mismos. O mejor dicho: de manera táctil y de manera visual.
Para llegar a un concepto de esta recepción hay que dejar de
imaginarla como la que es usual en quienes sienten recogi* En A, esta frase termina así: “reconocer la función social de la relación de las masas con la obra de arte”.

94

WALTER BENJAMIN

miento ante ella; en los viajeros ante un edificio famoso, por

ejemplo. En el lado de lo táctil no existe un equivalente de lo
que es la contemplación en el lado de lo visual. La recepción

táctil no acontece tanto por la vía de la atención como por la
del acostumbramiento.

Ante la arquitectura, incluso la re-

cepción visual está determinada en gran parte por la última;
también ella tiene lugar mucho menos en un atender tenso
que en un notar de pasada. Conformada ante la arquitectura,
esta recepción alcanza en ciertas circunstancias un valor canóni-

co. Ello se debe a que /as tareas que se le plantean al aparato

de la percepción humana en épocas de inflexión histórica no

pueden cumplirse porla vía de la simple visión, es decir, de la
contemplación. Se realizan paulatinamente, por acostuni-

bramiento, según las indicaciones de la aprehensión táctil.
También el distraído puede acostumbrarse. Más aún: que

uno sea capaz de realizar ciertas tareas en medio de la distracción demuestra que se le ha vuelto costumbre resolverlas. A
través de una distracción como la que puede ofrecer el arte se

pone a prueba subrepticiamente en qué medida nuevas tareas se le han vuelto solucionables a la percepción. Además,
dado que en el individuo existe la tentación de eludir tales

tareas, el arte, allí donde puede movilizar a las masas, se vuel-

ca sobre la más dificil e importante de ellas. Actualmente lo
hace en el cine. Za recepción en la distracción, que se hace

notar con éntasis creciente en todos los ámbitos del arte y que
es el síntoma de transformaciones profundas de la percepción,*
tiene en el cine,su medio de ensayo apropiado. A esta forma
* En A, esta tesis termina de la siguiente manera:
“tiene en las salas de cine su lugar central. Y aquí, donde el colectivo
busca su diversión, no deja de estar la dominante táctil que rige en el
reordenamiento de la percepción. Es en la arquitectura en donde está en

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

195

de recepción el cine responde con su acción de shock.* Y se
convierte así, en esta perspectiva, en el referente actual más
importante de aquella doctrina de la percepción que se llamó

estética entre los griegos.

casa originalmente. Pero nada delata más claramente las enormes tensiones
de nuestro tiempo que el hecho de que esta dominante táctil esté vigente

incluso en lo óptico, que es precisamente lo que ocurre en el cine mediante el efecto de choque de la sucesión de imágenes: De esta manera, también en esta perspectiva, el cine se convierte en el referente actual más

importante de aquella doctrina de la percepción que se llamó estética entre los griegos”.
* En D, esta tesis continúa así:

“El cine hace retroceder al valor de culto no sólo por el hecho de que
pone al público en una actitud examinante, sino también porque esta actitud examinante no incluye un estado de atención dentro de la sala de

proyección. El público es un examinador, pero un examinador distraido”.

XIX*

Estética de la guerra

La proletarización creciente del hombre actual y la creciente

formación de masas son dos lados de un mismo acontecimiento. El fascismo' intenta organizar a las masas proletarias que se han generado recientemente, pero sin tocar las
relaciones de propiedad hacia cuya eliminación ellas tienden.
Tiene puesta su meta en lograr que las masas alcancen su
expresión (pero de ningún modo, por supuesto, su derecho).” Las masas tienen un derecho* a la transformación de
las relaciones de propiedad; el fascismo intenta darles una
expresión que consista en la conservación de esas relaciones.

Es por ello que el fascismo se dirige hacia una estetización de
la vida políticaS Con D' Annunzio, la decadencia hace su entrada en la vida política; con Marinetti, el futurismo, y con

Hitler, la tradición de Schwabing.**
Todos los esfiuerzos hacía una estetización de la política
culminan en un punto. Este punto es la guerra. La guerra, y
sólo la guerra, vuelve posible dar una meta a los más grandes
movimientos de masas bajo el mantenimiento de las relaciones
* En D lleva el título: “Epílogo”.
! En C, en lugar de “fascismo” se halla siempre “Estado totalitario”.
* En C: “Las masas tienden...”
S En D, la última frase de este párrafo dice: “A la violación de la masas,
a las que el fascismo rebaja en el culto a un caudillo, corresponde la violación de un sistema de aparatos que él pone al servicio de la producción de
valores de culto”.
** Barrio muniqués conocido por su ambiente entre bohemio y maleante [N. del T.|.

[96]

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

97

de propiedad heredadas. Así se formula el estado de cosas
cuando se lo hace desde la política. Cuando se lo hace desde
la técnica, se formula de la siguiente manera: sólo la guerra*
vuelve posible movilizar el conjunto los medios técnicos del
presente bajo el mantenimiento de las relaciones de propiedad.
Por supuesto que los fascistas, en su apología de la guerra, no
se sirven de estos argumentos; pero una mirada sobre tal apo-

logía es de todos modos ilustrativa. En el manifiesto de Marinetti con motivo de la guerra colonial en Etiopía se lee: “Desde hace veintisiete años, nosotros, los futuristas, nos hemos

expresado contra la calificación de la guerra como antiestética [...] De acuerdo con ello reconocemos: [...] la guerra es
bella porque, gracias a las máscaras antigás, a los megáfonos
que causan terror, a los lanzallamas y los pequeños tanques,
ella funda el dominio del hombre sobre la máquina sometida.
La guerra es bella porque inaugura la metalización soñada
del cuerpo humano. La guerra es bella porque enriquece los
prados en flor con las orquídeas en llamas de las ametralladoras. La guerra es bella porque unifica en una gran sinfonía
el fuego de los fusiles, los cañonazos,

los silencios, los per-

fumnes y hedores de la putrefacción. La guerra es bella porque
crea nuevas arquitecturas como la de los grandes tanques, la
de los aviones en escuadrones geométricos, la de las espirales de humo en las aldeas en llamas, y muchas otras cosas [...]
Poetas y artistas del futurismo, recordad estos principios de
una estética de la guerra para que vuestros esfuerzos por alcanzar una nueva poesía y una nueva plástica [...] sean iluminados por ellos”.'

* En C, en lugar de “guerra”, “guerra moderna”.

98

WALTER BENJAMIN

Este manifiesto tiene la ventaja de la claridad. Sus planteamientos merecen ser retomados por la reflexión dialéctica.
Para ella, la estética de la guerra actual se presenta de la manera siguiente: cuando la utilización natural de las fuerzas
productivas es retenida por el ordenamiento de la propiedad,
entonces el incremento de los recursos técnicos, de los ritmos,

de las fuentes de energía tiende hacia una utilización antinatural. Ésta se encuentra en la guerra, cuyas destrucciones apor-

tan la prueba de que la sociedad no estaba madura todavía
para convertir a la técnica en un órgano suyo, de que la técnica no estaba todavía suficientemente desarrollada como para
dominar las fuerzas sociales elementales. La guerra imperialista,
en sus más terroríficos rasgos, está determinada por la discrepancia entre unos medios de producción gigantescos y su
utilización insuficiente en el proceso de producción (con otras

palabras, por el desempleo y la escasez de medios de consumo). La guerra imperialista es una rebelión de la técnica
que vuelca sobre el material humano aquellas exigencias a
las que la sociedad ha privado de su material natural. En
lugar de generadores de energía, despliega sobre el campo la
energía humana corporizada en los ejércitos; en lugar del trá-

fico aéreo, pone el tráfico de proyectiles, y en la guerra química encuentra un medio para eliminar el aura de una manera
diferente.
“Fiat ars, pereat mundus”, dice el fascismo, y espera, como

la fe de Marinetti, que la guerra sea capaz de ofrecerle una

satisfacción artística a la percepción sensorial transformada
por la técnica. Este es, al parecer, el momento culminante de/
“Tart pour l'ar?”. La humanidad,

que fue una vez, en Ho-

mero, un objeto de contemplación para los dioses olímpicos,
se ha vuelto ahora objeto de contemplación para sí misma. Su

LA OBRA DE ARTE EN LA ÉPOCA DE SU REPRODUCTIBILIDAD TÉCNICA

99

autoenajenación ha alcanzado un grado tal, que le permite
vivir su propia aniquilación como un goce estético de primer
orden. De esto se trata en la estetización de la política puesta
en práctica por el fascismo. El comunismo* le responde con
la politización del arte.

* En C, en lugar de “El comunismo”, dice: “Las fuerzas constructivas
de la humanidad...”

