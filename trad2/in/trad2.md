«En un tiempo muy distinto del nuestro, y por hombres
cuyo poder de acción sobré las cosas era insignificante
comparado con el que nosotros poseemos, fueron instituidas nuestras Bellas Artes y fijados sus tipos y usos. Pero
el acrecentamiento sorprendente de nuestros medios, la flexibilidad y la precisión que éstos alcanzan, las ideas y costumbres que introducen, nos aseguran respecto de cambios
próximos y profundos en la antigua industria de lo Bello.
En todas las artes hay una parte física que no puede ser
tratada como antaño, que no puede sustraerse a la acometividad del conocimiento y la fuerza modernos. Ni la materia, ni el espacio, ni el tiempo son, desde hace veinte
años, io que han venido siendo desde siempre. Es preciso
contar con que novedades tan grandes transformen toda
la técnica de las artes y operen por tanto sobre la inventiva, llegando quizás hasta a modificar de una manera
maravillosa la noción misma del arte.»
PAUL VALÉRY, Piéces sur l'art («La conquéte

de l'ubiquité»).

PROLOGO

Cuando Marx emprendió el análisis de la producción
capitalista estaba ésta en sus comienzos. Marx orientaba
su empeño de modo que cobrase valor de pronóstico.
Se remontó hasta las relaciones fundamentales de dicha
producción y las expuso de tal guisa que resultara de ellas
lo que en el futuro pudiera esperarse del capitalismo.
Y resultó que no sólo cabía esperar de él una explotación
crecientemente agudizada de los proletarios, sino además
el establecimiento de condiciones que posibilitan su propia abolición.
17

La transformación de la superestructura, que ocurre
mucho más lentamente que la de la infraestructura, ha necesitado más de medio siglo para hacer vigente en todos
los campos de la cultura el cambio de las condiciones de
producción. En qué forma sucedió, es algo que sólo hoy
puede indicarse. Pero de esas indicaciones debemos requerir determinados pronósticos. Poco corresponderán a
tales requisitos las tesis sobre el arte del proletariado
después de su toma del poder; mucho menos todavía algunas sobre el de la sociedad sin clases; más en cambio unas
tesis acerca de las tendencias evolutivas del arte bajo las
actuales condiciones de producción. Su dialéctica no es
menos perceptible en la superestructura que en la economía. Por eso sería un error menospreciar su valor combativo. Dichas tesis dejan de lado una serie de conceptos
heredados (como creación y genialidad, perennidad y misterio), cuya aplicación incontrolada, y por el momento difícilmente controlable, lleva a la elaboración del material
fáctico en el sentido fascista. Los conceptos que seguidamente introducimos por vez primera en la teoría del arte
se distinguen de los usuales en que resultan por completo inútiles para los fines del fascismo. Por el contrario,
son utilizables para la formación de exigencias revolucionarias en la política artística.

1

La obra de arte ha sido siempre fundamentalmente
susceptible de reproducción. Lo que los hombres habían
hecho, podía ser imitado por los hombres. Los alumnos
han hecho copias como ejercicio artístico, los maestros
las hacen para difundir las obras, y finalmente copian
también terceros ansiosos de ganancias. Frente a todo
ello, la reproducción técnica de la obra de arte es ajgo
nuevo que se impone en la historia intermitentemente, a
18

empellones muy distantes unos de otros, pero con intensidad creciente. Los griegos sólo conocían dos procedimientos de reproducción técnica: fundir y acuñar. Bronces, terracotas y monedas eran las únicas obras artísticas que pudieron reproducir en masa. Todas las restantes eran irrepetibles y no se prestaban a reproducción
técnica alguna. La xilografía hizo que por primera vez se
reprodujese técnicamente el dibujo, mucho tiempo antes de que por medio de la imprenta se hiciese lo mismo
con la escritura. Son conocidas las modificaciones enormes que en la literatura provocó la imprenta, esto es, la
reproductibilidad técnica de la escritura. Pero a pesar
de su importancia, no representan más que un caso especial del fenómeno que aquí consideramos a escala de historia universal. En el curso de la Edad Media se añaden a la xilografía el grabado en cobre y el aguafuerte,
así como la litografía a comienzos del siglo diecinueve.
Con la litografía, la técnica de la reproducción alcanza un grado fundamentalmente nuevo. El procedimiento,
mucho más preciso, que distingue la transposición del
dibujo sobre una piedra de su incisión en taco de madera o de su grabado al aguafuerte en una plancha de cobre,
dio por primera vez al arte gráfico no sólo la posibilidad
de poner masivamente (como antes) sus productos en
el mercado, sino además la de ponerlos en figuraciones
cada día nuevas. La litografía capacitó al dibujo para
acompañar, ilustrándola, la vida diaria. Comenzó entonces a ir al paso con la imprenta. Pero en estos comienzos fue aventajado por la fotografía pocos decenios después de que se inventara la impresión litográfica. En el
proceso de la reproducción plástica, la mano se descarga
por primera vez de las incumbencias artísticas más importantes que en adelante van a concernir únicamente
al ojo que mira por el objetivo. El ojo es más rápido_captando que la mano dibujando; por eso~se~ha apresurado
tantísimo el proceso~3é~Ia~ reproducciónjglistjca que j a
puede ir a paso con la^ palabra habládáb Al rodar en el
estudio, el operaHoFHe cine fija las imágenes con la mis19

ma velocidad con la que ej^cj&rjbtajjia. En la litografía
se escondía : virTüalmenté _ éTperiódico ilustrado y en la fotografía el cine sonoro. La reproducción técnica del sonido fue empresa acometida a finales del siglo pasado.
Todos estos esfuerzos convergentes hicieron previsible
una situación que Paul Valéry caracteriza con la frase
siguiente: «Igual que el agua, el gas y la corriente eléctrica vienen a nuestras casas, para servirnos, desde lejos
y por medio de una manipulación casi imperceptible, así
estamos también provistos de imágenes y de series de
sonidos que acuden a un pequeño toque, casi a un signo,
y que del mismo modo nos abandonan»'. Hacia 1900 la
reproducción técnica había alcanzado un standard en el
que no sólo comenzaba a convertir en tema propio la totalidad de las obras de arte heredadas (sometiendo además su función a modificaciones hondísimas), sino que
también conquistaba un puesto específico entre los procedimientos artísticos. Nada resulta más instructivo para
el estudio de ese standard que referir dos manifestaciones distintas, la reproducción de la obra artística y el
cine^ al arte en su figura tradicional.

2

Incluso en la reproducción mejor acabada falta algo:
el aquí y ahora de la obra de arte, su existencia irrepetible en el lugar en que se encuentra! En dicha existencia
singular, y en ninguna otra cosa, se realizóJajustoria'a'
la que ha estado somanta e j i j ^ j y j j ^ J ^ ^
También cuentan las alteraciones que haya padecido en
su estructura física a lo largo del tiempo, así como sus
eventuales cambios de propietario 2 No podemos seguir

1

'

2

PAUL VALÉRY, Piéces sur l'art, París, 1934.
Claro que la historia de una obra de arte abarca más ele-

/

20

el rastro de las primeras más que por medio de análisis
físicos o químicos impracticables sobre una reproducción; el de los segundos es tema de una tradición cuya
búsqueda ha de partir del lugar de origen de la obra.
(_E1 aquí y ahora del original constituye el concepto de
su autenticidad Los análisis químicos de la pátina de un
bronce favorecerán que se fije si es auténtico; correspondientemente, la comprobación de que un determinado
manuscrito medieval procede de un archivo del siglo XV
favorecerá la fijación de su autenticidad. El ámbito entero de la autenticidad_se sustrae a la reproductibiliSad
técnica—y desde luego que no sólo a la técnica— 3 . Cara
a la reproducción manual, que normalmente es catalogada como falsificación, lo auténtico conserva su autoridad
plena, mientras que no ocurre lo mismo cara a la reproducción técnica. La razón es doble. En primer lugar, la
reproducción técnica se acredita como más independiente que la manual respecto del original. En la fotografía;
por ejemplo, pueden resaltar aspectos del original accesibles únicamente a una lente manejada a propio antojo
con el fin de seleccionar diversos puntos de vista, inaccesibles en cambio para el ojo humano. O con ayuda de
ciertos procedimientos, como la ampliación o el retarda^
dor, retendrá imágenes que se le escapan sin más a la
óptica humana. Además, puede poner la copia del original en situaciones inasequibles para éste. Sobre todo le
mentos: la historia de Mona Lisa, por ejemplo, abarca el tipo
y número de copias que se han hecho de ella en los siglos diecisiete, dieciocho y diecinueve.
3
Precisamente porque la autenticidad no es susceptible de
que se la reproduzca, determinados procedimientos reproductivos, técnicos por cierto, han permitido al infiltrarse intensamente,
diferenciar y graduar la autenticidad misma. Elaborar esas distinciones ha sido una función importante del comercio del arte.
Podríamos decir que el invento de la xilografía atacó en su raíz
la cualidad de lo auténtico, antes desde luego de que hubiese
desarrollado su último esplendor. La imagen de una Virgen medieval no era auténtica en el tiempo en que fue hecha; lo fue
siendo en el curso de los siglos siguientes, y más exhuberantemente que nunca en el siglo pasado.

21

posibilita salir al encuentro de su destinatario, ya sea en
forma de fotografía o en la de disco gramofónico. La catedral deja su emplazamiento para encontrar acogida en
el estudio de un aficionado al arte; la obra coral, que fue
ejecutada en una sala o al aire libre, puede escucharse
en una habitación.
Las circunstancias en que se ponga al producto de
la reproducción de una obra de arte, quizás dejen intacta la consistencia de ésta, pero en cualquier caso deprecian su aquí y ahora. Aunque en modo alguno valga ésto
sólo para una obra artística, sino que parejamente vale
también, por ejemplo, para un paisaje que en el cine
transcurre ante el espectador. Sin embargo, el proceso
aqueja en el objeto de arte una médula sensibilísima que
ningún objeto natural posee en grado tan vulnerable. Se
trata de su autenticidad £La autenticidad de una cosa es
la cifra de todo lo que desde el origen puede transmitirse en ella desde su duración material hasta su testificación históricaT^Como esta última se funda en la primera,
que a su vez se le escapa al hombre en la reproducción,
por eso se tambalea en ésta la testificación histórica de
la cosa. Claro que sólo ella; pero lo que se tambalea de
tal suerte es su propia a u t o r i d a d ^
Resumiendo todas estas deficiencias en el concepto
de aura, podremos decir: en la época de la reproducción
técnica de Ja obra de arte Jo que se atrofia es el aura
de ésta. El proceso es sintomático; su significación señala
por encima del ámbito artístico. Conforme a una formulación general: lajécnica reproductiva desvincula lo reproducido del ámbito de la tradición. Al multiplicar las
reproducciones pone su presencia masiva en el lugar de
una presencia irrepetible. Y confiere actualidad a lo re4
La representación de Fausto más provinciana y pobretona
aventajará siempre a una película sobre la misma obra, porque
en cualquier caso le hace la competencia ideal al estreno en
Weimar. Toda la sustancia tradicional que nos recuerdan las
candilejas (que en Mefistófeles se esconde Johann Heinrich
Merck, un amigo de juventud de Goethe, y otras cosas parecidas), resulta inútil en la pantalla.

22

producido al permitirle salir, desde su situación respectiva, al encuentro de cada destinatario. Ambos procesos
conducen a una fuerte conmoción de lo transmitido, a
una conmoción de la tradición, que es el reverso de la
actual crisis y de la renovación de la humanidad. Están
además en estrecha relación con los movimientos de
masas de nuestros días. Su agente más poderoso es el
cine. La importancia social de éste no es imaginable incluso en su forma más positiva, y precisamente en ella,
sin este otro lado suyo destructivo, catártico: la liquidación del valor de la tradición en la herencia cultural.
Este fenómeno es sobre todo perceptible en las grandes
películas históricas. Es éste un terreno en el que constantemente toma posiciones. Y cuando Abel Gance proclamó
con entusiasmo en 1927: «Shakespeare, Rembrandt,
Beethoven, harán cine... Todas las leyendas, toda la mitología y todos los mitos, todos los fundadores de religiones y todas las religiones incluso... esperan su resurrección luminosa, y los héroes se apelotonan, para entrar,
ante nuestras puertas» 5 , nos estaba invitando, sin saberlo, a una liquidación general.

3
Dentro de grandes espacios históricos de tiempo se
modifican, junto con toda la existencia de las colectividades humanas, el modo y manera de su percepción, sensorial. Dichos modo y manera en que esa percepción se organiza, el medio en el que acontecen, están condicionados
no sólo natural, sino también históricamente. El tiempo
de la Invasión de los Bárbaros, en el cual surgieron la industria artística del Bajo Imperio y el Génesis de Vierta*
s

ABEL GANCE, «Le temps de l'image est venu» (L'art cinématographique, II), París, 1927.
* El Wiener Génesis es una glosa poética del Génesis bíblico,
compuesta por un monje austríaco hacia 1070 (N. del T.).

23

trajo consigo además de un arte distinto del antiguo una
percepción también distinta. Los eruditos de la escuela
vienesa, Riegel y Wickhoff, hostiles al peso de la tradición
clásica que sepultó aquel arte, son los primeros en dar
con la ocurrencia de sacar de él conclusiones acerca de
la organización de la percepción en el tiempo en que tuvo
vigencia. Por sobresalientes que fueran sus conocimientos, su limitación estuvo en que nuestros investigadores
se contentaron con indicar la signatura formal propia de
la percepción en la época del Bajo Imperio. No intentaron
(quizás ni siquiera podían esperarlo) poner de manifiesto
las transformaciones sociales que hallaron expresión en
esos cambios de la sensibilidad. En la actualidad son
más favorables las condiciones para un atisbo correspondiente. Y si las modificaciones en el medio de la percepción son susceptibles de que nosotros, sus coetáneos, las
entendamos como desmoronamiento del aura, sí que podremos poner de bulto sus condicionamientos sociales.
Conviene ilustrar el concepto de aura, que más arriba hemos propuesto para temas históricos, en el concepto de un aura de ^objetos naturales-! Definiremos esta
última como la manifestación irrepetible dgjanajejanía
(por cercana que pueda estar). Descansar en un atardecer de verano y seguir con la mirada una cordillera en el
horizonte o una rama que arroja su sombra sobre el que
reposa, eso es aspirar el aura de esas montañas, de esa
rama. De la mano de esta descripción es fácil hacer una
cala en los condicionamientos sociales del actual desmoronamiento del aura. Estriba éste en dos circunstancias que a su vez dependen de la importancia creciente
de las masas en la vida de hoy. A saber: acercar espacial y humanamente las cosas es una aspiración" de las
masas actuales" tan apasionada como su tendencia a
6

Acercar las cosas humanamente a las masas, puede significar que se hace caso omiso de su función social. Nada garantiza que un retratista actual, al pintar a un cirujano célebre
desayunando en el círculo familiar, acierte su función social con
mayor precisión que un pintor del siglo dieciséis que expone al
público los médicos de su tiempo representativamente, tal y

24

superar la singularidad de cada dato acogiendo su reproducción. Cada día cobra una vigencia más irrecusable la necesidad de adueñarse de los_^bjelos_jej]j^más j
próxima de las cercaniasTen la. ímafflPx^qíg¿_bign_g23_Ia
copia7elírÍa~reproducción~ Y la ^^ro3ucciór)7taly como
la aprestan íospenódicós ilustrallos"3^Í0snoticiarios > se
distingue inequívocamente de lamnage^. En ésta, la sin^
gularldad^Ja-perduración están jnfeicadas una enjytra
de manera tan estrecha como lo estáfl-£n_aguéllaja^ugacidad v la posible repetición.^ Quitarle su envoltura a;
cada objeto, triturar su aura, es la signatura de una percepción cuyo sentido para laJgual~£ii-jeL.rnimdoJb.a "cre^
cido tanto que incluso, por medio de la reproducción,
le gana terreno a lonrepetible. Se denota así en el ámbito pIa^ticbnEnqüe~^rT^r^mbito de la teoría advertimos
como un aumento de la importancia de la estadística. La
orientación de la realidad a las masas y de éstas a la
realidad es un proceso de alcance ilimitado tanto para
el pensamiento como para la contemplación.

4

La fanjcidac? de la obra de arte se identifica con su
ensamblamiento en el contexto de la tradición. Esa tradición es desde luego algo muy vivo, algo extraordinariamente cambiante. Una estatua antigua de Venus, por
ejemplo, estaba en un contexto tradicional entre los griegos, que hacían de ella objeto de culto, y en otro entre
los clérigos medievales que la miraban como un ídolo
maléfico. Pero a unos y a otros se les enfrentaba de igual
modo su unicidad, o dicho con otro término: su aura.
La índole original del ensamblamiento de la obra de arte
como lo hace, por ejemplo, Rembrandt en La lección de anatomía.
^ r

en el contexto de la tradición encontró su expresión en
el culto. Las obras artísticas más antiguas sabemos que
surgieron al servicio de un ritual primero mágico, luego
religioso. Es de decisiva importancia que el modo aurá^tico de existencia de la obra dejarte jamás se desligue
de la función r i t u a l ^ Con otras palabras: el valoFuñico
de~la auténtica obra artística se funda en el ritual en el
que tuvo su primer y original valor útil. Dicha fundamentación estará todo lo mediada que se quiera, peroincluso en las formas más profanas del servicio a la belleza resulta perceptible en cuanto ritual secularizado 8. Este
servicio profano, que se formó en el Renacimiento para
.seguir vigente por tres siglos, ha permitido, al transcurrir ese plazo y a la primera conmoción grave que le alcanzara, reconocer con toda claridad tales fundamentos.
Al irrumpir el primer medio de reproducción de veras
revolucionario, a saber la fotografía (a un tiempo con el
despunte del socialismo), el arte sintió la proximidad de
la crisis (que después de otros cien años resulta innegable), y reaccionó con la teoría de «l'art pour l'art», esto
es, con una teología del arte. De ella procedió ulteriormente ni más ni menos que una teología negativa en
figura de la idea de un arte «puro» que rechaza no sólo
cualquier función social, sino además toda determinación por medio de un contenido objetual. (En la poesía,
Mallarmé ha sido el primero en alcanzar esa posición.)
Hacer justicia a esta serie de hechos resulta indispen7

La definición del aura como «la manifestación irrepetible
de una lejanía (por cercana que pueda estar)» no representa
otra cosa que la formulación del valor cultual de la obra artística en categorías de percepción espacial-temporal. Lejanía es
lo contrario que cercanía. Lo esencialmente lejano es lo inaproximable. Y serlo es de hecho una cualidad capital de la imagen
cultual. Por propia naturaleza sigue siendo «lejanía, por cercana que pueda estar». Una vez aparecida conserva su lejanía,
a la cual en nada perjudica la cercanía que pueda lograrse de
su materia.
8
A medida que se seculariza el valor cultual de la imagen,
nos representaremos con mayor indeterminación el sustrato de
su singularidad. La singularidad empírica del artista o de su

26

sable p a r a una cavilación que tiene que habérselas con
la obra de arte en la época de su reproducción técnica.
Esos hechos p r e p a r a n un atisbo decisivo en nuestro tema:
p o r p r i m e r a vez en la historia universal, la reproductibilidad técnica emancipa a la obra artística de su existencia parasitaria en u n ritual. La obra de_arte reproducida
se convierte, en medida siempre creciente, en reproducción de u n a obra artística.dispuesta p a r a ser reprodjci,da 9 . D e ^ i a ^ I a c á f o t o g r á f i c a , por ejemplo,' son~posibles
muchas copias; preguntarse por la copia auténtica no
tendría sentido alguno. Pero en el mismo instante en que
la n o r m a de la autenticidad fracasa en la producción aractividad artística desplaza cada vez más en la mente del espectador a la singularidad de las manifestaciones que imperan en
la imagen cultual. Claro que nunca enteramente; el concepto de
autenticidad jamás deja de tender a ser más que una adjudicación de origen. (Lo cual se pone especialmente en claro en el
coleccionista, que siempre tiene algo de adorador de fetiches y
que por la posesión de la obra de arte participa de su virtud
cultual.) Pero a pesar de todo la función del concepto de lo auténtico sigue siendo terminante en la teoría del arte: con la secularización de este último la autenticidad (en el sentido de adjudicación de origen) sustituye al valor cultual.
' En las obras cinematográficas la posibilidad de reproducción técnica del producto no es, como por ejemplo en las obras
literarias o pictóricas, una condición extrínseca de su difusión
masiva. Ya que se funda de manera inmediata en la técnica
de su producción. Esta no sólo posibilita directamente la difusión
masiva de las películas, sino que más bien la impone ni más
ni menos que por la fuerza. Y la impone porque la producción
de una película es tan cara que un particular que, pongamos
por caso podría permitirse el lujo de un cuadro, no podrá en
cambio permitirse el de una película. En 1927 se calculó que
una película de largo metraje, para ser rentable, tenía que conseguir un público de nueve millones de personas. Bien es verdad que el cine sonoro trajo consigo por de pronto un movimiento de retrocesión. Su público quedaba limitado por las
fronteras lingüísticas, lo cual ocurría al mismo tiempo que el
fascismo subrayaba los intereses nacionales. Pero más importante que registrar este retroceso, atenuado por lo demás con
los doblajes, será que nos percatemos de su conexión con el
fascismo. Ambos fenómenos son simultáneos y se apoyan en la
crisis económica. Las mismas perturbaciones que, en una visión
de conjunto, llevaron a intentar mantener con pública violencia

27

tística, se trastorna la función íntegra del arte. En lugar
de su fundamentación en un ritual aparece su fundamentación en una praxis distinta, a saber en la política.

5

La recepción de las obras de arte sucede bajo diversos acentos entre los cuales hay dos que destacan por su
polaridad. Uñó~Hé "esos acentos reside en el valor cultual,
ej__gJro„,gn_eI valor exhibitivo de la obra artística 10 . La
producción artística comienza con hechuras que están al
servicio del culto. Presumimos que es más importante
las condiciones existentes de la propiedad, han llevado también
a un capital cinematográfico, amenazado por la crisis, a acelerar
los preparativos del cine sonoro. La introducción de películas
habladas causó en seguida un alivio temporal. Y no sólo porque
inducía de nuevo a las masas a ir al cine, sino además porque
conseguía la solidaridad de capitales nuevos procedentes de la
industria eléctrica.
Considerado desde fuera, el cine sonoro ha favorecido intereses nacionales; pero considerado desde dentro, ha internacionalizado más que antes la producción cinematográfica.
10
Esta polaridad no cobrará jamás su derecho en el idealismo, cuyo concepto de belleza incluye a ésta por principio
como indivisa (y por consiguiente la excluye en tanto que dividida). Con todo se anuncia en Hegel tan claramente como resulta imaginable en las barreras del idealismo. En las Lecciones
de Filosofía de la Historia se dice así: «Imágenes teníamos desde hace largo tiempo: la piedad necesitó de ellas muy temprano para sus devociones, pero no precisaba de imágenes bellas, que en este caso eran incluso perturbadoras. En una imagen bella hay también un elemento exterior presente, pero en
tanto que es bella su espíritu habla al hombre; y en la devoción
es esencial la relación para con una cosa, ya que se trata no
más que de un enmohecimiento del alma... El arte bello ha
surgido en la Iglesia... aunque... el arte proceda del principio
del arte» (GEORG FRIEDRICH WILHELM HEGEL, Werke, Berlín y
Leipzig, 1832, vol. IX, pág. 414). Un pasaje en las Lecciones sobre
Estética indica que Hegel rastreó aquí un problema: «Estamos
28

que dichas hechuras estén presentes y menos que sean
vistas. El alce que el hombre de la Edad de Piedra dibuja en las paredes de su cueva es un instrumento mágico. Claro que lo exhibe ante sus congéneres; pero está
sobre todo destinado a los espíritus. Hoy nos parece que
el valor cultual empuja a la obra de arte a mantenerse
oculta: ciertas estatuas de dioses sólo son accesibles a
los sacerdotes en la «celia». Ciertas imágenes de Vírgenes
permanecen casi todo el año encubiertas, y determinadas
esculturas de catedrales medievales no son visibles para
el espectador que pisa el santo suelo. A medida que las
ejercitaciones artísticas se emancipan del regazo ritual,
aumentan las ocasiones de exhibición de sus productos.
La capacidad exhibitiva de un retrato de medio cuerpo,
que puede enviarse de aquí para allá, es mayor que la de
la estatua de un dios, cuyo puesto fijo es el interior del
templo. Y si quizás la capacidad exhibitiva de una misa
por encima de rendir un culto divino a las obras de arte, de
poder adorarlas; la impresión que nos hacen es de índole más
circunspecta, y lo que provocan en nosotros necesita de una piedra de toque superior» (GEORG FRIEDRICH WILHELM HEGEL, l. c,
vol. X, pág. 14).
El tránsito del primer modo de recepción artística al segundo
determina. el decurso histórico de la recepción artística en general. No obstante podríamos poner de bulto una cierta oscilación entre ambos modos receptivos por principio para cada
obra de arte. Así, por ejemplo, para la Virgen Sixtina. Desde la
investigación de Hubert Grimme sabemos que originalmente fue
pintada para fines de exposición. Para sus trabajos le impulsó
a Grimme la siguiente pregunta: ¿por qué en el primer plano
del cuadro ese portante de madera sobre el que se apoyan los
dos angelotes? ¿Como pudo un Rafael, siguió preguntándose
Grimme, adornar el cielo con un par de portantes? De la investigación resultó que la Virgen Sixtina había sido encargada
con motivo de la capilla ardiente pública del Papa Sixto. Dicha
ceremonia pontificia tenía lugar en una capilla lateral de la
basílica de San Pedro. En el fondo a modo de nicho de esa capilla se instaló, apoyado sobre el féretro, el cuadro de Rafael.
Lo que Rafael representa en él es la Virgen acercándose entre
nubes al féretro papal desde el fondo del nicho delimitado por
dos portantes verdes. El sobresaliente valor exhibitivo del cuadro de Rafael encontró su utilización en los funerales del Papa
29

no es de por sí menor que la de una sinfonía, la sinfonía
ha surgido en un tiempo en el que su exhibición prometía ser mayor que la de una misa.
_
Con los diversos métodos de su reproducción técnica
han crecido en grado tan fuerte las pj^sihilidades^de exhibición de la obra de arte, que el corrimiento cuantitativo~^ñtre~sus dos polos se torna, como en los tiempos
primitivos, en una modificación cualitativa de su naturaleza. A saber, en los tiempos primitivos, y a causa de la
preponderancia absoluta de su valor cultual, fue en primera línea un instrumento de magia que sólo más tarde
se reconoció en cierto modo como obra artística; y hoy
la preponderancia absoluta de su valor exhibitivo hace
de ella una hechura con funciones por entero nuevas entre las cuales la artística —la que nos es consciente— se
destaca como la que más tarde tal vez se reconozca en
cuanto accesoria". Por lo menos es seguro que actualmente la fotografía y además el cine proporcionan las
aplicaciones más útiles de ese conocimiento.
Sixto. Poco tiempo después vino a parar el cuadro al altar mayor
de un monasterio de Piacenza. La razón de este exilio está en
el ritual romano que prohibe ofrecer al culto en un altar mayor
imágenes que hayan sido expuestas en celebraciones funerarias.
Hasta cierto punto dicha prescripción depreciaba la obra de
Rafael. Para conseguir sin embargo un precio adecuado, se decidió la Curia a tolerar tácitamente el cuadro en un altar mayor. Pero para evitar el escándalo lo envió a la comunidad de
una ciudad de provincia apartada.
11
Brecht dispone reflexiones análogas a otro nivel: «Cuando
una obra artística se transforma en mercancía, el concepto de
obra de arte no resulta ya sostenible en cuanto a la cosa que
surge. Tenemos entonces cuidadosa y prudentemente, pero sin
ningún miedo, que dejar de lado dicho concepto, si es que no
queremos liquidar esa cosa. Hay que atravesar esa fase y sin
reticencias. No se trata de una desviación gratuita del camino
recto, sino que lo que en este caso ocurre con la cosa la modifica fundamentalmente y borra su pasado hasta tal punto que, si
se aceptase de nuevo el antiguo concepto (y se le aceptará,
¿por qué no?), ya no provocaría ningún recuerdo de aquella
cosa que antaño designara» (BERTOLT BRECHT, Der Dreigroschenprozess).

3©

6

En la fotografía, el valor exhibitivo comienza a reprimir en toda la línea al valor cultual. Pero éste no cede
sin resistencia. Ocupa una última trinchera que es el rostro humano. En modo alguno es casual que en los albores
de la fotografía el retrato ocupe un puesto central. El
valor cultual de la imagen tiene su último refugio en el
culto al recuerdo de los seres queridos, lejanos o desaparecidos. En las primeras fotografías vibra por vez postrera el aura en la expresión fugaz de una cara humana.
Y esto es I ó ^ u e constituye su beu^zlTñéTaTñooTicT^lrP""
comparable. Pero cuando el hombre se retira de la fotografía, se opone entonces, superándolo, el valor exhibitivo al cultual. Atget es sumamente importante por haber
localizado este proceso al retener hacia 1900 las calles de
París en aspectos vacíos de gente. Con mucha razón se
ha dicho de él que las fotografió como si fuesen el lugar
del crimen. Porque también éste está vacío y se le fotografía a causa de los indicios. Con Atget comienzan las
placas fotográficas a convertirse en pruebas en el proceso histórico. Y así es como se forma su secreta significación histórica. Exigen una recepción en un sentido determinado. La contemplación de vuelos propios no resulta muy adecuada. Puesto que inquietan hasta tal punto
a quien las mira, que para ir hacia ellas siente tener que
buscar un determinado camino. Simultáneamente los periódicos ilustrados empiezan a presentarle señales indicadoras. Acertadas o erróneas, da lo mismo. Por primera
vez son en esos periódicos obligados los pies de las fotografías. Y claro está que éstos tienen un carácter muy
distinto al del título de un cuadro. El que mira una revista ilustrada recibe de los pies de sus imágenes unas
31

directivas que en el cine se harán más precisas e imperiosas, ya que la comprensión de cada imagen aparece
prescrita por la serie de todas las imágenes precedentes.

1

Aberrante y enmarañada se nos antoja hoy la disputa
sin cuartel que al correr el siglo diecinueve mantuvieron
la fotografía y la pintura en cuanto al valor artístico de
sus productos. Pero no pondremos en cuestión su importancia, sino que más bien podríamos subrayarla. De hecho esa disputa era expresión de un trastorno en la historia universal del que ninguno de los dos contendientes
era consciente.' La época de su reproductibilidad técnica
desligó al arte de su fundamento cultual: y el halo de su
autonomía se extinguió para siempre. Se produjo entonces una modificación en la función artística que cayó fuera del campo de visión del siglo. E incluso se le ha escapado durante tiempo al siglo veinte, que es el que ha
vivido el desarrollo del cine.
En vano se aplicó por de pronto mucha agudeza para
decidir si la fotografía es un arte (sin plantearse la cuestión previa sobre si la invención de la primera no modificaba por entero el carácter del segundo). Enseguida se
encargaron los teóricos del cine de hacer el correspondiente y precipitado planteamiento. Pero las dificultades
que la fotografía deparó a la estética tradicional fueron
juego de niños comparadas con las que aguardaban a
esta última en el cine. De ahí esa ciega vehemencia que
caracteriza los comienzos de la teoría cinematográfica.
Abel Gance, por ejemplo, compara el cine con los jeroglíficos: «Henos aquí, en consecuencia de un prodigioso
retroceso, otra vez en el nivel de expresión de los egipcios... El lenguaje de las imágenes no está todavía a
32

punto, porque nosotros no estamos aún hechos para
ellas. No hay por ahora suficiente respeto, suficiente culto por lo que expresan» 12. También Séverin - Mars
escribe: «¿Qué otro arte tuvo un sueño más altivo... a
la vez más poético y más real? Considerado desde este
punto de vista representaría el cine un medio incomparable de expresión, y en su atmósfera debieran moverse únicamente personas del más noble pensamiento y en los
momentos más perfectos y misteriosos de su carrera» u .
Por su parte, Alexandre Arnoux concluye una fantasía sobre el cine mudo con tamaña pregunta: «Todos los términos audaces que acabamos de emplear, ¿no definen al
fin y al cabo la oración?» 14. Resulta muy instructivo ver
cómo, obligados por su empeño en ensamblar el cine en
el arte, esos teóricos ponen en su interpretación, y por
cierto sin reparo de ningún tipo, elementos cultuales. Y
sin embargo, cuando se publicaron estas especulaciones
ya existían obras como La opinión pública y La quimera
del oro. Lo cual no impide a Abel Gance aducir la comparación con los jeroglíficos y a Séverin-Mars hablar del
cine como podría hablarse de las pinturas de Fra Angélico. Es significativo que autores especialmente reaccionarios busquen hoy la importancia del cine en la misma
dirección, si no en lo sacral, sí desde luego en lo sobrenatural. Con motivo de la realización de Reinhardt del
Sueño de una noche de verano afirma Werfel que no cabe
duda de que la copia estéril del mundo exterior con sus
calles, sus interiores, sus estaciones, sus restaurantes,
sus autos y sus playas es lo que hasta ahora ha obstruido
el camino para que el cine ascienda al reino del arte. «El
cine no ha captado todavía su verdadero sentido, sus
posibilidades reales... Estas consisten en su capacidad
singularísima para expresar, con medios naturales y con
" ABEL GANCE, /. c, págs. 100-101.
13

Séverin-Mars, cit. p o r ABEL GANCE, 1. c, pág. 100.

" ALEXANDRE ARNOUX, Cinema,

París, 1929, pág. 28.

33

una fuerza de convicción incomparable, lo quimérico, lo
maravilloso, lo sobrenatural» I5.

8

En definitiva, el actor de teatro presenta él mismo en
persona al público su ejecución artística; por el contrario, la del actor de cine es presentada por medio de todo
un mecanismo. Esto último tiene dos consecuencias. El
mecanismo que pone ante eí público la ejecución del
actor cinematográfico no está atenido a respetarla en
su totalidad. Bajo la guía del cámara va tomando posiciones a su respecto. Esta serie de posiciones, que el montador compone con el material que se le entrega, constituye la película montada por completo. La cual abarca
un cierto número de momentos dinámicos que en cuanto tales tienen que serle conocidos a la cámara (para
no hablar de enfoques especiales o de grandes planos).
La actuación del actor está sometida por tanto a una
"serie de tests ó p t i c o s ^ ésta es la primera consecuencia
de que su trabajo se exhiba por medio de un mecanismo. La segunda consecuencia estriba en que este actor,
puesto qú¥TEo^s _ eTmismo quien presenta a los espectadores su ejecución, se ve mermado en la posibilidad, reservada al actor de teatro, de acomodar su actuación al
público durante la función. El espectador se encuentra
pues en la actitud del experto que emite un dictamen sin
que para ello le estorbe ningún tipo de contacto personal
con el artista. Se compenetra con el actor sólo en tanto
1S

FRANZ WERFEL, «Ein Sommernachtstraum. Ein Film nach
Shakespeare von Reinhardt», Neues Wiener Journal, 15 de noviembre de 1935.

34

que se compenetra con el aparato. Adopta su actitud:
hace test 16 . Y no es ésta una actitud a la que puedan
someterse valores cultuales.

9

Al cine le importa menos que el actor represente ante
el público un personaje; lo que le importa es que se
represente a sí mismo ante el mecanismo. Pirandello
ha sido uno de los primeros en dar con este cambio
que los tests imponen al actor. Las advertencias que
hace a este respecto en su novela Se rueda quedan perjudicadas, pero sólo un poco, al limitarse a destacar
el lado negativo del asunto. Menos aún les daña que se
refieran únicamente al cine mudo. Puesto que el cine
sonoro no ha introducido en este orden ninguna alteración fundamental. Sigue siendo decisivo representar para
un aparato —o en el caso del cine sonoro para dos.
«El actor de cine», escribe Pirandello, «se siente como en
el exilio. Exiliado no sólo de la escena, sino de su propia
persona. Con un oscuro malestar percibe el vacío inexplicable debido a que su cuerpease convierte en un^ síntoma
16

«El cine... da (o podría ^ár) informaciones muy útiles por
su detalle sobre acciones humanas... No hay motivaciones de carácter, la vida interior de las personas jamás es causa primordial y raras veces resultado capital de la acción» (BERTOLT BRECHT,
l. c). Laj ampliación por medio del mecanismo cinematográfico
del campo sometido a los tests corresponde a la extraordinaria
ampliación que de ese campo «testable» traen consigo para el
individuo las circunstancias económicas. Constantemente está
aumentando la importancia de las pruebas de aptitud profesional. En ellas lo que se ventila son consecuencias de la ejecución
del individuo. El rodaje de una película y las pruebas de aptitud
profesional se desarrollan ante un gremio de especialistas. El
director en el estudio de cine ocupa exactamente el puesto del
director experimental en las pruebas a que nos referimos.

35

de deficiencia que se volatiliza y al que se expoliable su
realidad, de su vida, de su "voz y de los ruidos que produclTáTmoverse, transformándose entonces en una imaen muda que tiembla en la pantalla un instante" y que
e^paTél^^eTisegutda a^extementer. . L a pequeña maquina
representa ante el público su sombra, pero él tiene que
contentarse con representar ante la máquina» ".; He aquí
un estado de cosas que podríamos caracterizar así: por
primera vez —y esto es obra del cine— lleg£_el-iiombr_e
a ^la^tuación_de tener que actuar con toda su persona
viva, pero renunciando - á~sü aüFa. iForque~éT aura está ligada^_^u_jgjoí_j[_^;hora^X)el aura no hay copia. La que
rodea a Macbeth en escena es inseparable" de laque, para
un público vivo, ronda al actor que le representa. Lo
peculiar del rodaje en el estudio cinematográfico consiste en que ios aparatos ocupan el lugar del público. Y así
tiene que desaparecer el aura del actor y con ella la del
personaje que representa.
No es sorprendente que en su análisis del cine un
dramaturgo como Pirandello toque instintivamente el
fondo de la crisis que vemos sobrecoge al teatro. La escena teatral es de hecho la contrapartida más resuelta
respecto de una obra de arte captada íntegramente por
la reproducción técnica y que incluso, como el cine, pro- i
cede de ella. Así lo confirma toda consideración mínimamente intrínseca. Espectadores peritos, como Arnheim
en 1932, se han percatado hace tiempo de que en el cine
«casi siempre se logran los mayores efectos si se actúa
lo menos posible... *E1 último progreso consiste en que
se trata al actor como a un accesorio escogido característicamente... al cual se coloca en un lugar adecuado» 18.

f

17
LUIGI PIRANDELLO, On tourne, cit. por LÉON PIERRE-QUINT,
«Signification du cinema» (L'art cinématographique, II, París,
1927, págs. 14-15).
n
RUDOLF ARNHEIM, Film ais Kunst, Berlín, 1932. En este contexto cobran un interés redoblado determinadas particularidades,
aparentemente marginales, que distancian al director de cine
de las prácticas de la escena teatral. Así la tentativa de hacer
que los actores representen su papel sin maquillaje, como hizo

36

Pero hay otra cosa que tiene con esto estrecha conexión.
El artista que actúa en escena se transpone en un papel.
Lo cual se le niega frecuentemente al actor de cine. Su
ejecución no es unitaria, sino que se compone de muchas
ejecuciones. J u n t o a miramientos ocasionales p o r el precio del alquiler de los estudios, p o r la disponibilidad de
los colegas, por el decorado, etc., son necesidades elementales de la maquinaria las que desmenuzan la actuación del artista en una serie de episodios montables. Se
trata sobre todo de la iluminación, cuya instalación obliga a realizar en muchas tomas, distribuidas a veces en el
estudio en horas diversas, la exposición de u n proceso
que en la pantalla aparece como u n veloz decurso unitario. Para no hablar de montajes mucho más palpables.
El salto desde u n a ventana puede rodarse en forma de
salto desde el andamiaje en los estudios y, si se da el
caso, la fuga subsiguiente se t o m a r á semanas más t a r d e
Dreyer, entre nosotros, en su Juana de Arco. Empleó meses sólo
en encontrar los cuarenta actores que componen el jurado contra la hereje. Esta búsqueda se asemejaba a la de accesorios de
difícil procura. Dreyer aplicó gran esfuerzo en evitar parecidos en edad, estatura, fisionomía, etc. Si el actor se convierte
en accesorio, no es raro que el accesorio desempeñe por su lado
la función de actor. En cualquier caso no es insólito que llegue
el cine a confiar un papel al accesorio. Y en lugar de destacar
ejemplos a capricho en cantidad infinita, nos atendremos a uno
cuya fuerza de prueba es especial. Un reloj en marcha no es
en escena más que una perturbación. No puede haber en el teatro lugar para su papel, que es el de medir el tiempo. Incluso
en una obra naturalista chocaría el tiempo astronómico con el
escénico. Así las cosas, resulta sumamente característico que en
ocasiones el cine utilice la medida del tiempo de un reloj. Puede
que en ello se perciba mejor que en muchos otros rasgos cómo
cada accesorio adopta a veces en él funciones decisivas. Desde
aquí no hay más que un paso hasta la afirmación de Pudowkin:
«la actuación del artista ligada a un objeto, construida sobre él,
será... siempre uno de los métodos más vigorosos de la figuración cinematográfica» (W. PUDOWKIN, Filmregie und Filmmanuskript, Berlín, 1928, pág. 126). El cine es por lo tanto el primer
medio artístico que está en situación de mostrar cómo la materia colabora con el hombre. Es decir, que puede ser un excelente instrumento de discurso materialista^

37

en exteriores. Por lo demás es fácil construir casos muchísimo más paradójicos. Tras una llamada a la puerta
se exige del actor que se estremezca. Quizás ese sobresalto no ha salido tal y como se desea. El director puede
entonces recurrir a la estratagema siguiente: cuando el
actor se encuentre ocasionalmente otra vez en el estudio
le disparan, sin que él lo sepa, un tiro por la espalda.
Se filma su susto en ese instante y se monta luego en
la película. Nada pone más drásticamente de bulto que
el arte se ha escapado del reino del halo de lo bello, único
en el que se pensó por largo tiempo que podía alcanzar
florecimiento.

10

El extrañamiento del actor frente al mecanismo cinematográfico es de todas todas, tal y como lo describe
Pirandello, de la misma índole que el que siente el hombre ante su aparición en el espejo. Pero es que ahora esa
imagen del espejo puede despegarse de él, se ha hecho
transportable. ¿Y adonde se la transporta? Ante el público 19. Ni un solo instante abandona al actor de cine la
" También en la política es perceptible la modificación que
constatamos trae consigo la técnica reproductiva en el modo de
exposición. La crisis actual de las democracias burguesas implica una crisis de las condiciones determinantes de cómo deben
presentarse los gobernantes. Las democracias presentan a éstos
inmediatamente, en persona, y además ante representantes. ¡El
Parlamento es su público! Con las innovaciones en los mecanismos de transmisión, que permiten que el orador sea escuchado
durante su discurso por un número ilimitado de auditores y
que poco después sea visto por un número también ilimitado
de espectadores, se convierte en primordial la presentación del
hombre político ante esos aparatos. Los Parlamentos quedan
desiertos, así como los teatros. La radio y el cine no sólo modi-

38

consciencia de ello. Mientras está frente a la cámara sabe
que en última instancia es con el público con quien tiene
que habérselas: con el público de consumidores que forman el mercado. Este mercado, al que va no sólo con
su fuerza de trabajo, sino con su piel, con sus entrañas
todas, le resulta, en el mismo instante en que determina
su actuación para él, tan poco asible como lo es para cualquier artículo que se hace en una fábrica. ¿No tendrá
parte esta circunstancia en la congoja, en esa angustia
que, según Pirandello, sobrecoge al actor ante el aparato?
A la atrofia del aura el cine responde con una construci ción artificial de la personálity fuera de los estudios; el
j culto a las «estrellas», fomentado por el capital cinematográfico, conserva aquella magia de la personalidad, pero
reducida, desde hace ya tiempo, a la magia averiada de
1
su carácter de mercancía. Mientras sea el capital quien
de en él el tono, no podrá adjudicársele al cine actual
otro mérito revolucionario que el de apoyar una crítica
revolucionaria de las concepciones que hemos heredado
sobre el arte. Claro que no discutimos que en ciertos casos pueda hoy el cine apoyar además una crítica revolucionaria de las condiciones sociales, incluso del orden de
la propiedad. Pero no es éste el centro de gravedad de
la presente investigación (ni lo es tampoco de la producción cinematográfica de Europa occidental).
Es propio de la técnica del cine, igual que de la del
deporte, que cada quisque asista a sus exhibiciones como
un medio especialista. Bastaría con haber escuchado discutir los resultados de una carrera ciclista a un grupo de
repartidores de periódicos, recostados sobre sus biciclefican la función del actor profesional, sino que cambian también
la de quienes, como los gobernantes, se presentan ante sus mecanismos. Sin perjuicio de los diversos cometidos específicos de
ambos, la dirección de dicho cambio es la misma en lo que respecta al actor de cine y al gobernante. Aspira, bajo determinadas
condiciones sociales, a exhibir sus actuaciones de manera más
comprobable e incluso más asumible. De lo cual resulta una
nueva selección, una selección ante esos aparatos, y de ella salen vencedores el dictador y la estrella de cine.

39

\_

tas, para entender semejante estado de la cuestión. Los
editores de periódicos no han organizado en balde concursos de carreras entre sus jóvenes repartidores. Y por
cierto que despiertan gran interés en los participantes.
El vencedor tiene la posibilidad de ascender de repartidor
de diarios a corredor de carreras. Los noticiarios, por
ejemplo, abren para todos la perspectiva de ascender de
transeúntes a comparsas en la pantalla. De este modo
puede en ciertos casos hasta verse incluido en una obra
de arte —recordemos Tres canciones sobre Lenin de
Wertoff o Borinage de Ivens. Cualquier hombre aspirará
hoy a participar en un rodaje. Nada ilustrará mejor esta
aspiración que una cala en la situación histórica de la
literatura actual.
Durante siglos las cosas estaban así en la literatura:
a un escaso número de escritores se enfrentaba un número de lectores mil veces mayor. Pero a fines del siglo
pasado se introdujo un cambio. Con la creciente expansión de la prensa, que proporcionaba al público lector
nuevos órganos políticos, religiosos, científicos, profesionales y locales, una parte cada vez mayor de esos lectores
pasó, por de pronto ocasionalmente, del lado de los
que escriben. La cosa empezó al abrirles su buzón la
prensa diaria; hoy ocurre que apenas hay un europeo en
curso de trabajo que no haya encontrado alguna vez ocasión de publicar una experiencia laboral, una queja, un
reportaje o algo parecido. La distinción entre autor y
público está por tanto a punto de perder su carácter sistemático. Se convierte en funcional y discurre de distinta
manera en distintas circunstancias. El lector está siempre
dispuesto a pasar a ser un escritor. En cuanto perito (que
para bien o para mal en perito tiene que acabar en un
proceso laboral sumamente especializado, si bien su peritaje lo será sólo de una función mínima), alcanza acceso
al estado de autor. En la Unión Soviética es el trabajo
mismo el que toma la palabra. Y su exposición verbal
constituye una parte de la capacidad que es requisito para
su ejercicio. La competencia literaria ya no se funda en
40

una educación especializada, sino politécnica. Se hace así
patrimonio común 20 .
Todo ello puede transponerse sin más al cine, donde
ciertas remociones, que en la literatura han reclamado
siglos, se realizan en el curso de un decenio. En la praxis
cinematográfica —sobre todo en la rusa— se ha consumado ya esa remoción esporádicamente. Una parte de los
actores que encontramos en el cine ruso no son actores
en nuestro sentido, sino gentes que desempeñan su propio
papel, sobre todo en su actividad laboral. En Europa occidental la explotación capitalista del cine prohibe atender la legítima aspiración del hombre actual a ser reproducido. En tales circunstancias la industria cinematográfica tiene gran interés en aguijonear esa participación de
las masas por medio de representaciones ilusorias y especulaciones ambivalentes.
20
Se pierde así el carácter privilegiado de las técnicas correspondientes. Aldous Huxley escribe: «Los progresos técnicos han
conducido... a la vulgarización... Las técnicas reproductivas y
las rotativas en la prensa han posibilitado una multiplicación imprevisible del escrito y de la imagen. La instrucción escolar generalizada y los salarios relativamente altos han creado un público muy grande capaz de leer y de procurarse material de
lectura y de imágenes. Para tener éstos a punto, se ha constituido una industria importante. Ahora bien, el talento artístico es muy raro; de ello se sigue... que en todo tiempo y lugar
una parte preponderante de la producción artística ha sido minusvalente. Pero hoy el porcentaje de desechos en el conjunto
de la producción artística es mayor que nunca... Estamos frente a una simple cuestión de aritmética. En el curso del siglo pas.ado ha aumentado en más del doble la población de Europa
occidental. El material de lectura y de imágenes calculo que
ha crecido por lo menos en una proporción de 1 a 2 y tal vez
a 50 o incluso a 100. Si una población de x millones tiene n talentos artísticos, una población de 2x millones tendrá 2rc talentos artísticos. La situación puede resumirse de la manera siguiente. Por cada página que hace cien años se publicaba impresa con escritura e imágenes, se publican hoy veinte, si no
cien. Por otro lado, si hace un siglo existía un talento artístico,
existen hoy dos. Concedo que, en consecuencia de la instrucción
escolar generalizada, gran número de talentos virtuales, que no
hubiesen antes llegado a desarrollar sus dotes, pueden hoy ha-

41

11

El rodaje de u n a película, y especialmente de u n a película sonora, ofrece aspectos que eran antes completam e n t e inconcebibles. Representa un proceso en el que es
imposible o r d e n a r u n a sola perspectiva sin que todo u n
mecanismo (aparatos de iluminación, cuadro de ayudantes, etc.), que de suyo no pertenece a la escena filmada,
interfiera en el campo visual del espectador (a no ser que
la disposición de su pupila coincida con la de la cámara).
Esta circunstancia hace, más que cualquier otra, que las
semejanzas, que en cierto m o d o se dan entre u n a escena
en el estudio cinematográfico y en las tablas, resulten superficiales y de poca m o n t a . El teatro conoce por principio el emplazamiento desde el que no se descubre sin
m á s ni más que lo que sucede es ilusión. En el rodaje
de u n a escena cinematográfica no existe ese emplazamiento. La naturaleza de su ilusión es de segundo grado;
es un resultado del montaje. Lo cual significa: en el estudio de cine el mecanismo ha penetrado tan hondamencerse productivos. Supongamos pues... que haya hoy tres o incluso cuatro talentos artísticos por uno que había antes. No por
eso deja de ser indudable que el consumo de material de lectura y de imágenes ha superado con mucho la producción natural de escritores y dibujantes dotados. Y con el material sonoro pasa lo mismo. La prosperidad, el gramófono y la radio han
dado vida a un público, cuyo consumo de material sonoro está
fuera de toda proporción con el crecimiento de la población y
en consecuencia con el normal aumento de músicos con talento.
Resulta por tanto que, tanto hablando en términos absolutos
como en términos relativos, la producción de desechos es en
todas las artes mayor que antes; y así seguirá siendo mientras
las gentes continúen con su consumo desproporcionado de material de lectura, de imágenes y sonoro» (ALDOUS HUXLEY, Croisiére d'hiver en Amérique Céntrale, París, pág. 273). Semejante
manera de ver las cosas está claro que no es progresivo.

42

te en la realidad que el aspecto puro de ésta, libre de todo
cuerpo extraño, es decir técnico, no es más que el resultado de un procedimiento especial, a saber el de la toma
por medio de un aparato fotográfico dispuesto a este
propósito y su montaje con otras tomas de igual índole.
Despojada de todo aparato, la realidad es en este caso
sobremanera artificial, y en el país de la técnica la visión
de la realidad inmediata se ha convertido en una flor imposible.
Este estado de la cuestión, tan diferente del propio
del teatro, es susceptible de una confrontación muy instructiva con el que se da en la pintura. Es preciso que
nos preguntemos ahora por la relación que hay entre
el operador y el pintor. Nos permitiremos una construcción auxiliar apoyada en el concepto de operador usual
en cirugía. El cirujano representa el polo de un orden
cuyo polo opuesto ocupa el mago. La actitud del mago,
que cura al enfermo imponiéndole las manos, es distinta de la del cirujano que realiza una intervención.
El mago mantiene la distancia natural entre él mismo y
su paciente. Dicho más exactamente: la aminora sólo un
poco por virtud de la imposición de sus manos, pero la
acrecienta mucho por virtud de su autoridad. El cirujano procede al revés: aminora mucho la distancia para
con el paciente al penetrar dentro de él, pero la aumenta
sólo un poco por la cautela con que sus manos se mueven entre sus órganos. En una palabra: a diferencia del
mago (y siempre hay uno en el médico de cabecera) el
cirujano renuncia en el instante decisivo a colocarse frente a su enfermo como hombre frente a hombre; más bien
se adentra en él operativamente. Mago y cirujano se comportan uno respecto del otro como el pintor y el cámara.
El primero observa en su trabajo una distancia natural
para con su dato; el cámara por el contrario se adentra
hondo en la textura de los datos 2I . Las imágenes que con21
Las audacias del cámara pueden de hecho compararse a las
del cirujano. En un catálogo de destrezas cuya técnica es específicamente de orden gestual, enuncia Luc Durtain las que «en

43

siguen ambos son enormemente diversas. La del pintor es
total y la del cámara múltiple, troceada en partes que
se juntan según una ley nueva. La representación cinematográfica de la realidad es para el hombre actual incomparablemente más importante, puesto que garantiza,
por razón de su intensa compenetración con el aparato,
un aspecto de la realidad despojado de todo aparato que
ese hombre está en derecho de exigir de la obra de arte.

12

La reproductibilidad técnica de la obra artística modifica la relación de la masa para con el arte. De retrógrada, frente a un Picasso por ejemplo, se transforma en
progresiva, por ejemplo cara a un Chaplin. Este comportamiento progresivo se caracteriza porque el gusto por
mirar y por vivir se vincula en él íntima e inmediatamente con la actitud del que opina como perito. Esta
vinculación es un indicio social importante. A saber, cuanto más disminuye la importancia social de un arte, tanto
más se disocian en el público la actitud crítica y la fruitiva. De lo convencional se disfruta sin criticarlo, y se
critica con aversión lo verdaderamente nuevo. En el público del cine coinciden la actitud crítica y la fruitiva.
ciertas intervenciones difíciles son imprescindibles en cirujía.
Escoj'o como ejemplo un caso de otorrinolaringología; ...me reñero al procedimiento que se llama perspectivo-endonasal; o señalo las destrezas acrobáticas que ha de llevar a cabo la cirujía
de laringe al utilizar un espejo que le devuelve una imagen invertida; también podría hablar de la cirujía de oídos cuya precisión en el trabajo recuerda al de los relojeros. Del hombre
que quiere reparar o salvar el cuerpo humano se requiere en
grado sumo una sutil acrobacia muscular. Basta con pensar en
la operación de cataratas, en la que el acero lucha por así decirlo con tejidos casi fluidos, o en las importantísimas intervenciones en la región abdominal (laparatomía).

44

Y desde luego que la circunstancia decisiva es ésta: las
reacciones de cada uno, cuya suma constituye la reacción
masiva del público, jamás han estado como en el cine
tan condicionadas de antemano por su inmediata, inminente masificación. Y en cuanto se manifiestan, se controlan. La comparación con la pintura sigue siendo provechosa. Un cuadro ha tenido siempre la aspiración eminente a ser contemplado por uno o por pocos. La contemplación simultánea de cuadros por parte de un gran público,
tal y como se generaliza en el siglo xix, es un síntoma
temprano de la crisis de la pintura, que en modo alguno
desató solamente la fotografía, sino que con relativa independencia de ésta fue provocada por la pretensión por
parte de la obra de arte de llegar a las masas.
Ocurre que la pintura no está en situación de ofrecer
objeto a una recepción simultánea y colectiva. Desde
siempre lo estuvo en cambio la arquitectura, como lo
estuvo antaño el epos y lo está hoy el cine. De suyo no
hay por qué sacar de este hecho conclusiones sobre el
papel social de la pintura, aunque sí pese sobre ella como
perjuicio grave cuando, por circunstancias especiales y
en contra de su naturaleza, ha de confrontarse con las
masas de una manera inmediata. En las iglesias y monasterios de la Edad Media, y en las cortes principescas
hasta casi finales del siglo dieciocho, la recepción colectiva de pinturas no tuvo lugar simultáneamente, sino por
mediación de múltiples grados jerárquicos. Al suceder
de otro modo, cobra expresión el especial conflicto en
que la pintura se ha enredado a causa de la reproductibilidad técnica de la imagen. Por mucho que se ha intentado presentarla a las masas en museos y en exposiciones,
no se ha dado con el camino para que esas masas puedan organizar y controlar su recepción n. Y así el mismo
22

Esta manera de ver las cosas parecerá quizás burda; pero
como muestra el gran teórico que fue Leonardo, las opiniones
burdas pueden muy bien ser invocadas a tiempo. Leonardo
compara la pintura y la música en los términos siguientes: «La
pintura es superior a la música, porque no tiene que morir ape45

público que es retrógrado frente al surrealismo, reaccionará progresivamente ante una película cómica.

13

El cine no sólo se caracteriza por la manera como el
hombre se presenta ante el aparato, sino además por
cómo con ayuda de éste se representa el mundo en torno.
Una ojeada a la psicología del rendimiento nos ilustrará
sobre la capacidad del aparato para hacer tests. Otra ojeada al psicoanálisis nos ilustrará sobre lo mismo bajo
otro aspecto. El cine ha enriquecido nuestro mundo perceptivo con métodos que de hecho se explicarían por los
de la teoría freudiana. Un lapsus en la conversación pasaba hace cincuenta años más o menos desapercibido.
Resultaba excepcional que de repente abriese perspectivas profundas en esa conversación que parecía antes
discurrir superficialmente. Pero todo ha cambiado desde
la Psicopatología de la vida cotidiana. Esta ha aislado cosas (y las ha hecho analizables), que antes nadaban inadvertidas en la ancha corriente de lo percibido. Tanto en el
mundo óptico, como en el acústico, el cine ha traído consigo una profundización similar de nuestra apercepción.
Pero esta situación tiene un reverso: las ejecuciones que
expone el cine son pasibles de análisis mucho más exacto
y más rico en puntos de vista que el que se llevaría a
cabo sobre las que se representan en la pintura o en la
escena. El cine indica la situación de manera incomparablemente más precisa, y esto es lo que constituye su manas se la llama a la vida, como es el caso infortunado de la
música... Esta, que se volatiliza en cuanto surge, va a la zaga
de la pintura, que con el uso del barniz se ha hecho eterna»
(cit. en Revue de Littérature comparée, febrero-marzo, 1935, página 79).

46

yor susceptibilidad de análisis frente a la pintura; respecto de la escena, dicha capacidad está condicionada
porque en el cine hay también más elementos susceptibles de ser aislados. Tal circunstancia tiende a favorecer
—y de ahí su capital importancia— la interpenetración
recíproca de ciencia y arte. En realidad, apenas puede
señalarse si un comportamiento limpiamente dispuesto
dentro de una situación determinada (como un músculo
en un cuerpo) atrae más por su valor artístico o por la
utilidad científica que rendiría. Una de las funciones revolucionarias del cine consistirá en hacer que se reconozca que la utilización científica de la fotografía y su
utilización artística son idénticas. Antes iban generalmente cada una por su lado 23 .
Haciendo primeros planos de nuestro inventario, subrayando detalles escondidos de nuestros enseres más
corrientes, explorando entornos triviales bajo la guía genial del objetivo, el cine aumenta por un lado los atisbos en el curso irresistible por el que se rige nuestra
existencia, pero por otro lado nos asegura un ámbito de
acción insospechado, enorme. Parecía que nuestros bares,
nuestras oficinas, nuestras viviendas amuebladas, nuestras estaciones y fábricas nos aprisionaban sin esperanza.
Entonces vino el cine y con la dinamita de sus décimas
de segundo hizo saltar ese mundo carcelario. Y ahora
emprendemos entre sus dispersos escombros viajes de
23

Si buscamos una situación análoga, se nos ofrece como tal,
y muy instructivamente, la pintura del Renacimiento. Nos encontramos en ella con un arte cuyo auge incomparable y cuya
importancia consisten en gran parte en que integran un número de ciencias nuevas o de datos nuevos de la ciencia. Tiene
pretensiones sobre la anatomía y la perspectiva, las matemáticas, la meteorología y la teoría de los colores. Como escribe
Valéry: «Nada hay más ajeno a nosotros que la sorprendente
pretensión de un Leonardo, para el cual la pintura era una meta
suprema y la suma demostración del conocimiento, puesto que
estaba convencido de que exigía la ciencia universal. Y él mismo
no retrocedía ante un análisis teórico, cuya precisión y hondura nos desconcierta hoy» (PAUL VALÉRY, Piéces sur l'art, París,
1934, pág. 191).

47

aventuras. Con el primer plano se ensancha el espacio
y bajo el retardador se alarga el movimiento. En una
ampliación no sólo se trata de aclarar lo que de otra
manera no se vería claro, sino que más bien aparecen
en ella formaciones estructurales del todo nuevas. Y tampoco el retardador se limita a aportar temas conocidos
del movimiento, sino que en éstos descubre otros enteramente desconocidos.' que «en absoluto operan como lentificaciones de movimientos más rápidos, sino propiamente en cuanto movimientos deslizantes, flotantes, supraterrenales» 24. Así es como resulta perceptible que la naturaleza que habla a la cámara no es la misma que la que
habla al ojo. Es sobre todo distinta porque en lugar de
un espacio que trama el hombre con su consciencia presenta otro tramado inconscientemente. Es corriente que
pueda alguien darse cuenta, aunque no sea más que a
grandes rasgos, de la manera de andar de las gentes, pero
desde luego que nada sabe de su actitud en esa fracción
de segundo en que comienzan a alargar el paso. Nos resulta más o menos familiar el gesto que hacemos al coger el
encendedor o la cuchara, pero apenas si sabemos algo
de lo que ocurre entre la mano y el metal, cuanto menos
de sus oscilaciones según los diversos estados de ánimo
en que nos encontremos. Y aquí es donde interviene la
cámara con sus medios auxiliares, sus subidas y sus bajadas, sus cortes y su capacidad aislativa, sus dilataciones y arrezagamientos de un decurso, sus ampliaciones y
disminuciones.; Por su virtud experimentamos el inconsciente óptico, igual que por medio del psicoanálisis nos
enteramos del inconsciente pulsional.

24

RUDOLF ARXHEIM, /. c, pág.

138.

48

14

Desde siempre ha venido siendo uno de los cometidos
más importantes del arte provocar una demanda cuando
todavía no ha sonado la hora de su satisfacción plena 25.
La historia de toda forma artística pasa por tiempos críticos en los que tiende a urgir efectos que se darían sin
esfuerzo alguno en un tenor técnico modificado, esto es,
en una forma artística nueva. Y así las extravagancias y
crudezas del arte, que se producen sobre todo en los llamados tiempos decadentes, provienen en realidad de su
centro virtual histórico más rico. Últimamente el dadaísmo ha rebosado de semejantes barbaridades. Sólo ahora
entendemos su impulso: el dadaísmo intentaba, con los
medios de la pintura (o de la literatura respectivamente),
producir los efectos que el público busca hoy en el cine.
25

André Bretón dice que «la obra de arte solo tiene valor
cuando tiembla de reflejos del futuro». En realidad toda forma
artística elaborada se encuentra en el cruce de tres líneas de
evolución. A saber, la técnica trabaja por de pronto en favor
de una determinada forma de arte. Antes de que llegase el cine
había cuadernillos de fotos cuyas imágenes, a golpe de pulgar,
hacían pasar ante la vista a la velocidad del rayo una lucha de
boxeo o una partida de tenis; en los bazares había juguetes automáticos en los que la sucesión de imágenes era provocada por
el giro de una manivela. En segundo lugar, formas artísticas
tradicionales trabajan esforzadamente en ciertos niveles de su
desarrollo por conseguir efectos que más tarde alcanzará con
toda espontaneidad la forma artística nueva. Antes de que el cine :
estuviese en alza, los dadaístas procuraban con sus manifestaciones introducir en el público un movimiento que un Chaplin
provocaría después de manera más natural. En tercer lugar,
modificaciones sociales con frecuencia nada aparentes trabajan
en orden a un cambio en la recepción que sólo favorecerá a la
hueva forma artística. Antes de que el cine empezase a formar

49

Toda provocación de demandas fundamentalmente
nuevas, de esas que abren caminos, se dispara por encima
de su propia meta. Así lo hace el dadaísmo en la medida
en que sacrifica valores del mercado, tan propios del
cine, en favor de intenciones más importantes de las que,
tal y como aquí las describimos, no es desde luego consciente. Los dadaístas dieron menos importancia a la utilidad mercantil de sus obras de arte que a su inutilidad
como objetos de inmersión contemplativa. Y en buena
parte procuraron alcanzar esa inutilidad por medio de
una degradación sistemática de su material. Sus poemas
son «ensaladas de palabras» que contienen giros obscenos y todo detritus verbal imaginable. E igual pasa con
sus cuadros, sobre los que montaban botones o billetes
de tren o de metro o de tranvía. Lo que consiguen de
esta manera es una destrucción sin miramientos del aura
de sus creaciones. Con los medios de producción imprimen en ellas el estigma de las reproducciones. Ante un
cuadro de Arp o un poema de August Stramm es imposible emplear un tiempo en recogerse y formar un juicio,
tal y como lo haríamos ante un cuadro de Derain o un
poema de Rilke. Para una burguesía degenerada el recogimiento se convirtió en una escuela de conducta asocial,
su público, hubo imágenes en el Panorama imperial (imágenes
que ya habían dejado de ser estáticas) para cuya recepción se
reunía un público. Se encontraba éste ante un biombo en el
que estaban instalados estereoscopios, cada uno de los cuales
se dirigía a cada visitante. Ante esos estereoscopios aparecían
automáticamente imágenes que se detenían apenas y dejaban
luego su sitio a otras. Con medios parecidos tuvo que trabajar
Edison cuando, antes de que se conociese la pantalla y el procedimiento de la proyección, pasó la primera banda filmada
ante un pequeño público que miraba estupefacto un aparato en
el que se desenrollaban las imágenes. Por cierto que en la disposición del Panorama imperial se expresa muy claramente una
dialéctica del desarrollo. Poco antes de que el cine convirtiese en
colectiva la visión de imágenes, cobra ésta vigencia en forma individualizada ante los estereoscopios de aquel establecimiento,
pronto anticuado, con la misma fuerza que antaño tuviera en la
«celia» la visión de la imagen de los dioses por parte del
sacerdote.

50

y a él se le enfrenta ahora la distracción como una variedad de comportamiento social 26 . Al hacer de la obra de
arte un centro de escándalo, las manifestaciones dadaístas
garantizaban en realidad una distracción muy vehemente.
Había sobre todo que dar satisfacción a una exigencia,
provocar escándalo público.
De ser una apariencia atractiva o una hechura sonora convincente, la obra de arte pasó a ser un proyectil.
Chocaba con todo destinatario. Había adquirido una calidad táctil. Con lo cual favoreció la demanda del cine,
cuyo elemento de distracción es táctil en primera línea,
es decir que consiste en un cambio de escenarios y de
enfoques que se adentran en el espectador como un
choque. Comparemos el lienzo (pantalla) sobre el que se
desarrolla la película con el lienzo en el que se encuentra
una pintura. Este último invita a la contemplación; ante
él podemos abandonarnos al fluir de nuestras asociaciones de ideas. Y en cambio no podremos hacerlo ante un
plano cinematográfico. Apenas lo hemos registrado con
los ojos y ya ha cambiado. No es posible fijarlo. Duhamel,
que odia el cine y no ha entendido nada de su importancia, pero sí lo bastante de su estructura, anota esta circunstancia del modo siguiente: «Ya no puedo pensar lo
que quiero. Las imágenes movedizas sustituyen a mis pensamientos» 27. De hecho, el curso de las asociaciones en la
mente de quien contempla las imágenes queda enseguida
interrumpido por el cambio de éstas. Y en ello consiste
el efecto de choque del cine que, como cualquier otro,
pretende ser captado gracias a una presencia de espíritu
26
El arquetipo teológico de este recogimiento es la consciencia de estar a solas con Dios. En las grandes épocas de la burguesía ésta consciencia ha dado fuerzas a la libertad para sacudirse la tutela de la Iglesia. En las épocas de su decadencia la
misma consciencia tuvo que tener en cuenta la tendencia secreta a que en los asuntos de la comunidad estuviesen ausentes las
fuerzas que el individuo pone por obra en su trato con Dios.
27
GEORGES DUHAMEL, Scénes de la vie future, París, 1930, página 52.

51

más intensa 28 . Por virtud de su estructura técnica el cine
ha liberado al efecto físico de choque del embalaje por
así decirlo moral en que lo retuvo el dadaísmo 29 .

15

La masa es una matriz de la que actualmente surte,
como vuelto a nacer, todo comportamiento consabido
frente a las obras artísticas. La cantidad se ha convertido en calidad: el crecimiento masivo del número de
participantes ha modificado la índole de su participación.
Que el observador no se llame a engaño porque dicha
participación aparezca por de pronto bajo una forma desacreditada. No han faltado los que, guiados por su pasión, se han atenido precisamente a este lado superficial
del asunto. Duhamel es entre ellos el que se ha expresado
" El cine es la forma artística que corresponde al creciente
peligro en que los hombres de hoy vemos nuestra vida. La necesidad de exponerse a efectos de choque es una acomodación del
hombre a los peligros que le amenazan. El cine corresponde a
modificaciones de hondo alcance en el aparato perceptivo, modificaciones que hoy vive a escala de existencia privada todo transeúnte en el tráfico de una gran urbe, así como a escala históLrica cualquier ciudadano de un Estado contemporáneo.
29
Del cine podemos lograr informaciones importantes tanto
en lo que respecta al dadaísmo como al cubismo y al futurismo.
Estos dos últimos aparecen como tentativas insuficientes del
arte para tener en cuenta la imbricación de la realidad y los
aparatos. Estas escuelas emprendieron su intento no a través
de una valoración de los aparatos en orden a la representación
artística, que así lo hizo el cine, sino por medio de una especie
de mezcla de la representación de la realidad y de la de los
aparatos. En el cubismo el papel preponderante lo desempeña
el presentimiento de la construcción, apoyada en la óptica, de
esos aparatos; en el futurismo el presentimiento de sus efectos,
que cobrarán todo su valor en el rápido decurso de la película
de cine.
52

de modo más radical. Lo que agradece al cine es esa participación peculiar que despierta en las masas. Le llama
«pasatiempo para parias, disipación para iletrados, para
criaturas miserables aturdidas por sus trajines y sus preocupaciones..., un espectáculo que no reclama esfuerzo
alguno, que no supone continuidad en las ideas, que no
plantea ninguna pregunta, que no aborda con seriedad
ningún problema, que no enciende ninguna pasión, que
no alumbra ninguna luz en el fondo de los corazones, que
no excita ninguna otra esperanza a no ser la esperanza ridicula de convertirse un día en «star» en Los Angeles» 30. Ya vemos que en el fondo se trata de la antigua
queja: las masas buscan disipación, pero el arte reclama
recogimiento. Es un lugar común. Pero debemos preguntarnos si da lugar o no para hacer una investigación acerca del cine.
Se trata de mirar más de cerca. Disipación y recogimiento se contraponen hasta tal punto que permiten la
fórmula siguiente: quien se recoge ante una obra de arte,
se sumerge en ella; se adentra en esa obra, tal y como
narra la leyenda que le ocurrió a un pintor chino al
contemplar acabado su cuadro. Por el contrario, la masa
dispersa sumerge en sí misma a la obra artística. Y de
manera especialmente patente a los edificios. La arquitectura viene desde siempre ofreciendo el prototipo de
una obra de arte, cuya recepción sucede en la disipación
y por parte de una colectividad. Las leyes de dicha recepción son sobremanera instructivas.
Las edificaciones han acompañado a la humanidad
desde su historia primera. Muchas formas artísticas han
surgido y han desaparecido. La tragedia nace con los griegos para apagarse con ellos y revivir después sólo en
cuanto a sus reglas. El epos, cuyo origen está en la juventud de los pueblos, caduca en Europa al terminar el
Renacimiento. La pintura sobre tabla es una creación de
la Edad Media y no hay nada que garantice su duración
ininterrumpida. Pero la necesidad que tiene el hombre
30

GEORGES DUHAMEL, l. C, pág. 58.

53

de alojamiento sí que es estable. El arte de la edificación no se ha interrumpido jamás. Su historia es más
larga que la de cualquier otro arte, y su eficacia al presentizarse es importante para todo intento de dar cuenta
de la relación de las masas para con la obra artística.
Las edificaciones pueden ser recibidas de dos maneras:
por el uso y por la contemplación. O mejor dicho: táctil
y ópticamente. De tal recepción no habrá concepto posible si nos la representamos según la actitud recogida que,
por ejemplo, es corriente en turistas ante edificios famosos. A saber: del lado táctil no existe correspondencia
alguna con lo que del lado óptico es la contemplación.
La recepción táctil no sucede tanto por la vía de la atención como por la de la costumbre. En cuanto a la arquitectura, esta última determina en gran medida incluso la
recepción óptica. La cual tiene lugar, de suyo, mucho menos en una atención tensa que en una advertencia ocasional. Pero en determinadas circunstancias esta recepción
formada en la arquitectura tiene valor canónico. Porque
las tareas que en tiempos de cambio se le imponen al
aparato perceptivo del hombre no pueden resolverse por
la vía meramente óptica, esto es por la de la contemplación. Poco a poco quedan vencidas por la costumbre
(bajo la guía de la recepción táctil).
También el disperso puede acostumbrarse. Más aún:
sólo cuando resolverlas se le ha vuelto una costumbre,
probará poder hacerse en la dispersión con ciertas tareas.
Por medio de la dispersión, tal y como el arte la depara,
se controlará bajo cuerda hasta qué punto tienen solución las tareas nuevas de la apercepción. Y como, por
lo demás, el individuo está sometido a la tentación de
hurtarse a dichas tareas, el arte abordará la más difícil
e importante movilizando a las masas. Así lo hace actualmente en el cine. La recepción en la dispersión, que se
hace notar con insistencia creciente en todos los terrenos
del arte y que es el síntoma de modificaciones de hondo
alcance en la apercepción, tiene en el cine su instrumento
de entrenamiento. El cine corresponde a esa forma recep54

tiva por su efecto de choque. No sólo reprime el valor
cultual porque pone al público en situación de experto,
sino además porque dicha actitud no incluye en las salas
de proyección atención alguna. El público es un examinador, pero un examinador que se dispersa.

EPILOGO

La proletarización creciente del hombre actual y el
alineamiento también creciente de las masas son dos caras de uno y el mismo suceso. El fascismo intenta organizar las masas recientemente proletarizadas sin tocar las
condiciones de la propiedad que dichas masas urgen por
suprimir. El fascismo ve su salvación en que las masas
lleguen a expresarse (pero que ni por asomo hagan valer
sus derechos) 31 . Las masas tienen derecho a exigir que
se modifiquen las condiciones de la propiedad; el fascismo procura que se expresen precisamente en la conservación de dichas condiciones. En consecuencia, des31

Una circunstancia técnica resulta aquí importante, sobre
todo respecto de los noticiarios cuya significación propagandística apenas podrá ser valorada con exceso. A la reproducción masiva corresponde en efecto la reproducción de masas. La masa
se mira a la cara en los grandes desfiles festivos, en las asambleas monstruos, en las enormes celebraciones deportivas y en
la guerra, fenómenos todos que pasan ante la cámara. Este proceso, cuyo alcance no necesita ser subrayado, está en relación
estricta con el desarrollo de la técnica reproductiva y de rodaje.
Los movimientos de masas se exponen más claramente ante los
aparatos que ante el ojo humano. Sólo a vista de pájaro se captan bien esos cuadros de centenares de millares. Y si esa perspectiva es tan accesible al ojo humano como a los aparatos, también es cierto que la ampliación a que se somete la toma de la
cámara no es posible en la imagen ocular. Esto es, que los movimientos de masas y también la guerra representan una forma
de comportamiento humano especialmente adecuada a los aparatos técnicos.

55

emboca en un esteticismo de la vida política. A la violación de las masas, que el fascismo impone por la fuerza
en el culto a un caudillo, corresponde la violación de
todo un mecanismo puesto al servicio de la fabricación
de valores cultuales.
Todos los esfuerzos por un esteticismo político culminan en un solo punto. Dicho punto es la guerra. La guerra, y sólo ella, hace posible dar una meta a movimientos
de masas de gran escala, conservando a la vez las condiciones heredadas de la propiedad. Así es como se formula el estado de la cuestión desde la política. Desde la técnica se formula del modo siguiente: sólo la guerra hace
posible movilizar todos los medios técnicos del tiempo
presente, conservando a la vez las condiciones de la propiedad. Claro que la apoteosis de la guerra en el fascismo
no se sirve de estos argumentos. A pesar de ¡o cual es instructivo echarles una ojeada. En el manifiesto de Marinetti sobre la guerra colonial de Etiopía se llega a decir:
«Desde hace veintisiete años nos estamos alzando los futuristas en contra de que se considere a la guerra antiestética... Por ello mismo afirmamos: la guerra es bella,
porque, gracias a las máscaras de gas, ai terrorífico megáfono, a los lanzallamas y a las tanquetas, funda la soberanía del hombre sobre la máquina subyugada. La guerra es bella, porque inaugura el sueño de la metalización
del cuerpo humano. La guerra es bella, ya que enriquece
las praderas florecidas con las orquídeas de fuego de las
ametralladoras. La guerra es bella, ya que reúne en una
sinfonía los tiroteos, los cañonazos, los altos el fuego,
los perfumes y olores de la descomposición. La guerra es
bella, ya que crea arquitecturas nuevas como la de los
tanques, la de las escuadrillas formadas geométricamente, la de las espirales de humo en las aldeas incendiadas
y muchas otras... ¡Poetas y artistas futuristas... acordaos
de estos principios fundamentales de una estética de la
guerra para que iluminen vuestro combate por una nueva
poesía, por unas artes plásticas nuevas!» 32.
32

La Stampa, Turín.
56

Este manifiesto tiene la ventaja de ser claro. Merece
que el dialéctico adopte su planteamiento de la cuestión.
La estética de la guerra actual se le presenta de la manera siguiente: mientras que el orden de la propiedad impide el aprovechamiento natural de las fuerzas productivas, el crecimiento de los medios técnicos, de los ritmos,
de la fuentes de energía, urge un aprovechamiento antinatural. Y lo encuentra en la guerra que, con sus destrucciones, proporciona la prueba de que la sociedad no estaba todavía lo bastante madura para hacer de la técnica su órgano, y de que la técnica tampoco estaba suficientemente elaborada para dominar las fuerzas elementales
de la sociedad. La guerra imperialista está determinada
en sus rasgos atroces por la discrepancia entre los poderosos medios de producción y su aprovechamiento insuficiente en el proceso productivo (con otras palabras: por
el paro laboral y la falta de mercados de consumo). La
guerra imperialista es un levantamiento de la técnica,
que se cobra en el material humano las exigencias a las
que la sociedad ha sustraído su material natural. En lugar de canalizar ríos, dirige la corriente humana al lecho
de sus trincheras; en lugar de esparcir grano desde sus
aeroplanos, esparce bombas incendiarias sobre las ciudades; y la guerra de gases ha encontrado un medio nuevo
para acabar con el aura.
«Fiat ars, pereat mundus», dice el fascismo, y espera
de la guerra, tal y como lo confiesa Marinetti, la satisfacción artística de la percepción sensorial modificada por
la técnica. Resulta patente que esto es la realización acabada del «arte pour l'art». La humanidad, que antaño, en
Homero, era un objeto de espectáculo para los dioses
olímpicos, se ha convertido ahora en espectáculo de sí
misma. Su autoalienación ha alcanzado un grado que le
permite vivir su propia destrucción como un goce estético de primer orden. Este es el esteticismo de la política
que el fascismo propugna. El comunismo le contesta con
la politización del arte.

57

