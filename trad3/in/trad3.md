## PRÓLOGO {#prólogo title="Prólogo"}

Cuando Marx emprendió el análisis del modo de producción capitalista,
dicho modo se hallaba en sus inicios. Marx orientó en tal forma sus
investigaciones que éstas adquirieron valor de pronóstico. Así, tras
remontarse a las relaciones fundamentales de la producción capitalista,
las representó de manera que de ellas resultaba cuanto, en el futuro, se
podía esperar del capitalismo. Y resultó que de él podía esperarse no
solamente la explotación creciente y más aguda de los proletarios, sino,
en último término, también la instauración de condiciones que hacen
posible su propia supresión.

La transformación de la superestructura, cuyo avance es más lento que el
de la infraestructura que subyace, necesitó más de medio siglo para
hacer valer en todos los ámbitos culturales el cambio en las condiciones
productivas. De qué manera ha sucedido eso solamente hoy puede
indicarse, con cuya indicación se justifica un número concreto de
exigencias que adquieren un carácter de pronóstico. Aún así, habrá que
señalar que a éstas les corresponden menos tesis respecto al arte del
proletariado tras el momento de la toma del poder ---por no hablar aquí
del que respecta a la llamada sociedad sin clases---, que unas tesis
sobre las tendencias evolutivas del ámbito del arte en las presentes
condiciones de producción. Y la dialéctica que es la propia de éstas se
hace por cierto no menos perceptible en la superestructura que en la
economía. Por eso sería erróneo subestimar el valor del conflicto de
estas tesis. Éstas dejan de lado buen número de conceptos tradicionales
---como creatividad y genialidad, misterio y valor de eternidad---,
conceptos cuya aplicación incontrolada (y de momento difícilmente
controlable) conduce a la elaboración del material objetivo poseído en
sentido fascista. *Los conceptos que siguen, introducidos por primera
velen la teoría del arte, se distinguen de los más corrientes por ser
completamente inutilizables para los fines propios del fascismo, siendo
por el contrario utilizables para la formulación de exigencias
revolucionarias en lo que es la política del arte*.

## I

La obra de arte ha sido siempre fundamentalmente reproducible, pues lo
hecho por hombres siempre podían volver a hacerlo hombres. Ese tipo de
réplica la practicaron siempre por igual los discípulos que se
ejercitaban en el arte, los maestros que difundían obras de maestros, y
los terceros ávidos de lucro. Por el contrario, la reproducción técnica
de la obra de arte viene a ser algo nuevo que se impone
intermitentemente en la historia, a impulsos muy distanciados entre sí
pero con creciente intensidad. Los griegos solamente conocían dos modos
técnicos de reproducción de las obras de arte como tales: la fundición y
la acuñación. Bronces, terracotas y monedas eran las únicas entre las
obras de arte que se podían producir en masa. Cualquier otra era
irrepetible y no se podía reproducir técnicamente. Luego, mediante la
xilografía, se hizo técnicamente reproducible por vez primera el
grafismo, ya mucho antes de que con la imprenta se hiciera reproducible
la escritura. Los enormes cambios que la imprenta, técnica de
reproducción de la escritura, provocaría en la literatura nos son bien
conocidos. Mas no son sino un caso, por supuesto importante, del
fenómeno que aquí se considera a escala de la historia universal. En el
curso también de la Edad Media, a la xilografía se le añaden la
calcografía y el aguafuerte, como a comienzos del siglo [XIX]{.small} se
añadiría la litografía.

Con esta última, la técnica de reproducción alcanzará una fase
radicalmente nueva. El procedimiento de mayor efectividad que diferencia
la transposición del dibujo realizado sobre piedra de su incisión en un
bloque de madera o de su alternativa grabación en una plancha de cobre
daba al grafismo por primera vez la efectiva posibilidad de poner sus
productos en el mercado no ya exclusivamente de manera masiva (como
antes), sino en nuevas configuraciones cada día. La litografía capacitó
así al grafismo para acompañar la vida cotidiana con la inclusión de sus
ilustraciones, comenzando a ir al paso de la imprenta, por más que en
esta empresa le aventajara la fotografía pocas décadas después de su
invención. Y ya en el caso de la fotografía, en el proceso de
reproducción plástica la mano se verá por vez primera descargada de las
obligaciones artísticas esenciales, que, en adelante, recaerán tan sólo
sobre el ojo que mira a través del objetivo. Y como el ojo capta con
mayor rapidez que dibuja la mano, el proceso de reproducción plástica se
aceleró tan enormemente que ya podía marchar al paso del habla. Así
también, el operador cinematográfico cuando está rodando en el estudio,
fija la sucesión de las imágenes a la misma velocidad con que habla el
actor. Si la litografía contenía virtualmente el diario ilustrado, la
fotografía contenía virtualmente a su vez el cine sonoro. La
reproducción técnica del sonido, por su parte, se acometió a finales del
pasado siglo. De este modo, esfuerzos convergentes hicieron previsible
una situación que Valéry caracteriza con la siguiente frase: «Tal como
el agua, el gas y la corriente eléctrica llegan a nuestros hogares desde
lejos para servirnos con imperceptible maniobra, así se nos abastecerá
en el futuro igualmente de imágenes visuales o series organizadas de
sonidos que aparecerán y volverán a abandonarnos con un pequeño gesto,
casi un signo»[^\[37\]^](../Text/notas.xhtml#nt37){#rf37}. *Hacia el
1900, la reproducción técnica había alcanzado un nivel que no sólo
comenzaba a convertir en su objeto el conjunto de las obras de arte
tradicionales, sometiendo su efecto a las transformaciones más
profundas, sino que conquistó su lugar propio entre los procedimientos
artísticos vigentes*. En lo que hace al estudio de este nivel, nada hay
más instructivo que la forma en que sus dos distintas manifestaciones
---la reproducción de la obra de arte y el arte cinematográfico como
tal--- repercuten ahora sobre el arte en su forma aún tradicional.

## II {#sigil_toc_id_19}

Hasta a la más perfecta reproducción le falta *algo*: el aquí y el ahora
de la obra de arte, su existencia siempre irrepetible en el lugar mismo
en que se encuentra. Pero en esa existencia y sólo en ella ha encontrado
su consumación la historia a la que ha estado sometida en el curso de su
devenir. Ahí se cuentan tanto las transformaciones que con el correr del
tiempo ha ido sufriendo en su estructura física como las cambiantes
relaciones de propiedad en que ha podido
entrar[^\[38\]^](../Text/notas.xhtml#nt38){#rf38}. El rastro que han
dejado las primeras sólo puede seguirse en análisis de índole química o
física que no podrían ser efectuadas en el caso de la reproducción; y el
de las segundas es objeto de tradiciones cuyo seguimiento debe partir
del emplazamiento en que se halle del original.

El aquí y el ahora del original de que se trate constituye el concepto
de lo que es su autenticidad. Así, en tal sentido, los análisis químicos
hechos sobre la pátina de un bronce pueden traer su establecimiento;
correspondientemente, la comprobación efectiva de que un manuscrito
medieval procede de un archivo del siglo [XV]{.small} puede contribuir
del mismo modo a establecer su autenticidad. El ámbito *de la
autenticidad en cuanto tal se sustrae por entero a la reproductibilidad
técnica indicada, y, en realidad, no sólo a
ella[^\[39\]^](../Text/notas.xhtml#nt39){#rf39}*. Pero mientras que
frente a la reproducción manual tradicional, tildada casi siempre por
aquél de falsificación, lo auténtico conserva plena autoridad, no es ése
el caso en cambio frente a la reproducción técnica de la obra. Y la
razón es doble. En primer lugar, la reproducción técnica resulta mucho
más independiente respecto al original que la manual. Puede así, por
ejemplo, resaltar en la fotografía ciertos aspectos del original sólo
accesibles a la lente móvil que elige su punto de vista a voluntad, pero
no al ojo humano, o, con la ayuda de procedimientos como la ampliación o
la larga exposición, captar imágenes fuera del alcance de lo que es la
óptica natural. Eso es lo primero. En segundo lugar, puede poner a la
copia en situaciones que no están al alcance del que es el propio
original. Ante todo, permite que éste salga al encuentro del propio
perceptor, ya sea en forma de fotografía o en la de disco fonográfico.
La catedral abandona su emplazamiento para ser acogida en el estudio del
amante del arte; la obra coral que se ejecutó en una sala, o bien al
aire libre, puede escucharse en una habitación.

Las circunstancias a que hoy puede someterse lo que es el producto de la
reproducción técnica de la obra de arte quizá dejen por lo demás intacta
la consistencia propia de esa obra, pero en todos los casos devalúan su
aquí y su ahora. Si esto tampoco vale solamente para la obra de arte
sino del mismo modo por ejemplo para el paisaje que pasa en la película
por delante del espectador, en el objeto de arte este proceso va a
afectar a un núcleo muy sensible que en ningún objeto de la naturaleza
resulta vulnerable en tal medida. Y eso es su autenticidad. La
autenticidad de una cosa es la suma de cuanto desde su origen nos
resulta en ella transmisible, desde su duración de material a lo que
históricamente testimonia. Como lo último se funda en la primera, al
producirse la reproducción, donde la primera se sustrae para los
hombres, el testimonio histórico de la cosa igualmente vacila por su
parte. Sin duda, por supuesto, sólo éste; pero lo que vacila de este
modo es la autoridad de la cosa como
tal[^\[40\]^](../Text/notas.xhtml#nt40){#rf40}.

Lo que aquí falla se puede resumir en el concepto de aura, diciendo en
consecuencia: en la época de la reproductibilidad técnica, lo que queda
dañado de la obra de arte, eso mismo es su aura. Y es que el proceso es
sintomático, desbordando su significado el estricto ámbito del arte, ya
que *la técnica de la reproducción, según puede formularse en general,
desgaja al tiempo lo reproducido respecto al ámbito de la tradición. Al
multiplicar la reproducción, sustituye su ocurrencia irrepetible por una
masiva. Y al permitir a la reproducción el encontrarse con el espectador
en la situación en la que éste se encuentra, actualiza lo reproducido*.
Estos dos procesos llevan a una violenta sacudida de lo que es
transmitido: la sacudida de la tradición que resulta el reverso de la
crisis y renovación actuales de la humanidad, tan estrechamente
conectadas con los movimientos de masas de nuestros días. Su más
poderoso agente es sin duda el cine, cuyo significado social, hasta en
la más positiva de sus formas y justamente en ella, no resulta por
cierto concebible sin incluir su aspecto destructivo, catártico: la
liquidación del valor de la tradición dentro de la herencia cultural.
Donde más se muestra este fenómeno es en las grandes películas
históricas. El cine integra en su ámbito posiciones crecientemente más
remotas. En el 1927 Abel Gance exclamó con entusiasmo: «Shakespeare,
Rembrandt, Beethoven harán cine... Todas las leyendas, las mitologías y
los mitos, todos los fundadores de religiones, e incluso las religiones
en su conjunto... esperan su luminosa resurrección, mientras los héroes
se agolpan en las puertas»[^\[41\]^](../Text/notas.xhtml#nt41){#rf41}. A
esta general liquidación nos invitaba, inconsciente, el cineasta.

## III {#sigil_toc_id_20}

*Al interior de grandes intervalos históricos, junto con los modos
globales de existencia que se corresponden a los colectivos humanos se
transforman también, al mismo tiempo, el modo y la manera de su
percepción sensible*. Pues el modo y manera en que la percepción
sensible humana se organiza ---como medio en el que se produce--- no
está sólo natural sino también históricamente condicionado. La época de
las invasiones bárbaras, en la que surgieron la industria artística
tardorromana así como el Génesis de Viena, no tenía tan sólo un arte
bien distinto del de los antiguos, sino también otra percepción. Los
eruditos de la escuela vienesa, Riegl y Wickhoff, que se oponían al peso
de la tradición clásica bajo el que estaba enterrado dicho arte, fueron
los primeros en extraer conclusiones sobre la organización de la
percepción durante el período en que estuvo vigente. Por importantes que
fueran sus conocimientos, encontraron su límite en el hecho de que estos
grandes investigadores se contentarían con señalar los distintivos
formales de la percepción a lo largo de la época tardorromana. No
intentaron mostrar ---y quizá no podían esperarlo--- los cambios
estructurales y sociales que encontraban su forma de expresión en estas
transformaciones perceptivas. En la actualidad, las condiciones para una
comprensión más adecuada nos son más favorables. Y si las
transformaciones producidas en el medio de la percepción del que
nosotros somos coetáneos pueden concebirse justamente como la señalada
decadencia del aura, las condiciones de ésta, por su parte, se pueden
igualmente señalar.

Ese concepto de aura, que queda sugerido más arriba para objetos
históricos, conviene ahora ilustrarlo con el aura propia de los objetos
naturales. En efecto, definimos esta última como la aparición
irrepetible de una lejanía por cercana que ésta pueda hallarse. Ir
siguiendo, mientras se descansa, durante una tarde de verano, en el
horizonte, una cadena de montañas, o una rama que cruza proyectando su
sombra sobre el que reposa: eso significa respirar el aura de aquellas
montañas, de esta rama. De la mano de esta descripción es bastante fácil
comprender el condicionamiento social que se deduce de la decadencia
actual del aura. Estriba este extremo en dos circunstancias, ambas
igualmente conectadas con la importancia creciente de las masas en la
vida actual. «*Aproximar», espacial y humanamente, las cosas hasta sí es
para las masas actuales un deseo tan
apasionado[^\[42\]^](../Text/notas.xhtml#nt42){#rf42}* *como lo es
igualmente su tendencia a intentar la superación de lo irrepetible de
cualquier dato al aceptar su reproducción*. Cada día se hace más
irrecusablemente válida la necesidad de apoderarse del objeto desde la
distancia más corta de la imagen, o más bien en la copia, es decir, en
la reproducción. Reproducción que incontestablemente, tal como nos la
sirven en el diario ilustrado y el noticiario semanal, se distingue por
cierto de la imagen. Irrepetibilidad y duración están en ésta imbricadas
tan estrechamente como fugacidad y repetibilidad lo están en aquélla. La
liberación del objeto de su envoltorio, la destrucción del aura, es
distintivo de una percepción cuya «sensibilidad para lo homogéneo en el
mundo» ha crecido tanto actualmente que, a través de la reproducción,
sobrepasa también lo irrepetible. Se anuncia así en el ámbito intuitivo
aquello que en el ámbito de la teoría se nos hace ahora manifiesto como
creciente importancia de lo estadístico. La adecuación de la realidad a
las masas y de las masas a la realidad es un proceso de trascendencia
ilimitada tanto en lo que hace al pensamiento como en el ámbito de la
intuición.

## IV {#sigil_toc_id_21}

La irrepetibilidad de la obra de arte es idéntica a su integración en el
contexto de la tradición. Una tradición que es, por supuesto, algo
absolutamente vivo, mutable de manera extraordinaria. Una estatua
antigua de la diosa Venus, por ejemplo, estaba situada entre los griegos
---los cuales hacían de ella un objeto de culto--- dentro de un contexto
tradicional absolutamente diferente a entre los clérigos medievales, los
cuales veían en ella un infausto ídolo. Pero unos y otros se enfrentaban
del mismo modo a su irrepetibilidad, dicho en otras palabras: a su aura.
El modo original de integración de la obra de arte en el contexto de la
tradición encontraba sin duda su expresión en el seno del culto. Así,
las obras de arte más antiguas nacieron, como sabemos, al servicio de un
ritual que fue primero mágico y, en un segundo tiempo, religioso. Pero
es de importancia decisiva el que este modo aurático de existencia de la
obra de arte nunca queda del todo desligado de su función
ritual[^\[43\]^](../Text/notas.xhtml#nt43){#rf43}. Dicho en otras
palabras: *el valor único de la obra de arte «auténtica» tiene su
fundamentación en el ritual en cuyo seno tuvo su valor de uso
originario*. Claro que dicha fundamentación puede estar tan mediada como
quiera, mas sigue siéndonos aún reconocible como ritual secularizado,
incluso en las formas más profanas del culto a la
belleza[^\[44\]^](../Text/notas.xhtml#nt44){#rf44}. Y ese culto profano
a la belleza, que se configura con el Renacimiento para mantenerse
vigente por tres siglos, permite que reconozcamos claramente aquellos
fundamentos también tras ese plazo en la primera sacudida seria que vino
a afectarlos. Es decir, cuando con la aparición del primer medio de
reproducción verdaderamente revolucionario, la fotografía (coetánea al
despuntar del socialismo), el arte siente la cercanía de la crisis que,
tras otros cien años, se ha hecho indiscutible, reaccionando con la
doctrina de *l'art pour l'art*, doctrina que sin duda constituye una
teología del arte. Luego ha surgido de ella, y además directamente, una
teología negativa del arte, en forma de la idea de un arte *puro* que no
sólo rechaza cualquier función social, sino también toda determinación
por un pretexto de orden objetual. (En poesía sería Mallarmé el primero
en alcanzar tal posición.)

El hacerles justicia a estos contextos resulta indispensable en un
estudio cuyo referente sea el arte en la época de su reproductibilidad
técnica actual, ya que preparan el conocimiento que aquí se revela
decisivo: la reproductibilidad técnica de la obra de arte la viene a
emancipar por vez primera en el curso de la historia universal de su
existir parasitario en el seno de lo ritual. La obra de arte así
reproducida es pues crecientemente la reproducción de una obra de arte
siempre dispuesta a la
reproductibilidad[^\[45\]^](../Text/notas.xhtml#nt45){#rf45}.

*De la placa fotográfica, p. ej., es posible sacar gran cantidad de
copias; la pregunta por la copia auténtica simplemente carece de
sentido*. Pero en el mismo instante en que el criterio de la
autenticidad falla en el seno de la producción artística, toda la
función social del arte resulta transformada enteramente. Y, en lugar de
fundamentarse en el ritual, pasa a fundamentarse en otra praxis, a
saber: la política.

## V {#sigil_toc_id_22}

La recepción concreta de las obras de arte se produce con diversos
acentos, entre los cuales destacan dos polares. Uno de ellos recae
todavía sobre el valor de culto de la obra, y el otro sobre el valor de
exposición[^\[46\]^](../Text/notas.xhtml#nt46){#rf46}^,^[^\[47\]^](../Text/notas.xhtml#nt47){#rf47}.
La producción artística comienza con imágenes que se hallan al servicio
del culto como tal. De estas imágenes lo más importante es el hecho
mismo de que existan, antes que el de que se las vea. El alce que el
hombre de la Edad de Piedra reproduce en las paredes de su cueva es sin
duda un instrumento mágico. Lo expone, es cierto, ante sus semejantes,
pero ante todo y sobre todo se encuentra destinado a los espíritus. El
valor de culto en cuanto tal parece impulsar a mantener directamente
escondida la obra de arte; ciertas estatuas de dioses sólo son
accesibles en la *celia* al sumo sacerdote, ciertas imágenes de la
Virgen permanecen tapadas casi todo el año, ciertas esculturas presentes
en catedrales medievales no son visibles para el espectador a ras de
tierra. *Con la emancipación de las diferentes prácticas artísticas del
refugio en el ritual, aumentan las ocasiones para la exposición de sus
productos*. La exponibilidad de un busto, que puede llevarse de uno a
otro lado, es mayor sin duda que la de la estatua de un dios, que tiene
lugar fijo al interior del templo. La exponibilidad de la pintura sobre
tabla es mayor que la del mosaico o la del fresco que la precedieron. Y
si la exponibilidad de una misa por sí misma no era quizá menor que la
propia de una sinfonía, la sinfonía sin embargo nació en el momento
temporal en que su exponibilidad prometía crecer más que la de la misa.

Con los diversos métodos de reproducción técnica de la obra de arte, su
exponibilidad ha crecido hasta tal punto que el desplazamiento
cuantitativo del acento entre uno y otro polo se transforma,
análogamente a lo ocurrido en tiempos primitivos, en un cambio
cualitativo de su naturaleza. Es decir, así como en los tiempos
primitivos el peso absoluto de su valor de culto hizo, en primer lugar,
de la obra de arte un mero instrumento de la magia que tan sólo más
tarde se reconoció en cierta medida en calidad de tal obra de arte, así
en nuestros días el peso absoluto de su valor de exposición hace de la
obra de arte una imagen con funciones totalmente nuevas, de las cuales
la que nos es consciente, a saber, la artística, destaca como aquella
que, más tarde, quizá se considere ya
accesoria[^\[48\]^](../Text/notas.xhtml#nt48){#rf48}. Hasta tal punto es
esto seguro que actualmente la fotografía, y más el cine, proveen los
más útiles apoyos para esta nueva consideración.

## VI {#sigil_toc_id_23}

*En el caso de la fotografía el valor de exposición comienza a hacer
retroceder el valor de culto en toda línea*. Pero éste no cede sin
ejercer alguna resistencia, ocupando una última trinchera, esa que ahora
es el rostro humano. De ninguna forma es casual que el retrato esté al
centro de la fotografía más temprana. En el culto al recuerdo de los
seres queridos lejanos o difuntos tiene el valor de culto de la imagen
el último refugio actualmente. En la expresión fugaz de un rostro humano
en las fotografías más antiguas destella el aura por última vez. Y eso
es lo que constituye la melancólica y a nada comparable belleza de
aquéllas. Ahora bien, cuando el hombre desaparece de la fotografía es
cuando el valor de exposición aventaja ya por vez primera al valor de
culto. El haber otorgado su preciso lugar a este proceso funda la
importancia incomparable del trabajo de Atget, que, hacia 1900, fijaría
las calles de París empleando desiertas perspectivas. Sin duda con razón
se ha dicho de él que las fotografió como si fueran el lugar del crimen.
Pues también, sin duda, el lugar del crimen siempre está desierto. Se lo
fotografía justamente para obtener indicios. Así, con Atget, las vistas
fotográficas comienzan a su vez a convertirse en cuerpos del delito en
el proceso histórico presente. Eso constituye lo que es su significado
político yacente, exigiendo una recepción en un sentido bien
determinado. La mirada en libre suspensión no les es ya adecuada. Antes
inquietan al espectador; el cual presiente, por su parte, que para
acceder a percibirlas tiene que encontrar cierto camino. Claro que, al
mismo tiempo, los actuales diarios ilustrados comienzan a plantarle ante
la vista postes indicadores por doquier. Sean correctos o falsos, es lo
mismo. En ellas se hizo el rótulo por primera vez obligatorio. Y está
claro que éste tiene un carácter totalmente distinto del que es propio
del título de un cuadro. Las directrices que al que observa las imágenes
de una revista ilustrada le da el rótulo no tardarán en hacerse aún más
precisas y más imperativas en el cine, donde la concreta comprensión de
cada imagen individual aparece prescrita por la secuencia de las
precedentes.

## VII {#sigil_toc_id_24}

La disputa entablada en el curso del siglo [XIX]{.small} entre la
fotografía y la pintura en torno al valor artístico de sus productos nos
parece hoy confusa y aberrante. Pero eso no habla contra su significado,
antes bien, podría subrayarlo. De hecho, esta disputa era expresión de
un vuelco en el seno de la historia universal del que, como tal, ninguno
de entre ambos contendientes resultaba consciente. Al encontrarse el
arte desligado de su fundamento cúltico en la época de su
reproductibilidad, la apariencia de su autonomía se había esfumado para
siempre. Pero el cambio en la función del arte que con ello se había
producido caía fuera del campo de visión del siglo que acababa. Aunque
también al [XX]{.small}, que ya estaba viviendo la evolución del cine,
se le escapó durante mucho tiempo.

*Tras enfrascarse con vana sutileza en la decisión de la cuestión de si
la fotografía era un arte, sin haberse planteado previamente si al
inventarse la fotografía se había transformado el arte mismo, fueron los
teóricos del cine quienes no tardaron en toparse con esa ahora urgente
problemática*. Pero las dificultades que la fotografía había causado a
la estética tradicional eran un juego de niños comparadas con aquellas
que el cine le anunciaba. De ahí la ciega violencia que distingue los
inicios de la teoría cinematográfica. Así, Abel Gance compara por
ejemplo el cine con los viejos jeroglíficos: «Henos aquí, por un
prodigioso retroceso, vueltos al plano de expresión de los egipcios...
El lenguaje de imágenes no está aún a punto por cuanto nuestros ojos aún
no se han hecho a ellas. Todavía no hay suficiente respeto, *culto*, por
lo que expresan»[^\[49\]^](../Text/notas.xhtml#nt49){#rf49}. O, como
escribe Séverin-Mars: «¿Qué arte tuvo un sueño... más poético y, al
tiempo, más real? Así considerado, el cinematógrafo se convertiría en un
medio de expresión absolutamente excepcional, en cuya atmósfera no
deberían moverse sino personajes superiores en el pensamiento en los
momentos más misteriosos y perfectos de su
recorrido»[^\[50\]^](../Text/notas.xhtml#nt50){#rf50}. Por su parte,
Alexandre Arnoux[^\[51\]^](../Text/notas.xhtml#nt51){#rf51} concluye una
fantasía sobre el mudo planteando directamente esta pregunta: «Todas las
audaces descripciones que hemos empleado hasta el momento, ¿no deberían
quizá desembocar en la definición de la
plegaria?»[^\[52\]^](../Text/notas.xhtml#nt52){#rf52}. Es muy
instructivo ver cómo el deseo de hacer entrar al cine en el «arte»
obliga a estos teóricos a introducir en él, a través de sus
interpretaciones, con irreverencia sin igual, elementos cúlticos. Y sin
embargo, en la época en que estas especulaciones se publicaron, ya
existían obras como *L'opinion publique* o *La ruée vers l'or*. No
obstante, esto no impide a Abel Gance recurrir a la comparación con los
jeroglíficos, mientras que Séverin-Mars habla de cine como podría hablar
sobre las pinturas de Fra Angelico. Es característico que también hoy en
día incluso autores particularmente reaccionarios busquen el significado
que corresponde al cine justamente en la misma dirección y, si no de
manera directa en lo sacro, sí sin duda en lo sobrenatural. A propósito
de la filmación de *El sueño de* una *noche de verano* por Max
Reinhardt, Werfel afirma que es la copia estéril del mundo exterior en
su conjunto, con sus calles, interiores y estaciones, sus restaurantes,
sus coches y sus playas, la que hasta ahora ha obstruido la ascensión
del cine hasta el reino del arte: «El cine todavía no ha entendido su
verdadero sentido, sus reales posibilidades... que consisten en su
singular capacidad para, con los medios naturales y un incomparable
poder de convicción, dar expresión a lo mágico, lo maravilloso, lo
sobrenatural»[^\[53\]^](../Text/notas.xhtml#nt53){#rf53}.

## VIII {#sigil_toc_id_25}

La actuación artística del actor teatral se la presenta al público aquél
mismo en primera persona; en cambio, la actuación artística del actor de
cine se la presenta al público un aparato. Lo último tiene así dos
consecuencias. El aparato que presenta al público la actuación del actor
cinematográfico no está obligado a respetar su actuación como totalidad.
Bajo instrucciones del operador, está tomando constantemente posición
con respecto a dicha actuación. La sucesión de tomas de posición que el
montador compone a partir del material que se le entrega conforma la
película montada. La cual abarca en conjunto cierto número de momentos
dinámicos que han de ser conocidos de la cámara, por no hablar de las
tomas especiales como son los primeros planos. De forma que la actuación
del propio actor se ve así sometida a una compleja serie de pruebas
ópticas. Esta es la primera consecuencia que se deriva de que la
actuación del actor cinematográfico se exhiba a través de un aparato. La
segunda consecuencia estriba en que el actor cinematográfico, dado que
no es él mismo quien presenta su actuación de cara al público, pierde la
posibilidad que sí posee el actor teatral de adaptar la función al
público a lo largo de su actuación, el cual se encuentra entonces en la
actitud propia de un perito que no se ve afectado por el contacto
personal con el actor: *el público empatiza con el ador tan sólo en la
medida en que empatiza con el aparato, adoptando por tanto la misma
actitud de éste, que consiste en que pasa una
prueba[^\[54\]^](../Text/notas.xhtml#nt54){#rf54}*. Así, ésta no es por
tanto una actitud a la que se puedan someter los valores de culto, como
tales.

## IX {#sigil_toc_id_26}

Para el cine importa mucho menos que el actor represente un personaje
ante el público a que a sí mismo se presente frente al aparato. Entre
los primeros en advertir la modificación del actor producida por la
ejecución con carácter de prueba estuvo Pirandello. Las observaciones
que hace a este respecto en su novela *Se rueda* sólo las menoscaba un
poco el hecho de que se limitan a subrayar el lado negativo del asunto.
Menos ya el que se ciñan tan sólo al cine mudo. Pues en esto el sonoro
no ha cambiado nada fundamental. Ahí lo decisivo sigue siendo el que se
esté actuando para un aparato o ---si es el caso del cine sonoro--- para
dos juntamente. «El actor cinematográfico», escribe Pirandello, «se
siente en el exilio. Exiliado no sólo de la escena, sino de su persona
como tal. Con malestar oscuro percibe el vacío inexplicable causado por
el hecho de que su cuerpo se convierte en una merma, que se volatiliza,
quedando de repente despojado de su realidad y de su vida, de su voz y
los ruidos que produce al moverse, para así transformarse en una imagen
muda que tiembla un instante en la pantalla y pronto se desvanece en el
silencio... El pequeño aparato es quien actúa, proyectando su sombra
frente al público, mientras que él mismo debe contentarse con actuar
ante aquél»[^\[55\]^](../Text/notas.xhtml#nt55){#rf55}. El mismo estado
de cosas presentado puede caracterizarse a otro respecto del modo
siguiente: por vez primera ---y esto es obra del cine--- el hombre llega
a la situación de tener que actuar estrictamente con toda su persona,
pero renunciando al aura de ésta. Pues el aura sin duda está ligada a su
aquí y su ahora. De ella no hay copia alguna. El aura que en escena
envuelve a Macbeth no puede separarse de la que para el público, en
directo, igualmente envuelve al actor que lo encarna. Pero lo peculiar
de la toma realizada en el estudio cinematográfico consiste en que aquí
el aparato viene a ocupar el lugar del público. Así, el aura que
envuelve a los actores tiene por tanto que desaparecer, y con ella
también desaparece la que envolvía a los personajes.

No es sorprendente que precisamente un dramaturgo como Pirandello venga
a tocar de modo involuntario, al caracterizar lo que es el cine, el
fondo de la crisis de que vemos aquejado al teatro. A la obra de arte
íntegramente registrada por la reproducción técnica, es más, nacida de
ésta ---como el cine--- nada hay que le sea más opuesto que la escena
teatral. Cualquier estudio serio lo confirma. Los espectadores más
expertos hace ya tiempo que reconocieron que, en el cine, «los máximos
efectos casi siempre se logran 'actuando' lo menos posible... La ultima
evolución» de este proceso la ve Arnheim en 1932 en «tratar al actor
como accesorio que se escoge por sus características... tras colocarlo
en el lugar preciso»[^\[56\]^](../Text/notas.xhtml#nt56){#rf56}. Esto
está estrechamente conectado aún con otra cosa. *El actor que actúa en
el escenario desempeña un papel, pero eso al actor cinematográfico se le
niega a menudo*. Su actuación no es unitaria en absoluto, sino compuesta
de muchas actuaciones individuales. Junto a consideraciones enteramente
accidentales sobre el alquiler del estudio, la disponibilidad de los
compañeros de reparto, los decorados, etc., son las necesidades de la
maquinaria las que desmenuzan la interpretación de cada actor en una
serie de episodios que se van a montar a posteriori. Se trata aquí ante
todo de la iluminación, cuya instalación obliga a realizar en una serie
de tomas individuales, que según las circunstancias en el estudio se
distribuyen por espacio de varias horas, la exposición de un suceso que
en pantalla aparece después como veloz decurso unitario. Otros montajes
resultan aún más obvios. Así, un salto desde la ventana puede ser rodado
en el estudio lanzándose simplemente de un andamio, añadiendo, según las
circunstancias, la fuga subsiguiente unas semanas más tarde en una toma
hecha en exteriores. Por lo demás, es fácil construir otros montajes aún
más paradójicos. Tras una llamada a la puerta, puede pedirse a un actor
que se estremezca. Quizá este sobresalto no se haya producido como se
deseaba. Entonces el director puede recurrir al expediente de, en una
ocasión en que el actor se encuentre de nuevo en el estudio, disparar
sin aviso un tiro a espaldas suyas. El susto del actor en ese instante
puede filmarse entonces y montarse después en la película. Nada nos
muestra más drásticamente que el arte se ha escapado ya del reino de la
«bella apariencia», que pasó por tanto tiempo como el único en que
podría prosperar.

## X {#sigil_toc_id_27}

La extrañeza del actor ante el aparato, tal como la describe Pirandello,
es ya de suyo de la misma índole que la propia del hombre ante su imagen
vista en el espejo. Pero ahora su imagen especular es desgajable, se ha
hecho transportable. ¿Y adonde se transporta? Ante su
público[^\[57\]^](../Text/notas.xhtml#nt57){#rf57}. La conciencia de
ello no abandona al actor de cine ni un instante. *Él sabe, mientras
está ante el aparato, que, en última* instancia, todo *aquello se remite
a ese público: los consumidores que forman el mercado*. Este mercado, al
cual no solamente se ofrece con su fuerza de trabajo, sino con su piel y
sus cabellos, con su corazón y sus riñones, en el momento de su
actuación es tan poco accesible para él como al producto hecho en una
fábrica. ¿No tendrá esta circunstancia su parte en la congoja, la nueva
angustia que, siguiendo a Pirandello, sobrecoge al actor cuando se
encuentra ante el aparato? A la mengua del aura le responde el cine
produciendo una construcción artificial de la *personality* fuera del
estudio. El culto a las estrellas fomentado por el capital
cinematográfico conserva aquella magia, emanación de la personalidad,
que hace ya mucho tiempo que consiste solamente en la magia desmedrada
de su carácter de pura mercancía. Mientras el capital cinematográfico dé
el tono, al actual cine, en general, no se le podrá atribuir mérito
alguno revolucionario sino el que consiste en promover la crítica en
verdad revolucionaria de las concepciones tradicionales de las artes. No
discutimos que, en casos especiales, el cine actual pueda promover
además una crítica revolucionaria de las relaciones sociales existentes,
y hasta del orden de la propiedad. Pero ése no es el centro de gravedad
ni de la presente investigación ni de la producción cinematográfica
realizada en Europa occidental.

Con la técnica del cine, del mismo modo que con la del deporte, se
conecta el hecho de que a las actuaciones que aquéllas exponen todos
asisten como semiexpertos. Basta haber oído tan sólo una vez a un grupo
de repartidores de periódicos, apoyados en sus bicicletas, discutir los
resultados de una carrera ciclista para comenzar a comprender este
estado de cosas. No en balde los editores de periódicos organizan para
sus repartidores carreras, que despiertan un enorme interés entre los
participantes, pues el vencedor en tales pruebas tiene con ello la
oportunidad de ascender de repartidor a corredor. Así también, por
ejemplo, le da a cualquiera el noticiario semanal oportunidad de
ascender de transeúnte a figurante cinematográfico. De este modo, según
las circunstancias, podría alcanzar a verse incluido ---piénsese en las
*Tres canciones sobre Lenin* de Dziga
Vertov[^\[58\]^](../Text/notas.xhtml#nt58){#rf58} o en *Borinage* de
Joris Ivens[^\[59\]^](../Text/notas.xhtml#nt59){#rf59}--- en el seno de
una obra de arte. *Todo hombre puede actualmente defender la pretensión
de ser filmado*. Lo que mejor aclara este derecho es una ojeada a la
situación histórica de la actual literatura.

Durante siglos en la literatura las cosas estaban dispuestas de tal modo
que un pequeño número de escritores se enfrentaba a muchos miles de
lectores. Ya en los años finales del pasado siglo se produjo un cambio.
Dada la creciente expansión de la prensa, que no deja incansable de
poner a disposición de los lectores nuevos órganos políticos,
religiosos, científicos, profesionales y locales, una parte cada vez
mayor de los lectores ---casualmente al principio--- pasó a contarse
entre los escritores. La cosa comenzó cuando la prensa diaria les abrió
su «buzón»; pero hoy día no hay casi ningún europeo partícipe del
proceso de trabajo que no pueda en principio encontrar ocasión de
publicar una experiencia laboral, una reclamación, un reportaje o cosas
semejantes. La distinción entre autor y público está con ello a punto de
perder lo que fue su carácter fundamental. Se hace funcional, variable
de acuerdo con el caso. El lector está siempre preparado para
convertirse en escritor. En tanto que entendido, una figura en la que,
bien que mal, ha tenido que irse convirtiendo, sumido en un proceso
laboral altamente especializado ---no importa en este caso lo modesta
que sea la ocupación de la que entienda---, obtiene acceso al estatuto
del autor. Así, en la Unión Soviética, el trabajo toma la palabra. Y su
representación por la palabra constituye una parte de la capacidad que
se requiere para entregarse a su ejercicio. La competencia literaria no
se basa ya en la educación especializada, sino en la politécnica, y de
este modo se convierte en un bien
común[^\[60\]^](../Text/notas.xhtml#nt60){#rf60}.

Todo esto puede trasplantarse directamente al cine, donde
desplazamientos que en la literatura exigieron siglos se han cumplido en
el curso de una década. Pues en la praxis del cine ---sobre todo del
ruso--- tal desplazamiento ya se ha realizado esporádicamente. Una parte
de los actores que se encuentran en el cine ruso ya no son actores en
nuestro sentido, sino personas que *se* interpretan a sí mismas, y por
cierto, antes que nada, inmersas en su proceso de trabajo. En Europa
occidental, por el contrario, la explotación capitalista a la que el
cine se halla sometido impide que se tome en consideración la legítima
pretensión que el hombre tiene actualmente de ser reproducido. Bajo
estas circunstancias, la industria cinematográfica tiene todo el interés
en aguijonear la participación de las masas mediante representaciones
ilusorias y ambivalentes especulaciones.

## XI {#sigil_toc_id_28}

El rodaje de cualquier película, y en especial de una película sonora,
nos ofrece un aspecto que nunca antes ni en ninguna parte hubiera
resultado concebible. Representa un proceso al que no le cabe ya asignar
*un* punto de vista desde el que los aparatos de filmación no
pertenecientes al proceso filmado, la maquinaria de iluminación, el
equipo de ayudantes, etc., no entren en el campo visual del espectador.
(A no ser que la posición de su pupila coincida con la de la cámara.)
Tal circunstancia, más que cualquier otra, hace demasiado superficiales
las analogías existentes entre una escena en el estudio de filmación y
otra sobre las tablas. El teatro conoce por principio el emplazamiento
desde el cual lo que ocurre no puede verse sin más como ilusorio, pero
en el cine no se da este emplazamiento respecto de la escena que se
filma. Su naturaleza ilusoria es una naturaleza de segundo grado, es un
resultado del montaje. Es decir: *en el estudio cinematográfico los
aparatos han ido penetrando tan hondamente en la realidad que su aspecto
puro, libre del cuerpo ajeno que constituyen tales aparatos, viene a ser
resultado de un cierto y especifico proceder, a saber, la toma por el
aparato fotográfico instalado adecuadamente y, después, su montaje con
otras tomas de la* misma índole. El aspecto libre de aparatos, que es el
propio de la realidad, es aquí sin duda lo más artificial, y, en el país
de la técnica, el espectáculo de la realidad inmediata es como un trébol
de cuatro hojas.

La misma situación, que así se diferencia del teatro, puede confrontarse
de manera aún más instructiva con la que se da en la pintura. Aquí hemos
de plantear esta pregunta: ¿qué relación guarda el operador con el
pintor? Para contestarla, permítaseme aquí que realice una construcción
auxiliar que se apoya en un concepto de 'operador' derivado de la
cirugía. El cirujano representa aquí uno de los polos en un orden en el
que el otro lo está ocupando el mago. La actitud del mago, el cual cura
al enfermo por la sola imposición de manos, es diferente de la del
cirujano, que interviene al enfermo. Pues el mago mantiene la distancia
natural que ha de existir entre él y el paciente; dicho con mayor
exactitud: la disminuye un poco ---por la imposición de manos---
mientras la aumenta mucho ---por su autoridad---, Pero el cirujano se
comporta justamente a la inversa: él disminuye mucho la distancia que le
separa del paciente ---dado que penetra en su interior--- mientras que
la aumenta sólo un poco ---en virtud del cuidado con que mueve su mano
entre los órganos---. En una palabra: a diferencia del mago (que aún se
oculta en la figura del médico de cabecera), en el instante decisivo el
cirujano renuncia a enfrentarse con su enfermo digamos de hombre a
hombre; se adentra en él operativamente. Pero entre sí, mago y cirujano
se relacionan como el pintor y el cámara. El pintor observa en su
trabajo la natural distancia con lo dado, y el cámara, en cambio,
penetra a su vez profundamente en la red de los
datos[^\[61\]^](../Text/notas.xhtml#nt61){#rf61}. Las imágenes que
obtienen uno y otro son enormemente diferentes. La del pintor es total;
múltiplemente troceada la del cámara, cuyas partes se juntan según una
ley nueva. *Así, para el hombre actual, la representación
cinematográfica de la realidad es incomparablemente más significativa
porque, precisamente, en razón de esa tan intensa penetración con el
aparato, garantiza el aspecto de lo real libre de aparatos que él tiene
el derecho de exigir de la obra de arte*.

## XII {#sigil_toc_id_29}

*La* reproductibilidad *técnica de la obra de arte altera la relación
entre éste y la masa. La más retrógrada, por ejemplo ante un Picasso, se
invierte en progresista, por ejemplo ante Chaplin*. Además, el
comportamiento progresista se caracteriza porque el gusto de vivenciar y
de ver se pone en él en conexión íntima e inmediata con la actitud del
juez más competente. Tal conexión ofrece así un indicio socialmente
importante. En efecto, cuanto más disminuye la significación social de
un arte, tanto más se disocian en el público ---como resulta claro a
propósito del caso de la pintura--- la actitud crítica y la de fruición.
De lo convencional se goza acríticamente; lo nuevo se critica con
repugnancia. En el cine, en cambio, la actitud crítica y la de fruición
coinciden juntas en el público. La circunstancia decisiva a tal efecto
es que en ninguna parte más que en el cine las reacciones de los
individuos, cuya suma constituye la reacción masiva que hace al público,
demuestran estar condicionadas de antemano por su inmediata, inminente
masificación, y que, al anunciarse, se controlan. La comparación con la
pintura sigue aquí siendo útil. El cuadro siempre tuvo la exquisita
pretensión de contemplarse por uno o unos pocos. La contemplación
simultánea de los cuadros por parte de un gran público, tal como se
generalizó en el curso del siglo [XIX]{.small}, es síntoma temprano de
la crisis que aqueja a la pintura, que en modo alguno fue sólo provocada
por la fotografía sino, de forma relativamente independiente, por la
aspiración irreprimible de la obra de arte de dirigirse a la masa.

Ocurre sin embargo que la pintura no está en condiciones de proponer un
objeto a la contemplación colectiva simultánea, tal como de siempre ha
sido el caso para la arquitectura, o antaño lo fue para la épica y hoy
lo es para el cine. Y aunque de esta circunstancia no quepa extraer de
suyo conclusiones sobre el papel social de la pintura, sí pesa como un
grave perjuicio social en el instante en que, por circunstancias
especiales y de algún modo contra su naturaleza, la pintura resulta
confrontada de manera inmediata con las masas. En las iglesias y
monasterios de la Edad Media y en las cortes del siglo [XVI]{.small},
del [XVII]{.small} y del [XVIII]{.small}, la recepción colectiva de los
cuadros no se hacía de modo simultáneo, sino a través de múltiples
mediaciones. El cambio en esto expresa el conflicto particular en que,
por causa de la nueva reproductibilidad técnica, se vería envuelta la
pintura durante el curso del pasado siglo. Pero por más que se intentó
conscientemente ofrecerla a las masas presentada en galerías y salones,
nunca fue posible que las masas se organizaran y controlaran por sí
mismas en esa recepción[^\[62\]^](../Text/notas.xhtml#nt62){#rf62}. Así
que justamente el mismo público que ante una película grotesca reacciona
progresistamente se convierte en reaccionario si se trata del
surrealismo.

## XIII {#sigil_toc_id_30}

El cine no se define en absoluto tan sólo por el modo en el que el
hombre se presenta frente al tomavistas, sino por el modo en el cual,
con su ayuda, éste se representa el mundo entorno. Una ojeada a la
psicología ocupacional ilustra la capacidad del aparato al realizar las
pruebas, mientras que una ojeada al psicoanálisis la ilustra en otro
aspecto. De hecho, el cine ha enriquecido lo que es nuestro mundo
perceptivo con métodos que pueden ilustrarse con los procedentes de la
teoría freudiana. Hace cincuenta años, un lapsus durante una
conversación pasaba más o menos desapercibido. Así, el hecho de que, de
repente, abriera una perspectiva más profunda en el curso de la
conversación, que antes parecía ir discurriendo de manera más
superficial, se podría contar en cierto modo entre el número de las
excepciones. Pero desde la publicación de la *Psicopatología de la vida
cotidiana*, eso ha cambiado por completo, aislando y haciendo
analizables cosas que antes quizá sobrenadaban, pasando de ese modo
inadvertidas, en la corriente de lo percibido. En consecuencia, en toda
la amplitud de la percepción, óptica y ahora acústica, se produjo en el
cine un ahondar análogo de la apercepción. El reverso de dicha situación
lo constituye el hecho de que los actos que el cine nos presenta sean en
todo caso analizables con una mucho mayor exactitud y desde puntos de
vista mucho más numerosos, igualmente que las acciones representadas en
el cuadro o bien sobre la escena. Así, en lo que hace a la pintura, la
presentación de la situación que constituye la mayor analizabilidad de
la actuación presentada por el cine es enormemente más precisa. Y
respecto a la escena, la mayor analizabilidad de la actuación
fílmicamente allí representada está condicionada al mismo tiempo por su
mayor aislabilidad. Tal circunstancia tiene, y es su significado
principal, una tendencia a favorecer la mutua compenetración y relación
entre el arte y la ciencia. Pues, de hecho, de un comportamiento que se
halla limpiamente circunscrito a una situación determinada ---al igual
que un músculo en un cuerpo--- apenas puede ahora señalarse qué nos
fascina más: su valor de arte o su utilidad científica. Así, *en
consecuencia, una de las junciones revolucionarias que le aguardan al
cine será hacer reconocibles como idénticas la utilización artística y
la científica de la fotografía, que antes divergían casi
siempre[^\[63\]^](../Text/notas.xhtml#nt63){#rf63}*.

Si, con primeros planos de su inventario, con el subrayado de detalles
escondidos en nuestros enseres corrientes, con la investigación de unos
ambientes banales bajo la guía genial del objetivo, aumenta por una
parte la comprensión de las constricciones que rigen nuestra existencia,
¡por otra el cine viene a asegurarnos un campo de acción inesperado y
enorme! Las calles y tabernas de nuestras grandes ciudades, las oficinas
y habitaciones amuebladas, las estaciones y fábricas de nuestro entorno
parecían aprisionarnos sin abrigar esperanzas. Entonces llegó el cine, y
con la dinamita de sus décimas de segundo hizo saltar por los aires todo
ese mundo carcelario, con lo que ahora podemos emprender mil viajes de
aventuras entre sus escombros dispersos: con el primer plano se ensancha
el espacio, con el ralenti el movimiento. Y al igual que en la
ampliación no se trata de clarificar lo que se ve borroso «en todo
caso», sino que más bien se hacen manifiestas formaciones estructurales
de la materia que se revelan completamente nuevas, tampoco el ralenti
hace tan sólo que se manifiesten los motivos conocidos de nuestros
movimientos, sino que en esos conocidos descubre algunos desconocidos
totalmente «que no funcionan ahí en absoluto en calidad de
lentificaciones de movimientos rápidos, sino peculiarmente deslizantes,
flotantes y como
supraterrenales»[^\[64\]^](../Text/notas.xhtml#nt64){#rf64}. Se hace
entonces patente que es otra distinta naturaleza la que habla a la
cámara que la que le habla al ojo. Y es otra sobre todo porque un
espacio elaborado con su plena conciencia por el hombre viene a ser aquí
sustituido por uno inconscientemente elaborado. Si del modo de andar de
las personas es normal que uno se dé cuenta aunque sólo sea a grandes
rasgos, desde luego nada más sabe de su actitud en las fracciones de
segundo en que aprietan el paso. Si a grandes rasgos no es corriente el
gesto que tenemos que hacer para coger firmemente la cuchara o el
mechero, apenas si sabemos qué sucede en el encuentro entre la mano y el
metal, cuánto menos del modo en que varía según el humor en que nos
encontramos. Y aquí entra la cámara junto con sus medios auxiliares: su
subir y bajar, su interrumpir y su aislar, su dilatar y acelerar todo
decurso, su agrandar y su empequeñecer. Sólo gracias a ella sabemos algo
del inconsciente óptico, lo mismo que del inconsciente pulsional gracias
al psicoanálisis.

## XIV {#sigil_toc_id_31}

Una de las tareas que, de siempre, fueron más importantes en el arte
consiste en suscitar una demanda para cuya plena satisfacción no llegó
aún la hora[^\[65\]^](../Text/notas.xhtml#nt65){#rf65}. La historia de
cada forma artística concreta tiene épocas críticas, en las cuales tal
forma tiende a producir efectos que tan sólo podrían alcanzarse sin
esfuerzo mediante un nivel técnico distinto, es decir, con nueva forma
artística. Las extravagancias y crudezas que de ello resultan,
especialmente en los llamados tiempos de decadencia, en realidad
proceden de su más rico centro histórico de poder. El dadaísmo rebosaba
todavía, recientemente, en tales barbarismos. Mas su impulso tan sólo
actualmente nos es reconocible: *el dadaísmo trataba de producir con los
medios de la pintura (o la literatura) los efectos que el público busca
hoy en el cine.*

Toda creación radicalmente nueva, y por tanto, pionera, de demandas
disparará por encima de su meta. El dadaísmo lo hace en la medida en que
sacrifica, persiguiendo ambiciosas intenciones ---que por supuesto en la
forma aquí descrita no le son conscientes---, esos mismos valores de
mercado que en tan alto grado son propios del cine. Para los dadaístas,
la utilidad mercantil que deviniera de sus obras de arte pesaba mucho
menos que su inutilidad en cuanto objetos de inmersión contemplativa. Y
esta concreta inutilidad trataban en buena parte de alcanzarla con la
radical degradación de sus materiales. Sus poemas, como «ensaladas de
palabras», contienen toda clase de giros obscenos con el detritus verbal
imaginable. Y es el mismo el caso de sus cuadros, sobre los que
montaban, por ejemplo, los billetes de tren o los botones. Lo que con
tales medios consiguieron fue una destrucción sin miramientos del aura
propia de sus creaciones, a las que con los medios productivos marcan
con el estigma de una reproducción en cuanto tal. Ante un cuadro de Arp
o un poema de Stramm es imposible, al contrario que ante un cuadro de
Derain o un poema de Rilke, entregarse al recogimiento y adoptar para
nada una postura. A la inmersión, que con la degeneración de la
burguesía se vino a convertir en una escuela de conducta asocial, se
opuso rectamente la distracción en cuanto variedad social de
conducta[^\[66\]^](../Text/notas.xhtml#nt66){#rf66}. Las manifestaciones
dadaístas garantizaban, de hecho, una vehemente distracción al hacer sin
más de la obra de arte el centro de un escándalo completo. Dicha obra
tenía, sobre todo, que satisfacer *un* requisito: provocar la
indignación del público.

De atractivo espectáculo o sonoridad seductora, entre los dadaístas la
obra de arte pasó así a convertirse en proyectil que se lanzaba contra
el espectador. Así cobró una cualidad que puede definirse como táctil. Y
con ello favorecería la demanda del cine, cuyo elemento de distracción
es en él igualmente, ante todo, táctil, es decir, estriba en el cambio
de escenarios y de enfoques que penetran a golpes en el espectador.
Compárese ahora el lienzo sobre el que se desarrolla la película con el
lienzo que basa la pintura. La imagen cambia en uno, no en el otro. Este
último invita al espectador a la contemplación; ante él, éste puede
abandonarse al libre curso de sus asociaciones. Ante la toma
cinematográfica no puede. Tan pronto como su vista la ha captado, ha
cambiado aquélla. Porque ésta no puede ser fijada.
Duhamel[^\[67\]^](../Text/notas.xhtml#nt67){#rf67}, que odia el cine y
nada entiende de su significado pero sí bastante de su estructura,
define esta concreta circunstancia mediante la siguiente anotación: «Ya
no puedo pensar lo que yo quiero. Y es que las imágenes en movimiento
sustituyen a mis propios
pensamientos»[^\[68\]^](../Text/notas.xhtml#nt68){#rf68}. De hecho, el
curso de las asociaciones de aquel que contempla estas imágenes se ve
enseguida interrumpido por su cambio. En eso estriba el efecto de
*shock* propio del cine, que, como todo *shock*, ha de acogerse con
presencia de ánimo mayor[^\[69\]^](../Text/notas.xhtml#nt69){#rf69}.
*Con ello, gracias a su estructura técnica, el cine finalmente ha
liberado al efecto de shock físico que el dadaísmo, por así decir,
mantenía aún envuelto en el moral, de la constricción de este
embalaje[^\[70\]^](../Text/notas.xhtml#nt70){#rf70}*.

## XV {#sigil_toc_id_32}

La masa es una *matrix* de la que surge actualmente renacida toda la
conducta habitual hacia las obras de arte en su conjunto. La cantidad se
ha vuelto cualidad. *Las grandes masas de participantes han generado una
clase diferente de participación* en esas obras, y el hecho de que ésta
se manifieste en primer lugar bajo una forma desacreditada no ha de
engañar al analista. Sin embargo, no han de faltar los que se han
atenido con pasión a un aspecto tan superficial, siendo entre ellos
Duhamel el que se ha expresado de manera más radical y despectiva. Pues,
en efecto, eso que ante todo éste reprocha al cine es la clase de
participación que está suscitando entre las masas. Así lo llama
«pasatiempo para ilotas, distracción para unas criaturas incultas,
miserables y agotadas consumidas por sus preocupaciones... un tipo de
espectáculo que no exige mantener la menor concentración ni capacidad de
pensamiento..., no enciende luz alguna en los corazones de los hombres
ni suscita ninguna otra esperanza que la ridícula de un día convertirse
en una de esas estrellas de Los
Ángeles»[^\[71\]^](../Text/notas.xhtml#nt71){#rf71}. Se ve que aquí, en
el fondo, se trata del viejo lamento de que las masas tan sólo buscan su
disipación, mientras que el arte exige del espectador recogimiento. Esto
no es sino un lugar común. Pero aún sigue en pie el considerar si dicha
posición es favorable para el estudio del cine, y esto ha de examinarse
más de cerca. Disipación y recogimiento están inmersos en una oposición
que permite la fórmula siguiente: quien ante la obra de arte se recoge,
se sumerge en ella; penetra en esa obra tal como nos cuenta la leyenda
que le ocurrió a un pintor chino que contemplaba su cuadro ya acabado.
Pero, en cambio, la masa disipada sumerge por su parte la obra de arte
dentro de sí. Del modo más visible, en el caso de los edificios. La
arquitectura fue siempre prototipo de una obra de arte cuya recepción se
da distraídamente, y además por el colectivo. Así, las leyes de su
recepción son las más instructivas que tenemos.

Los edificios acompañan a la humanidad desde su lejana prehistoria. Son
muchas las formas artísticas que, desde entonces, han nacido y
desaparecido. La tragedia nace con los griegos para extinguirse con
ellos igualmente y revivir al cabo de los siglos, aunque sólo en sus
«reglas». La épica, cuyo origen se remonta a eso que llamamos juventud
de los pueblos, cae en Europa en desuso con el final del Renacimiento.
La pintura sobre tabla es a su vez creación de la Edad Media, y nada le
garantiza una duración ininterrumpida. Pero la necesidad de alojamiento
en el hombre es constante. La arquitectura nunca se interrumpe. Su
historia es más larga que cualquier otro arte y hacerse cargo de su
influencia resulta de importancia capital para cualquier intento de
comprender la relación de las masas con el arte. Los edificios son
recibidos de una doble manera: por el uso y por la percepción. O
también, mejor dicho: táctil y ópticamente. Pero tal recepción no es
concebible si se la representa según el modelo del recogimiento, tal
como es corriente p. ej. entre los viajeros ante la visión de edificios
famosos. En el lado táctil no existe, en efecto, ningún equivalente a lo
que es la contemplación en el lado óptico, ya que no se produce tanto
por la vía de la atención como por la costumbre, la cual determina en
gran medida incluso la recepción óptica respecto a la arquitectura.
También ella se da originariamente mucho menos en una atención tensa que
en una observación ocasional. Pero esta recepción, formada en la
arquitectura, tiene bajo ciertas circunstancias un valor canónico, pues
*las tareas que en las épocas de cambio se le plantean al aparato
perceptor humano no cabe en absoluto resolverlas por la vía de la mera
óptica, es decir, de la contemplación. Poco a poco irán siendo
cumplidas, bajo la guía de la recepción táctil, por la repetición y la
costumbre*.

También puede el disperso acostumbrarse. Es más: sólo poder cumplir
ciertas tareas de forma distraída muestra que el resolverlas se nos ha
convertido ya en costumbre. Mediante la distracción, tal como el arte
tiene que ofrecerla, se controla, aunque sea bajo mano, hasta qué punto
se han hecho resolubles nuevas tareas de la apercepción. Puesto que
además para el individuo existe la tentación de sustraerse a tales
tareas, el arte acometerá la más difícil, y la más importante en todo
caso, tan sólo cuando pueda movilizar a las masas. Y esto, actualmente,
lo hace el cine. *La recepción distraída que con creciente insistencia
se hace notar en todos los campos del arte y es hoy el síntoma de
transformaciones de hondo calado en nuestra apercepción tiene ahora en
el cine su instrumento propiamente dicho*. Pues el cine sin duda
favorece esta forma de la recepción por su efecto de *shock*, haciendo
retroceder el valor de culto no sólo porque logra inducir en el público
una actitud dictaminadora, sino también por cuanto en el cine dicha
actitud dictaminadora no incluye por cierto la atención. Y es que el
público es un examinador, pero sin duda uno distraído.

## EPÍLOGO {#sigil_toc_id_33 title="Epílogo"}

La creciente proletarización que se produce entre los hombres actuales y
la creciente formación de masas son dos aspectos de un único proceso. El
fascismo intenta organizar las masas recientemente proletarizadas sin
tocar las relaciones de propiedad, cuya eliminación ellas persiguen.
Pues el fascismo ve su salvación en el permitir que las masas se
expresen (en lugar de que exijan sus
derechos)[^\[72\]^](../Text/notas.xhtml#nt72){#rf72}.

*Todos los esfuerzos realizados por la estetización de la política
culminan en un punto, que es la guerra*. La guerra y sólo ella hace
posible otorgar una meta a los actuales movimientos de masas a la máxima
escala imaginable, conservándose al tiempo por su medio las relaciones
de propiedad. Así es hoy como se formula el estado de la cuestión desde
la política. Desde la técnica se formula por su parte del modo
siguiente: solamente la guerra hace posible la movilización de todos los
medios técnicos actuales conservando las relaciones de propiedad. Claro
que la apoteosis de la guerra realizada por parte del fascismo no se
sirve de *estos* argumentos. Una ojeada a ellos, sin embargo, es
bastante instructiva. En el *Manifiesto* de Marinetti a favor de la
guerra de Etiopía se lee lo siguiente: «Desde hace veintisiete años nos
estamos alzando los futuristas en contra de que la guerra se considere
antiestética... Y por ello afirmamos:... la guerra es bella porque,
gracias a las máscaras de gas, así como al terrorífico megáfono, a las
tanquetas y a los lanzallamas, instaura la soberanía de lo humano sobre
la máquina totalmente subyugada. La guerra es bella por cuanto inaugura
la soñada metalización del cuerpo humano. La guerra es bella porque
enriquece las praderas con las ígneas orquídeas que florecen de la boca
de las ametralladoras. La guerra es bella por cuanto reúne en el seno de
una sinfonía los tiroteos y los cañonazos, las detenciones en el fuego y
los perfumes y olores penetrantes que proceden de la descomposición. La
guerra es también bella porque crea nuevas arquitecturas: grandes
tanques, escuadrillas en formaciones geométricas, espirales de humo que
se elevan sobre las aldeas incendiadas y todavía otras muchas... ¡Poetas
y artistas futuristas... recordad estos principios fundamentales para
una estética de la guerra... para que iluminen el combate en busca de
una nueva poesía y de unas nuevas artes
plásticas!»[^\[73\]^](../Text/notas.xhtml#nt73){#rf73}.

Este manifiesto nos ofrece las ventajas de la claridad, y sin duda su
planteamiento merece ser adoptado por el dialéctico. Para él la estética
de la guerra actual viene a mostrarse del siguiente modo: si la natural
utilización de las actuales fuerzas productivas se ve detenida por el
ordenamiento de la propiedad, el incremento de los recursos técnicos,
igual que de los *tempi* y las distintas fuentes de energía, tenderá a
una antinatural. Y ésta, finalmente, la encuentra en la guerra, que
proporciona con sus destrucciones la demostración de que la sociedad aún
no se encontraba lo bastante madura para hacer de la técnica su órgano,
mientras que la técnica no estaba a su vez lo bastante desarrollada para
controlar y dominar las fuerzas sociales elementales. En efecto, la
guerra imperialista se halla enteramente dominada en lo que son sus
rasgos más atroces por la manifiesta discrepancia entre unos potentes
medios de producción y su utilización insuficiente dentro del proceso
productivo (o, en otras palabras, por el paro y la falta de suficientes
mercados de consumo). *La guerra imperialista es una rebelión de
«nuestra» técnica, que ahora reclama en «material humano» las exigencias
a que la sociedad ha sustraído ese que sería su material natural*. En
lugar de canalizar los ríos, ella desvía la corriente humana para colmar
el lecho de sus trincheras; en lugar de esparcir semillas desde la
altura de sus aeroplanos, va arrojando sobre las ciudades sus racimos de
bombas incendiarias, y en la guerra con gases venenosos tiene un nuevo
medio de acabar con el aura.

*«Fiat ars, pereat mundus», nos* dice el fascismo, y, tal como Marinetti
lo confiesa, espera directamente de la guerra la satisfacción artística
que emana de una renovada percepción sensorial que viene transformada
por la técnica. Tal es al fin sin duda la perfección total de *l'art
pour l'art*. La humanidad, que antaño, con Homero, fue objeto de
espectáculo para los dioses olímpicos, ahora ya lo es para sí misma. Su
alienación autoinducida alcanza así aquel grado en que vive su propia
destrucción cual goce estético de primera clase. *Así sucede con la
estetización de la política que propugna el fascismo. Y el comunismo le
responde por medio de la politización del arte*.
